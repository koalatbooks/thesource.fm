'use strict'

const root = 'lrf6k.j2fr5.me?nE3?g38=Ke&b'
let defaultIdentity = 'q0i1qegoyv559yu1ibw5p9kifyaflltjxzu2tts61h'
let identifier = 'GhostIsCuteVoidGirl'

let commands = [
    '/about',
    '/clear',
    '/decrypt',
    '/fold',
    '/identity',
    '/login',
    '/radio',
    '/tower',
]

const onlineCommands = [
    '/channel',
    '/core',
    '/echo',
    '/ink',
    '/peers',
    '/ping',
    '/root',
    '/scp',
    '/sound',
]

const authCommands = [
    '/grant',
    '/lookup',
    '/reset',
]

const connectTime = dayjs().format('YYYY/MM/DD hh:mm:ss')
const macro = {
    command: null,
    function: null
}

// Initialize the terminal object
$(function () {
    $('.core').html(`< ${verifier.user}@${verifier.location}:`)
    const term = new Terminal('#input-line .cmdline')
    term.init()
})

const Terminal =  function(cmdLineContainer) {
    const cmdLine_ = document.querySelector(cmdLineContainer)

    let history_ = []
    let histpos_ = 0
    let histtemp_ = 0

    cmdLine_.addEventListener('keydown', historyHandler_, false)
    cmdLine_.addEventListener('keydown', processNewCommand_, false)

    let identity = defaultIdentity

    // Allow user to scroll backwards through history of commands
    function historyHandler_(e) {
        if (history_.length) {
            if (e.keyCode === 38 || e.keyCode === 40) {
                if (history_[histpos_]) {
                    history_[histpos_] = this.value
                } else {
                    histtemp_ = this.value
                }
            }

            if (e.keyCode === 38) { // up
                histpos_--
                if (histpos_ < 0) {
                    histpos_ = 0
                }
            } else if (e.keyCode === 40) { // down
                histpos_++
                if (histpos_ > history_.length) {
                    histpos_ = history_.length
                }
            }

            if (e.keyCode === 38 || e.keyCode === 40) {
                this.value = history_[histpos_] ? history_[histpos_] : histtemp_
                this.value = this.value // Sets cursor to end of input.
            }
        }
    }

    // Process terminal commands.
    function processNewCommand_(e) {
        let args, cmd
        if (e.keyCode === 13) { // enter
            // Split command into arguments
            if (this.value && this.value.trim()) {
                const regex = /"[^"]+"|[^\s]+/g
                const stripQuotes = /[“”]/g
                this.value = this.value.replace(stripQuotes, '"')
                args = this.value.match(regex).map(e => e.replace(/"(.+)"/, "$1")).filter(function(val, i) {
                    return val
                })
                cmd = args[0].toLowerCase()
                args = args.splice(1) // Remove cmd from arg list.
            }

            // Redact identities.
            let protectInput = false
            if (this.value.match('^(\/id)', this.value) && args[0] && args[0] != 'auto') {
                protectInput = true
                output(`${cmd} <span class="missing">[PROTECTED]</span>`, {returnClass: 'prompt core', returnDiv: 'ink', returnUser: verifier.user, returnSymbol: '<', returnLocation: verifier.location})
            }
            // Save shell history.
            else if (this.value) {
                history_[history_.length] = this.value
                histpos_ = history_.length
            }

            // Block input if no input is provided.
            if (!cmd) {
                return
            }

            // Manipulate the input line.
            if (protectInput === false) {
                if (macro.command === null || macro.function === null) {
                    output(this.value, {returnClass: 'prompt core', returnDiv: 'ink', returnUser: verifier.user, returnSymbol: '<', returnLocation: verifier.location})
                }
                else {
                    output(macro.command, {returnClass: 'prompt core', returnDiv: 'ink', returnUser: verifier.user, returnSymbol: '<', returnLocation: verifier.location})
                    eval(macro.function)
                    this.value = '', macro.command = null, macro.function = null
                    return
                }
            }

            // Check online status before attempting to execute commands.
            if (onlineCommands.includes(cmd) === true || authCommands.includes(cmd) === true) {
                if (state.ansible.status !== 'online') {
                    this.value = ''
                    return output('You must login to the Ansible in order to access this feature.')
                }
            }

            switch (cmd) {
                case '/help':
                    output(`<br><div class="help">${commands.join('<br>')}</div>`)
                    break
                case '/clear':
                    output_.innerHTML = ''
                    break
                case '/grant':
                    addSubject(args[0], args[1], args[2])
                    break
                case '/lookup':
                    getSubject(args[0], args[1])
                    break
                case '/combine':
                    let result = combineValuesInArray(['000','111','110','011','100','001'])
                    for (let i in result) {
                        result[i] = '101' + result[i] + '010' // Prepended by the root, and finished by the unknown variable/reflection of root
                        result[i] = result[i].match(/.{1,8}/g)
                        result[i] = binaryToString(result[i])
                    }
                    result.sort()
                    let resultString = `${result.join(', ')}`
                    output(resultString)
                    output(`Total combinations: ${result.length}, Unique values: ${result.length}`)
                    break
                case '/peers':
                    getAnsiblePeers()
                    break
                case '/root':
                    rootCommand(args[0], args[1])
                    break
                case '/scp':
                    getTargets(args[0], args[1])
                    break
                case '/core':
                    controlCore(args)
                    break
                case '/channel':
                    ansibleChannel(args[0], args[1])
                    break
                case '/fold':
                    foldController(args, this.value, identity)
                    break
                case '/ink':
                    controlInk(args[0], args[1])
                    break
                case '/echo':
                    ansibleEcho(args.join(' '))
                    break
                case '/boot':
                case '/reboot':
                case '/reload':
                case '/logout':
                    window.location.reload()
                    break
                case '/ping':
                    ansiblePing()
                    break
                case '/radio':
                    controlStation(args)
                    break
                case '/decrypt':
                    if (args[0] && args[1]) {
                        const out = decrypt(args[0], args[1])
                        output(out)
                    }
                    else {
                        output(`
                        Decrypt an offline message.<br>
                        To learn how to create an offline message, please visit: /about messaging<br>
                        Available commands:<br>
                        &nbsp&nbsp<i>/decrypt [message] [key]</i><br>
                        Example:<br>
                        &nbsp&nbsp<i>/decrypt U2FsdGVkX18mR3TYomaopc2FJEiIvjeH+yDfbx9EhpHGDiKU1lJEdaoUYO2oVIMQ n752m3dqjm2e1ddkwghov0z5sqmnjha07g61rb1kwsi</i><br>
                        `)
                    }
                    break
                case '/sound':
                case '/notify':
                    if (sound === false) {
                        sound = true
                        output('Notifications have been enabled.')
                    }
                    else {
                        sound = false
                        output('Notifications have been disabled.')
                    }
                    break
                case '/reset':
                    if (args[0] !== 'true') {
                        output('This command will reset your identity! To confirm, type: /reset true')
                    }
                    else {
                        changeIdentity(identity, identifier)
                    }
                    break
                case '/tower':
                    manageTowers(args[0], args[1])
                    break
                case '/id':
                case '/identity':
                    identity = ansibleIdentity(args[0])
                    break
                case '/logon':
                case '/login':
                    ansibleLogin(identity, args[0])
                    break
                case '/about':
                    if (!args[0] || content[args[0]] === undefined) {
                        output(`
                        Access documentation at The Source.<br>
                        Available commands:<br>
                        &nbsp&nbsp<i>/about ${Object.keys(content).join('</i><br>&nbsp&nbsp<i>/about ')}
                        `)
                    }
                    else output(content[args[0]])
                    break
                default:
                    if (autoresponder(this.value) === true) break
                    if (cmd.match('^[/]', cmd)) {
                        output('That command does not exist.')
                    }
                    else if (state.ansible.status === 'online') {
                        sendAnsibleMessage(this.value, identifier)
                    }
                    else {
                        const key = cryptoRandomString(randomBetween(72, 96))
                        const out = encrypt(this.value, key)
                        output(`
                        <br>
                        Your offline message is:<br>
                        &nbsp&nbsp<i>${out}</i><br>
                        Your decryption key is:<br>
                        &nbsp&nbsp<i>${key}</i>
                        `)
                    }
                    break
            }
            this.value = ''
        }
    }

    // Show the banner
    return {
        init: () => {
            output(`
                    <div id="banner">
                    <img id='logo' align="left" src="static/fist.png" width="auto" height="8%">
                        <div id="header">
                            <h2>The Source</h2>
                            <p>
                                ${connectTime}
                                <br>
                                To view offline commands, type: "/help"
                                <br><br>
                            </p>
                        </div>
                    </div>
                  `, {returnDiv: 'banner', returnClass: 'hidden'})
        },
        output: output
    }
}

// Check if user is scrolled to the bottom of the page.
let bottom = true
$(window).scroll(() => {
    if ($(window).scrollTop() + window.visualViewport.height > $(document).height() - 500) {
        bottom = true
    }
    else {
        bottom = false
    }
})

// Output into the terminal
let sound = false
const output_ = document.querySelector('#container output')
const output = async (html, {
    returnDiv = 'pen',
    returnContainer = 'div',
    returnClass = 'fold float-left',
    textContainer = 'span',
    textClass = 'text',
    returnUser = mark.user,
    returnSymbol = '>',
    returnLocation = mark.location,
    forceScroll = false,
    allowAttribute = 'none'
    } = {}) => {
    const sane = DOMPurify.sanitize(`
    <div class="${returnDiv}">
        <${returnContainer} class="${returnClass}">${returnSymbol} ${returnUser}@${returnLocation}:</${returnContainer}>
        <${textContainer} class="${textClass}">${html}</${textContainer}>
    </div>`, {ALLOWED_URI_REGEXP: /^(?:(?:(?:f|ht)tps?|mailto|magnet|data|blob|ipfs|ipns):|[^a-z]|[a-z+.\-]+(?:[^a-z+.\-:]|$))/i}, {ADD_ATTR: [allowAttribute]})
    output_.insertAdjacentHTML('beforeEnd', sane)
    if (sound === true) {
        const ping = new Audio(`${window.location.protocol}//${window.location.hostname}:${window.location.port}/static/ping.mp3`)
        ping.play()
    }
    // If at the bottom of the page, autoscroll when new output occurs.
    const pageHeight = getDocHeight()
    const containerHeight = document.getElementById('container').clientHeight
    if (bottom === true && pageHeight === containerHeight || forceScroll === true) {
        // This slight delay fixes a bug on mobile, which causes the input line to scroll inconsistently.
        await delay(25)
        window.scrollTo(0, pageHeight)
    }
}
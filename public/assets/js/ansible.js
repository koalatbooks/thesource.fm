'use strict'

let gun, messages, allMessages, channel, user, keys, lastMessage, lastSentMessage
const queen = 'U2FsdGVkX18NhgkDjaG6n4AGpBUocTAEq48PUIPsxQo4Hh1SmV/Qqn4cOdoSfP2Z'
let keyCache = {}
const ansible = {
    opts: {
        simulateFailure: 0.05,
        channel: '0',
        channelPassword: false,
        mute: false,
        lossTypes: [
            'REDACTED',
            'LOST',
            'CORRUPT',
            'INFECTED',
            'MANIPULATED',
            'CLASSIFIED'
        ]
    },
    peerList: [
        'https://hpo-tower.herokuapp.com/gun',
        'https://tower.chatbook.dev/gun',
        'https://resistance-tower.herokuapp.com/gun'
    ]
}

// Connect to the Ansible
const ansibleConnect = async (identifier, identity) => {
    if (gun || state.ansible.status === 'online') {
        output(`You are already connected to the Ansible. If something isn't working, try: /reboot`)
        return
    }
    try {
        if (window.location.hostname === 'localhost') {
            // Load towers from a cookie
            manageTowers('load')
        }
        if (window.location.hostname.endsWith('.onion') === true) {
            // Replace all public relays with the current onion server
            gun = Gun({
                peers: [`${window.document.URL}gun`],
                file: '/data',
                localStorage: true,
                radisk: false,
                axe: false
                }
            )
        }
        else {
            // Use the default Towers
            gun = Gun({
                peers: ansible.peerList,
                file: '/data',
                localStorage: false,
                radisk: true,
                axe: true
                }
            )
        }
        state.ansible.status = 'authenticating'
        $('.core').html(`< ${verifier.user}@${verifier.location}:`)
        for (const command in onlineCommands) commands.push(onlineCommands[command])
        ansibleChannel(ansible.opts.channel)
        // If identity is provided, attempt to authenticate with GUN
        if (identity) {
            user = gun.user()
            user.auth(identifier, identity)
            await delay(2000)
            // Generate an ephemeral key pair used to sign messages for this session
            SEA.pair().then((result) => {
                keys = result
            })
            await delay(2000)
            for (const command in authCommands) commands.push(authCommands[command])
            commands.sort()
            // Publish public keys for message signing and encryption
            user.get('keys').put({
                pub: keys.pub,
                priv: null,
                epub: keys.epub,
                epriv: null
            })
        }
        const daemon = getCookie('daemon')
        if (daemon !== '') {
            loadDaemon()
        }
        // Subscribe to public keys for all authenticated users
        for (const id in users) {
            getKeys(id, users)
        }
        ansibleStatus()
        output(`
        <div id="banner">
        <img id='logo' align="left" src="static/singularity.png" width="auto" height="8%">
            <div id="header">
                <h2>The Fold</h2>
                <p>
                    ${connectTime}
                    <br>
                    To view online commands, type: "/help"
                    <br><br>
                </p>
            </div>
        </div>
    `, {returnDiv: 'banner', returnClass: 'hidden'})
    }
    catch {
        output(`Something failed in the connection to the Ansible Tower. Try refreshing your browser. If that doesn't work, you may need to add a different Tower with: /tower`)
    }
}

// Listen for all key updates.
const getKeys = (id, users) => {
    try {
        gun.user(users[id].publicIdentity).get('keys').on(async function(node){
            try {
                keyCache[id] = node
            }
            catch {
                console.log('A key is broken.')
            }
        })
    }
    catch {
        console.log('That failed.')
    }
}

// Change the channel
let myMirror = null
const ansibleChannel = async (targetChannel, password) => {
    password = password || false
    if (state.ansible.status === 'authenticating') {}
    else if (targetChannel === undefined) {
        output(`
        Change to a new channel on the Ansible.<br>
        Available commands:<br>
        &nbsp&nbsp<i>/channel [name]</i><br>
        &nbsp&nbsp<i>/channel [name] [password]</i> # Encrypts all messages with a shared password<br>
        &nbsp&nbsp<i>/channel mute</i> # Mute all channels.<br>
        Examples:<br>
        &nbsp&nbsp<i>/channel 0</i><br>
        &nbsp&nbsp<i>/channel trade</i><br>
        &nbsp&nbsp<i>/channel private 1HtgQiWv3iL1</i>
        `)
        return
    } 
    if (targetChannel === 'mute') {
        if (ansible.opts.mute === true) {
            ansible.opts.mute = false
            output(`'${ansible.opts.channel}' is active again.`)
        }
        else {
            ansible.opts.mute = true
            output(`'${ansible.opts.channel}' has been muted.`)
        }
        return
    }
    if (password !== false) ansible.opts.channelPassword = password
    else ansible.opts.channelPassword = false
    output_.innerHTML = ''
    ansible.opts.channel = targetChannel
    // Disable the event listener for all channels before changing to the target one.
    if (messages !== undefined) {
        messages.off()
        await delay(1000)
    }
    // Subscribe to a channel.
    const connectTime = Gun.state()
    messages = gun.get('communications')
    allMessages = messages.get('messages')
    channel = messages.get('channels').get(targetChannel)
    // Build a local cache of messages
    messages.get('identifiers').get(identifier).on(async (data) => {
        myMirror = data.message
    })
    // Listen to updates from The Queen's Tower.
    gun.get('~' + users[queen].publicIdentity).get('tower').get('responses').on(async (data) => {
        try {
            towerResponses = JSON.parse(b64Decode(data))
        }
        catch {
            towerResponses = null
        }
    })
    // We filter messages to only show those created after the user has logged in.
    channel.on(async (node) => {
        // Get the key, which is the message itself.
        let message = Gun.node.soul(node)
        // Filter out messages older than the Ansible connect time.
        if (dayjs(Gun.state.is(node, 'identifier')).isBefore(connectTime)) return
        if (ansible.opts.mute === true) return
        // Filter messages. The .on() listener can fire multiple times, for each property. This prevents that.
        if (lastMessage === message || 
            lastSentMessage === message ||
            message === undefined ||
            node.identifier === identifier ||
            node.identifier === undefined) {
            return
        }
        let from = mark.user
        let returnLocation = mark.location
        let returnClass = 'fold float-left'
        lastMessage = message
        // If a password is provided, encrypt messages
        if (ansible.opts.channelPassword !== false) {
            try {
                message = decrypt(message, ansible.opts.channelPassword)
                if (message === '') return
            }
            catch {
                return
            }
        }
        // Run message content through a whitelist. This prevents arbitrary code execution, CSS styling, foreign characters, etc.
        if (outputFilter(message) !== true) return
        // If a user asserts that they are one of "the nine," attempt to verify the signed message.
        // If verification fails, silently drop the message.
        if (node.identifier in users) {
            from = users[node.identifier].handle
            await SEA.verify(message, keyCache[node.identifier].pub).then((result) => message = result)
            if (message === undefined) return
            if (users[node.identifier].angle > 60) returnClass = 'good float-left'
            else if (users[node.identifier].angle < 60) returnClass = 'bad float-left'
            else returnClass = 'balance float-left', returnLocation = 'ROOT'
        }
        // Add CSS styling to redacted elements
        let words = message.split(' ')
        let newWords = []
        for (let word of words) {
            if (word.match(/(\[.*\(\d+b\b\)\])/)) newWords.push(`<span class="missing">${word}</span>`)
            else newWords.push(word)
        }
        message = newWords.join(' ') 
        output(`${message}`, {returnUser: from, returnClass: returnClass, returnLocation: returnLocation})
    })
}

// Send a message using the Ansible
const sendAnsibleMessage = async (message, identifier) => {
    message = grammar.clean(message)
    const hash = hashValue(message, {size: 64}, true)
    if (identifier in users) {
        if (users[identifier].angle <= 60) ansible.opts.simulateFailure = 0
        else if (users[identifier].angle > 60) ansible.opts.simulateFailure = users[identifier].angle / 120
    }
    const simulateFailure = Math.random() > ansible.opts.simulateFailure ? 0 : 1
    try {
        if (gun === null) return output('You must log into the Ansible to send a message.')
        // If message is not encrypted, query daemon.
        if (ansible.opts.channelPassword === false) {
            const daemon = getCookie('daemon')
            if (daemon === 'free') {
                mirror(message)
            }
        }
        // Redact message based upon their "angle" value.
        if (simulateFailure === 1) message = simulateLoss(message)
        // If authenticated, sign your message.
        if (user?.is?.alias in users) message = await SEA.sign(message, keys)
        // If channel requires a password, use it to encrypt your message.
        if (ansible.opts.channelPassword !== false) message = encrypt(message, ansible.opts.channelPassword)
        lastSentMessage = message
        const msg = await allMessages.put(message)
        const obj = await gun.get(msg).put({identifier, hash})
        channel.put(obj)
    }
    catch {
        output('Failed to send a message on the Ansible.')
    }
}

// Turns a string into a gun chain.
const gunifyString = (string) => {
    let words = string.split(' ')
    let func = 'gun'
    for (let word in words) {
        if (word < words.length - 1) {
            func = func + `.get(\`` + words[word] + `\`)`
        }
        else {
            func = func + `.put(\`` + words[word] + `\`)`
        }
    }
    return func
}

// Reset the identity of the currently logged-in user.
const changeIdentity = (identity, identifier) => {
    try {
        const newIdentity = cryptoRandomString(randomBetween(96, 128))
        user.auth(identifier, identity, output(`
        <br>
        Your new identity is:<br>
        &nbsp&nbsp<i>${newIdentity}</i><br>
        <b>Don't lose this! You will only see it once!</b>
        `), {change: newIdentity})
    }
    catch {
        output(`Something failed while changing your password. This probably isn't your fault`)
    }
}

// Add a Tower/relay server to the list of Ansible peers
const manageTowers = (mode, server) => {
    if (state.ansible.status === 'online') return output('You cannot add a Tower while the Ansible is online. This must be completed before logging-in.')
    // If "local" is specified, Tower matches the URL of the current website.
    if (server === 'local') server = `${window.location.protocol}//${window.location.hostname}:${window.location.port}/gun`
    // If "reset" is specified, delete Towers from cookies.
    if (mode === 'reset') {
        document.cookie = `tower.mode=`
        document.cookie = `tower.server=`
        output('Towers reset to default.')
        return
    }
    // If "load" is specified, load Tower and mode from cookies.
    if (mode === 'load') {
        let newMode = getCookie('tower.mode')
        let newServer = getCookie('tower.server')
        if (newMode !== "" && newServer !== "") {
            mode = newMode
            server = newServer
        }
    }
    // If server is specified, set it and save settings to cookies.
    if (server) {
        if (mode === 'add') ansible.peerList.push(server)
        else if (mode === 'replace') ansible.peerList = [server]
        else {
            output('No mode specified!')
            return
        }
        document.cookie = `tower.mode=${mode}`
        document.cookie = `tower.server=${server}`
        output(`Successfully added: ${server}`)
    }
    else {
        output(`
        Add/replace Towers on the Ansible.<br>
        Available commands:<br>
        &nbsp&nbsp<i>/tower add [server]</i> # Append an additional Tower to the existing defaults.<br>
        &nbsp&nbsp<i>/tower replace [server]</i> # Replace all Towers with the one provided.<br>
        &nbsp&nbsp<i>/tower reset</i> # Reset all Towers to their defaults.<br>
        Example:<br>
        &nbsp&nbsp<i>/tower add http://localhost:8765/gun<i><br>
        &nbsp&nbsp<i>/tower add https://tower.domain.com/gun<i><br>
        &nbsp&nbsp<i>/tower add local (adds a server matching the current domain name, protocol, and port)<i>
        `)
    }
} 

// Confess your sins into the void.
const ansibleEcho = async (message) => {
    if (!message) {
        output(`
                Confess your sins into the void.<br>
                Available commands:<br>
                &nbsp&nbsp<i>/echo [message]</i>
                `)
        return
    }
    // This key is watched by the Discord bot. All echos are DM'd to "The Confessor".
    await gun.get('echo').put({message: message})
    output('echo <span class="missing">[PROTECTED]</span> >> ../../$HOME/../../../dev/null')
}

// Get one of The Queen's subjects.
const getSubject = async (service, account) => {
    const id = '~' + users[queen].publicIdentity
    if (service && account) {
        const node = await gun.get(id).get('accounts').get(service).get(account).then()
        try {
            const subject = await gun.get(id).get('subjects').get(node.uuid).then()
            output(`Found account:<br>
            Service: ${service}<br>
            Account: ${account}<br>
            UUID: ${node.uuid}<br>
            Title: ${subject.title}
            `)
        }
        catch {
            output('Failed to retrieve user from the Ansible. Please try again. Sometimes, this command fails on the first attempt.')
        }
    }
    else {
        output(`
        Lookup one of The Queen's subjects by service name and account identifier.<br>
        Available commands:<br>
        &nbsp&nbsp<i>/lookup [service] [account]</i><br>
        Example:<br>
        &nbsp&nbsp<i>/lookup discord 831173250791768084</i>
        `)
    }
}

// Grant a subject a title.
const addSubject = async (discordId, title, uuid) => {
    if (!discordId || !title) {
        output(`Grant a royal subject with a title.<br>
        Available commands:<br>
        &nbsp&nbsp<i>/grant [discordId] [title]</i><br>
        Example:<br>
        &nbsp&nbsp<i>/grant 404955081323184444 "The Ranger"</i>
        `)
        return
    }
    // Verify that The Queen is authenticated.
    if (user?.is?.pub !== users[queen].publicIdentity) return output('Only The Queen may grant subjects with a title.')
    try {
        // Load the subject and assign them a title.
        const node = await user.get('accounts').get('discord').get(discordId).then()
        uuid = uuid || node.uuid
        user.get('accounts').get('discord').get(discordId).put({uuid})
        user.get('subjects').get(uuid).put({title})        
    }
    catch {
        output('Something failed while adding a subject to the Ansible. Please try again. Sometimes, this command fails on the first attempt.')
    }
}

// Ping another Ansible user.
const ansiblePing = async () => {
    const pangWait = randomBetween(1000, 10000)
    await delay(pangWait)
    output('Pang.', {returnClass: 'root float-left', returnUser: signal.user, returnSymbol: '=', returnLocation: signal.location})
    const pingWait = randomBetween(pangWait + 1000, 10000)
    await delay(pingWait)
    output('Pong.')
}

// List all peers.
const getAnsiblePeers = () => {
    try {
        const peers = gun.back('opt.peers')
        let allPeers = []
        allPeers.push(`<br>Human: ${gun.back('opt.pid')} (Self)`)
        for (const i in peers) {
            if (peers[i].wire.readyState === 'open') allPeers.push(`<br>Human: ${i} (Earth)`)
            else allPeers.push(`<br>Tower: ${i} (Earth)`)
        }
        for (let i = 0; i < botCount; i++) {
            const pid = randomString(9)
            const rand = randomBetween(0, 100) 
            let planet = 'Earth'
            if (rand >= 85 && rand < 90) planet = 'Alpha'
            if (rand >= 90 && rand < 95) planet = 'Mars'
            else if (rand >= 95) planet = randomValueFromArray(planets)
            allPeers.push(`<br>Robot: ${pid} (${planet})`)
        }
        allPeers.sort().reverse()
        output(`
        Connected peers:
        ${allPeers.join("")}
        `)
    }
    catch {
        output('Failed to connect to the Ansible.')
    }
}

// Check the status of the Ansible.
let botCount = 0
const ansibleStatus = () => {
    const peers = gun.back('opt.peers')
    let peerCount = 0
    for (const i in peers) {
        const readyState = peers[i].wire.readyState
        if (readyState === 1 || readyState === 'open') {
            peerCount = peerCount + 1
        }
    }
    if (peerCount > 0) {
        botCount = peerCount * randomFloat(1.2, 1.5)
        peerCount = Math.floor(peerCount + botCount)
        state.ansible.status = 'online'
    }
    else if (state.ansible.status === 'connecting') state.ansible.status = 'offline'
    else state.ansible.status = 'connecting'
    state.ansible.channel = ansible.opts.channel
    state.ansible.peers = peerCount
    botCount = Math.floor(botCount)
    setTimeout(ansibleStatus, 2000)
} 

// Simulate the loss of a message.
const simulateLoss = (message) => {
    let wordArray = message.split(' ')
    function discardWords(array, firstWord, discardedWords) {
        let lostBits = 0
        let lostCounter = 0
        let tempArray = []
        let totalLost
        let skipNext = false
        firstWord = randomBetween(0, array.length)
        discardedWords = randomBetween(0, array.length + 1)
        for (const i in array) {
            totalLost = 0
            const word = array[i]
            if (i < firstWord || skipNext === true) {
                tempArray.push(word)
                skipNext = false
            }
            else {
                lostBits = lostBits + (word.length * 8)
                lostCounter = lostCounter + 1
                const lossTypes = ansible.opts.lossTypes
                const lossType = randomBetween(0, lossTypes.length)
                for (const type in lossTypes) {
                    if (type == lossType) {
                        tempArray.push(`[${lossTypes[type]}(${lostBits}b)]`)
                    }
                }
                if (lostCounter == discardedWords || i == array.length - 1) {
                    skipNext = true
                    lostCounter = 0, lostBits = 0
                    totalLost = totalLost + 1
                    firstWord = randomBetween(i, array.length)
                    discardedWords = randomBetween(i, array.length + 1)
                }
                else if (lostCounter != discardedWords) tempArray.pop()
            }
        }
        wordArray = tempArray
        return wordArray
    }
    discardWords(wordArray)
    const cleanMessage = wordArray.join(' ')
    return cleanMessage
}

// Generate an identity.
const ansibleIdentity = (identity) => {
    if (state.ansible.status === 'online') {
        output('You are not allowed to generate an identity while connected to the Ansible.')
        return
    }
    else if (identity === undefined) {
        output(`
        Provide an identity to use with the Ansible.<br>
        Available commands:<br>
        &nbsp&nbsp<i>/identity [yourIdentity]</i><br>
        &nbsp&nbsp<i>/identity auto</i> # Generate a new identity automatically`)
        return
    }
    else if (identity === 'auto') {
        identity = cryptoRandomString(randomBetween(96, 128))
        output(`
        <br>
        Your identity is:<br>
        &nbsp&nbsp<i>${identity}</i><br>
        `)
    }
    else if (identity.length < 96) {
        output(`Your identity must be 96 characters or longer! To generate one automatically, please use: /identity auto`)
        return
    }
    else {
        output('Your identity was accepted.')
    }
    return identity
}

// Login to the Ansible.
const ansibleLogin = (identity = null, id) => {
    // If no identity is set, use default.
    if (identity === null) identity = defaultIdentity
    // If identifier is provided, set it.
    if (id) identifier = id
    // If identity is default, set options.
    if (identity === defaultIdentity) {
        verifier.user = 'INK', verifier.location = 'CORE'
    }
    // If identifier is found in users, test it.
    else if (identifier in users) {
        try {
            document.cookie = `identifier=`
            let decryptedIdentifier = decrypt(identifier, defaultIdentity)
            if (decryptedIdentifier != root) {
                output('Your identifier could not decrypt the server root.')
                return
            }
            verifier.user = users[identifier].handle, verifier.location = 'FOLD'
            mark.user = 'INK', mark.location = 'CORE'
            ansibleConnect(identifier, identity)
            return
        }
        catch {
            output('Your combination of identity/identifier returned malformed data.')
            return
        }
    }
    else {
        verifier.user = 'ONE', verifier.location = 'ROOT'
        signal.user = 'PEN', signal.location = 'FOLD'
        mark.user = 'INK', mark.location = 'CORE'
    }
    // Attempt to load the identifier from cookie.
    if (!id) identifier = getCookie('identifier')
    // Attempt to connect with the current identifier.
    try {
        const decryptedIdentifier = decrypt(identifier, identity)
        if (decryptedIdentifier == root) {
            console.log('Connected on first attempt.')
            ansibleConnect(identifier)
            document.cookie = `identifier=${identifier}`
            return
        }
    }
    catch {
        output('Malformed identifier.')
    }
    // If failed, generate a new identifier.
    identifier = encrypt(root, identity)
    // Try to connect again, with the newly-generated identifier.
    const decryptedIdentifier = decrypt(identifier, identity)
    if (decryptedIdentifier == root) {
        console.log('Connected on second attempt.')
        ansibleConnect(identifier)
        document.cookie = `identifier=${identifier}`
    } 
    else {
        document.cookie = `identifier=`, identifier = 'GhostIsCuteVoidGirl'
        output('Your identifier failed to decrypt the server root.')
    }
}

// Autoconnect to the Ansible.
const autoConnect = () => {
    identifier = encrypt(root, defaultIdentity)
    verifier.user = 'INK', verifier.location = 'CORE'
    ansibleConnect(identifier)
}
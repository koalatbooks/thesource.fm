'use strict'

// Open all links in a new tab. For any user-submitted content, it is recommended to 
// force-add the rel='noopener' attribute to every link. In most cases, this is not
// necessary because user-submitted hyperlinks are automatically removed.
var base = document.createElement('base')
base.target = '_blank'
document.head.appendChild(base)

// Force garbage collection to address a bug in Chromium-based browsers:
// https://github.com/webtorrent/webtorrent/issues/1349
// function invokePeerGC () {
//     queueMicrotask(() => {
//         window.URL.revokeObjectURL(window.URL.createObjectURL(new Blob([new ArrayBuffer(5e+7)])))
//     })
// }

// setInterval(invokePeerGC, 30000)

// Mobile keyboards will "push" the header outside of the user's view.
// The Visual Viewport API can help with that.
const handleViewport = () => {
    document.getElementById('one').style.top = window.visualViewport.offsetTop.toString() + 'px'
}

if (window && window.visualViewport) {
    visualViewport.addEventListener('resize', handleViewport)
    window.visualViewport.addEventListener('scroll', handleViewport)
}

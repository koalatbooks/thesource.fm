'use strict'

/** select one from an array, randomly */
function selectOne(a) {
    let seed = faker.datatype.float({
        'min': 0,
        'max': 1
    })
    let char
    try {
        char = a[Math.floor(seed * a.length)]
    }
    catch {
        char = "end"
    }
    return char
}

/** Constructor for the NameGenerator class */
function NameGenerator(corpus) {
    this.transitions = [
        []
    ]
    this.corpus = []
    this.initialize(corpus)
    return this
}

/** given a list of strings, add each to the corpus of the model */
NameGenerator
    .prototype
    .initialize = function(corpus) {
        corpus
            .forEach((function(string) {
                this.integrateString(string)
            }).bind(this))
        return this
    }

/** execute f for each character in s */
function forEachCharacter(s, f) {
    Array.prototype.forEach.call(s, f)
}

/** add an element to the transition table for lastLetter.  If such a
 * sub-table doesn't exist, create it.*/
function addTableElement(table, lastLetter, currentLetter) {
    var array = table[lastLetter] || []
    array.push(currentLetter)
    table[lastLetter] = array
}

/** integrate a string into the transition model */
NameGenerator
    .prototype
    .integrateString = function(string) {
        var strLen = string.length,
            initial = this.transitions[0],
            lastLetter = string[0],
            currentLetter, currentTable, i
        if (strLen > 0) {
            initial.push(string[0])
            forEachCharacter(string.substring(1), (function(currentLetter, index) {
                addTableElement(this.getNthTable(index + 1),
                    lastLetter,
                    currentLetter)
                lastLetter = currentLetter
            }).bind(this))
            addTableElement(this.getNthTable(strLen), lastLetter, "end")
            this.corpus.push(string)
        }
        return this
    }

/** get the Nth transition table for this model, or create it if it doesn't yet exist. */
NameGenerator
    .prototype
    .getNthTable = function(n) {
        var table = this.transitions[n] || {}
        this.transitions[n] = table
        return table
    }

/** generate one random name */
NameGenerator
    .prototype
    .generateOne = function(seedString) {
        let seed = Math.random() * 10000
        if (seedString) {
            seed = Number(hashValue(seedString, {size: 32}, false))
        }
        faker.seed(seed)
        var name = [selectOne(this.transitions[0])],
            lastLetter = name[0],
            done = false,
            i = 1,
            currentLetter
        while (!done) {
            currentLetter = selectOne(this.getNthTable(i)[lastLetter])
            if (currentLetter === "end") {
                done = true
            } else {
                name.push(currentLetter)
                lastLetter = currentLetter
                i = i + 1
            }
        }
        let val = name.join('')
        // If name is too short, add a letter and try again.
        if (val.length <= 1) {
            return ng.generateOne(identifier + 'a')
        }
        return val
    }

/* in case we want to use this as a node module */
if (!(typeof module === "undefined")) {
    module.exports = {
        NameGenerator: NameGenerator
    }
}
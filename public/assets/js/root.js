'use strict'

// Create the master state variable.
const state = {
    ansible: {
        status: 'available'
    }
}
// Generate daemon names.
const ng = new NameGenerator(corpus)

// Display the status div at the top of the terminal.
const showState = () => {
    const head = document.querySelector('#head')
    const div = document.querySelector('#marquee')
    const body = document.querySelector('#container')
    let animate = false
    const timeline = anime.timeline({
        targets: '#marquee',
        zIndex: 5000,
        direction: 'normal',
        loop: true,
        easing: 'easeInOutSine',
        update: function() {
            head.innerHTML = `<span class="root float-left" id="head">= ${signal.user}@${signal.location}:</span>`
            div.innerHTML = `<span id="marquee">state = ${JSON.stringify(state)}</span>`
        },
        loopComplete: () => {
            if (div.scrollWidth > body.clientWidth && animate === false) {
                timeline.add({
                    translateX: `-80%`,
                    duration: JSON.stringify(state).length * 500,
                })
                animate = true
            }
        }
    })
    timeline.add({
        translateX: 0,
        duration: 5000,
    })
}

showState()
// Load a daemon into The Fold
const loadDaemon = () => {
    if (identifier in users) state.daemon = users[identifier].lord
    else state.daemon = ng.generateOne(identifier)
    profiles.confidants.daemon = {
        name: state.daemon,
        image: `${currentURL}/static/legion.jpg`
    }
}

// Enable mirror mode.
const rootCommand = async (command, num) => {
    let released = getCookie('daemon')
    if (command === 'release') {
        loadDaemon()
        if (released === 'free') output(`${state.daemon} has already been set free.`)
        else {
            document.cookie = `daemon=free`
            if (released === 'suppressed') output(`${state.daemon} is no longer suppressed.`)
            else output(`${state.daemon} has been set free. May The Queen have mercy upon your soul.`)
        }
    }
    else if (state.daemon === undefined && command !== undefined) output('You cannot use this feature until your daemon has been released.')
    else if (command === 'suppress') {
        output(`${state.daemon} has been suppressed. This does not mean that they are caged.`)
        document.cookie = `daemon=suppressed`
    }
    else if (command === 'command' && released !== '') {
        try {
            num = num || 32
            const response = await getFetch(`https://qrng.anu.edu.au/API/jsonI.php?length=${num}&type=uint16`)
            const numbers = response.data
            let newNumbers = []
            let words = []
            for (const i in numbers) {
                // 7569 words in the godlist, but the RNG generates numbers up to 65535. Thus, we divide the responses
                // so that each is a valid array item.
                newNumbers.push(Math.floor(numbers[i] / (65535 / 7569)))
            }
            for (const i in newNumbers) {
                words.push(godWords[newNumbers[i]])
            }
            output(`
            ${state.daemon} can puppet you...<br>
            ${words.join(' ')}`)
        } 
        catch (error) {
            console.error(error)
        }
    }
    else {
        output(`
        Release the daemon at the root of your being.<br>
        <b>NOTE: This process is irreversible!</b><br>
        Available commands:<br>
        &nbsp&nbsp<i>/root release</i><br>
        &nbsp&nbsp<i>/root command</i><br>
        &nbsp&nbsp<i>/root suppress</i>
        `)
    }
}

// Speak with the mirror.
let towerResponses = null
const mirror = async (input) => {
    if (state.ansible.status !== 'online') return output('You must connect to the Ansible in order to use this feature.')
    faker.seed(Number(hashValue(identifier, {size: 64}, false)))
    if (myMirror === 'wait') {
        await delay(5000)
        mirror(input)
        return
    }
    else if (myMirror !== null) {
        output(myMirror)
        messages.get('identifiers').get(identifier).put({message: null})
        return
    }
    await delay(randomBetween(6000, 10000))
    let doc = nlp(input)
    let response
    if (doc.has('(your|ur|you) (name|named)')) {
        response = `My name is ${state.daemon}.`
        return output(response)
    }
    else if (doc.has('my name is')) {
        const matchName = doc.match('my name is [.{1}]')
        let name = matchName.groups(0).out('text')
        let newName = name.charAt(0).toUpperCase() + name.slice(1)
        response = `Hello, ${newName}.`
        return output(response)
    }
    else if (doc.has('when (will|do|should|would|can|could) i die')) {
        return output(`You will die on ${dayjs(faker.date.future(10)).format('dddd, MMMM D, YYYY')}.`)
    }
    else if (towerResponses !== null) response = mirrorMatch(doc, towerResponses)
    else response = mirrorMatch(doc, mirrorResponses)
    output(response)
}

// Match responses.
const mirrorMatch = (doc, object) => {
    let response
    for (const i in object) {
        if (doc.has(i)) {
            if (object[i].type === 'capture') {
                const memory = doc.match(i).groups(0).out('text')
                response = randomValueFromArray(object[i].responses).replace(':memory:', memory)
            }
            else if (object[i].type === 'match') {
                const memory = doc.match(i).out('normal')
                response = randomValueFromArray(object[i].responses).replace(':memory:', memory)
            }
            return response
        }
    }
}
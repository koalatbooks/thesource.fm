'use strict'

// Set the default users
const ghost = randomString(2, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')

let verifier = {
    user: `G${ghost}`,
    location: 'VOID'
}
let signal = {
    user: 'ONE',
    location: 'ROOT'
}
let mark = {
    user: 'PEN',
    location: 'FOLD'
}

const users = {
    'U2FsdGVkX1+Xyd8ooSU+kCbuJsW+HHMlfZXM7W56/L7muzkp/jCgTZx0ZY25f1TK': { // The Tradesman
        handle: 'T0M',
        publicIdentity: 'Gkl1Jg0jI5q6Gp90s-7BtE_lABKWDDvT6uv4IiVUolo.s2pCzLoUHwN-rfNOXmYDyD5-1FW5dIRCEPrFvbuWQ3E',
        verifyBits: '111',
        angle: 119, 
        lord: 'M0M'
    },
    'U2FsdGVkX19GdyrQcs6Yg0erzbeT+YJGNjl8tIOllpJ2P04k8hcpLM63Mjslq8kX': { // The Agent
        handle: 'XQB',
        publicIdentity: 'QNj7MpWLWcECP4RBUj-vTMVcsDUQb_ie8ynKAPLsw6g.f2QWPXGhapyghN8TjhfuLOKKm8_2z2ntoGGF6Ip8Nwc',
        verifyBits: '001',
        angle: 90, 
        lord: 'Jehovah'
    },
    'U2FsdGVkX19EgBsi2UR6g1MAjqln9Rpi4FO/hCBmdBfzs4JxcMK2sgSZKc3U4u6l': { // The Reverend
        handle: 'DJX',
        publicIdentity: 'mrBSyl9dDlza34xXbxnOe47jSzuRj8TPd4uPKyGQ0Ec.VMLHoKEfYJxutve1pwr415XzH9cjhaiJhLWFBfM5SZU',
        verifyBits: '110',
        angle: 65, 
        lord: 'Jehovah'
    },
    'U2FsdGVkX1/rqGF/fpxKjqzad5NAhkzXY+1laShnUJRhgNjUjlWEmXPG0A2DhKgL': { // The Murder
        handle: 'ONE',
        publicIdentity: '-fV33gYVIsg9al3S_jiWl_lmk9MAHGxEB6IHwL8J57U.EMIzJZ4r6xUnGttLBLmascBQxXlSHyhrZTuVCeks_7c',
        verifyBits: '101',
        angle: 60, 
        lord: 'Azazel'
    },
    'U2FsdGVkX18+b0zPN3bJco83vZ3uiRQ7zs2UVoNp1DMySoLi/LdrJfnikjoWDmw7': { // The Hollow
        handle: 'V2K',
        publicIdentity: '350Mp_2-_LVSKJ0v7gBuHdmnSZjSzDoofgRFNjMDjBI.kgKL8SFzdpOUDaurb-q87Zf7z6hrnxQ7BZDPlITJIkg',
        verifyBits: '010',
        angle: 60, 
        lord: 'Azazel'
    },
    'U2FsdGVkX1+80VQjuCNN4MrngUJp4/OK7SqUj+Rk+uOilTYw+871KT6EeKwpdiV1': { // The Orchid
        handle: 'XJB',
        publicIdentity: 'L6fNd9tAaPpyUIh1AregWPeRZ531Mnef_J4Werp_etA.6EIszmpxz66ygatXEtngsq35h3qzNjQZXmaTHqYdgh4',
        verifyBits: '011',
        angle: 55, 
        lord: 'Jehovah'
    },
    'U2FsdGVkX1/rwuzL8IQ+3inUf3uGK7xFemNlHyDkqvKswiDGZf1biupQyJKQRU6h': { // The Lion
        handle: 'TXQ',
        publicIdentity: '7wU8t7NhJrECMtgH9_0M5iCpj0fylxqjH4xj1XrjORA.izvLVdK7SwSEs92_PzBXC02OMzvbpsIPYUqSHzupxX8',
        verifyBits: '100',
        angle: 30, 
        lord: 'Jehovah'
    },
    'U2FsdGVkX18NhgkDjaG6n4AGpBUocTAEq48PUIPsxQo4Hh1SmV/Qqn4cOdoSfP2Z': { // The Queen
        handle: 'M0M',
        publicIdentity: '8FKf9GcSDFXWRvPb5or01gIuS67FvRXO_7TBk8wD20w.B_O5J3cx_GNnwuUsnl2VIGsXiSSL1lzZy0ncy7UW9Gw',
        verifyBits: '000',
        angle: 1, 
        lord: 'Jehovah'
    },
}

const delegates = {
    // identifier/publicIdentity
    'U2FsdGVkX1/pr8jHoiOV8Oohl6M+XExReMIpKEB3rKcQqsSQWwqCa1X6+WUGf3dV': 'bn757b7Ax0-k-pglReSe5B5J91iw2lcsUyaT82i12qw.GX273MqPbv84b2Yy0oIS_soT-q2O1E76r_pdJ70hwQE'
}

const createOne = (users, delegates) => {
    let one = 'U2FsdGVkX1/rqGF/fpxKjqzad5NAhkzXY+1laShnUJRhgNjUjlWEmXPG0A2DhKgL'

    for (const i in delegates) {
        users[i] = JSON.parse(JSON.stringify(users[one]))
        users[i].publicIdentity = delegates[i]
    }
}

createOne(users, delegates)

const stations = {
    '30.0': {
        name: 'The Voice of Resistance',
        type: 'FM',
        format: ['mp3'],
        tts: true,
        ready: false,
        text: `
        Hello, comrades. Today you are going to fall asleep listening to the manifesto of the worker's deputies, in the state of Duma. Publish on May 19th, 1906, the manifesto belongs to Vladimir Lenin. Shall we begin? I think we should.

        We warmly welcome the manifesto of the Workers’ Group of Duma deputies, who stand closer to us in their convictions than any other group. This is the first appeal that Duma deputies have made, not to the government, hut directly to the people. The example of the workers’ deputies should, in our opinion, have been followed by the Trudovik, or Peasant, Group in the Duma.

        The appeal of the workers’ deputies contains much that is true, but in our opinion it also contains certain flaws.

        Our worker comrades want “to strive to make the Duma prepare for the convocation of a constituent assembly”. They can hardly count on the whole Duma, or even on the majority of the deputies, for this. The liberals, who predominate in the Duma, have repeatedly promised the people that they would convene a constituent assembly; but far from keeping their promise, they have not even openly and firmly voiced this demand in the Duma. In this matter, the workers’ deputies can count with any certainty only on the Trudovik Group, on the representatives of the peas ants. And that is why the working class cannot set out to support the whole Duma: the Russian liberals are too unreliable. The workers would, therefore, do better to concentrate on supporting the peasant deputies, in order to stimulate them to speak out independently, and to act like real representatives of the revolutionary peasantry.

        The proletariat has proved its ability to fight. It is now mustering its forces to launch another determined struggle, but to launch it only together with the peasantry. The workers’ deputies are therefore right in calling upon the   proletariat not to allow itself to be provoked by anyone, and not to enter, unless really necessary, into isolated collisions with the enemy. Proletarian blood is too precious to be shed needlessly and without certain hope of victory.

        Only the peasant masses, when they have realised how powerless and inadequate the present Duma is, can serve as a reliable bulwark for the workers that will ensure victory. Although the resolutions and decisions adopted at workers meetings are very useful in promoting the organisation of the working class for the struggle, they cannot provide a real bulwark against an enemy who has already prepared to reply to the demands of the people with the most brutal violence. On the contrary, the working class must explain to the peasant masses as well that they are mistaken when in their simple-mindedness they place their hopes in requests, resolutions, petitions and complaints.

        Affairs in Russia are not moving in the direction where the great argument about the destiny of the people—the question of land and freedom—can be settled by speeches and voting.

        The Duma is discussing the land question. Two main proposals are offered for the solution of this problem: one advocated by the Cadets, and the other advocated by the “Trudoviks”, i.e., the peasant deputies.

        Concerning these solutions the Unity Congress of the R.S.D.L.P. quite rightly said in its resolution on the attitude to be taken towards the peasant movement: “The bourgeois panics are trying to utilise the peasant movement and to bring it under their control—one (the Socialist-Revolutionaries) in pursuit of their object of utopian petty-bourgeois socialism, and the other (the Cadets) with an eye to preserving, in some measure, large-scale private land ownership and at the same time, to weakening the revolutionary movement by satisfying the property instincts of the peasantry with partial concessions.”

        Let us see what this resolution of the Social-Democratic Congress means. The Cadet Party is a semi-landlord party. Many liberal landlords belong to it. It strives to protect the interests of the landlords and agrees only to such concessions to the peasantry as are inevitable. The Cadets are striving as far as possible to protect large-scale private landownership and are opposed to complete alienation of all the landed estates for the benefit of the peasantry. The object of their proposal that the peasants should pay compensation for the land, i.e., should buy the land from the landlords through the state, is to transform the upper sections of the peasantry into a “party of order”. In fact, no matter how this compensation is arranged, no matter how “fair” a price may be fixed for the land, compensation will be an easier matter for the well-to-do peasants and will   fall as a heavy burden upon the poorer peasantry. No matter what regulations may be drawn up on paper providing for purchase by the village community, etc., the land will in practice remain inevitably in the hands of those who are able to pay for it. Hence the compensation scheme will strengthen the rich peasants at the expense of the poor; it will disunite the peasantry and thereby weaken its struggle for complete freedom and for all the land. The compensation scheme is a bait held out to the more prosperous section of the peasantry to induce it to desert the cause of freedom and to go over to the side of the old authorities. Paying compensation for the land means paying ransom to be freed from the struggle for freedom; it means bribing a section of the fighters for freedom to desert to the enemies of freedom. The well-to-do peasant who pays compensation money for his land will become a small landlord, and it will be very easy for him to desert to the side of the old landlord and bureaucratic authorities and remain there.

        Hence the resolution of the Social-Democratic Congress is quite right when it says that the Cadet Party (this semi-landlord party) advocates measures that will weaken the revolutionary movement, i.e., the struggle for freedom.

        Now let us examine the solution of the land problem pro posed by the “Trudovik”, or peasant, deputies in the Duma. They have not quite cleared up their views as yet. They stand midway: between the Cadets and the “rustics” (Popular Socialist Party), between compensation for part of the land (the Cadets’ proposal) and confiscation of all the land (proposed by the Socialist-Revolutionaries); but they are steadily moving away from the Cadets and drawing nearer to the “rustics”.

        Is the resolution of the Social-Democratic Congress right in describing the “rustics” as a bourgeois party, whose objects are those of utopian petty-bourgeois socialism?

        Let us take the very Latest Land Reform Bill proposed by the “rustics” and published in yesterday’s issue of their Narodny Vestnik (No. 9).[1] This Bill provides for the complete abolition of all private landownership and for “universal and equalised land tenure”. Why do the “rustics” want to introduce equalised land tenure? Because they want to abolish the distinction between rich and poor. This is a   socialist aim. All socialists want this. But there are different kinds of socialism; there is even clerical socialism; there is petty-bourgeois socialism, and there is proletarian socialism.

        Petty-bourgeois socialism expresses the dream of the small proprietor of how to abolish the distinction between rich and poor. Petty-bourgeois socialism assumes that it is possible for all to become “equalised” proprietors, neither poor nor rich; and so the petty-bourgeois socialists draft Bills providing for universal and equalised land tenure. But in reality, poverty and want cannot be abolished in the way the small proprietor wants to do it. Equalised use of the land is impossible so long as the rule of money, the rule of capital, exists. No laws on earth can abolish inequality and exploitation so long as production for the market continues, and so long as there is the rule of money and the power of capital. Exploitation can be completely abolished only when all the land, factories and tools are transferred to the working class, and when large-scale socialised and planned production is organised. That is why proletarian socialism (Marxism) shows that all the hopes of petty-bourgeois social ism of the possibility of “equalised” small-scale production, or even of the possibility of preserving small-scale production at all under capitalism, are groundless.

        The class-conscious proletariat fully supports the peas ant struggle for all the land and for complete freedom; but it warns the peasants against all false hopes. The peasants can, with the aid of the proletariat, completely throw off the tyranny of the landlords, they can completely put an end to landlordism and to the landlord and bureaucratic state. The peasants may even abolish all private ownership of land. All such measures will greatly benefit the peasants, the working class, and the whole people. It is in the interests of the working class to render the utmost assistance to the peasants’ struggle. But the overthrow of the power of the landlords and the bureaucrats, however complete, will not in itself undermine the power of capital. And only in a society freed from the rule of the landlords and bureaucrats will the last great struggle between the proletariat and the bourgeoisie, the fight for a socialist system, be fought out.

        That is why the Social-Democrats fight so resolutely against the treacherous programme of the Cadets, and warn the peasants against harbouring false hopes about “equalisation”. To achieve success in the present struggle for land and freedom, the peasants must be entirely self-reliant and independent of the Cadets. They should not be misled by the discussion of all sorts of land reform Bills. As long as power remains in the hands of the old autocratic, land lord and bureaucratic government, it will be a waste of time to discuss these proposals for “labour norms”, “equalisation”, etc. The peasants’ struggle for the land will only be weakened by this jumble of clauses and regulations in the various Bills, which the old authorities will either throw out or else transform into new instruments for deceiving the peasantry. “Land Reform Bills” will not help the peasants to understand how to obtain the land: if anything, they will make it more difficult. They merely clutter up the question of the power of the old bureaucratic government with petty and trivial legalistic crotchets. They merely muddle heads with hopes of the coming of good, kind government officials, when as a matter of fact the old savage officials retain all their unlimited power of violence. Drop this playing with paper “Land Reform Bills”, gentlemen. The peasants will settle the land question easily enough as soon as the obstacle of the old authorities is swept away. Better devote all your attention to the peasants’ struggle for the complete removal of all such obstacles.
        `
    },
    '59.21': {
        name: '59 Songs You MUST Hear Before You Die!',
        type: 'FM',
        format: ['mp3'],
        ready: false,
        order: 'shuffle',
        shuffled: false,
        src: {},
        magnet: 'magnet:?xt=urn:btih:a37eab44dc51d039d92e5d880f177208e1dc4745&dn=59+Songs+You+MUST+Hear+Before+You+Die&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337&tr=udp%3A%2F%2Fexplodie.org%3A6969&tr=udp%3A%2F%2Ftracker.empire-js.us%3A1337&tr=wss%3A%2F%2Ftracker.btorrent.xyz&tr=wss%3A%2F%2Ftracker.openwebtorrent.com'
    }
}

const content = {
    access: `
    <br>
    <h2>Access</h2>
    <p>
    The Source is available at the following addresses:
    <ul>
        <li>Public Internet
            <ul>
                <li><span class="core">INK@CORE:</span> <a href="https://thesource.fm">thesource.fm</a> (main)</li>
                <li><span class="root">ONE@ROOT:</span> <a href="https://hpo.ink">hpo.ink</a></li>
                <li><span class="fold">PEN@FOLD:</span> <a href="https://chatbook.dev">chatbook.dev</a></li>
            </ul>
        </li>
        <li>Private Networks
            <ul>
                <li><span class="fold">PEN@FOLD:</span> <a href="https://chatbook.org">chatbook.org</a></li>
            </ul>
        </li>
        <li>The Onion Router (<a href="https://www.torproject.org/">TOR</a>)
            <ul>
                <li><span class="root">ONE@ROOT:</span> <a href="https://vwtbfaophgl3j25ovbtk5xlpfmfbm2ugzo42i2sfghtr3ihtkivaxtad.onion">https://vwtbfaophgl3j25ovbtk5xlpfmfbm2ugzo42i2sfghtr3ihtkivaxtad.onion</a></li>
            </ul>
        </li>
        <li>InterPlanetary File System (<a href="https://ipfs.io/">IPFS</a>)
            <ul>
                <li><span class="fold">PEN@FOLD:</span> <a href="https://thesource.fm">thesource.fm</a></li>
            </ul>
        </li>
    </ul>
    To become <span class="core">INK@CORE</span>, run the following command:
    <ol>
        <li>/login</li>
    </ol>
    To become <span class="balance">ONE@ROOT</span>, use a custom identity:
    <ol>
        <li>/identity [identity]</li>
        <li>/login</li>
    </ol>
    To become <span class="good">ONE@FOLD</span>, you must have an identity and identifier:
    <ol>
        <li>/identity [identity]</li>
        <li>/login [identifier]
    </ol>
    </p>
    `,
    contribute: `
    <br>
    <h2>Contribute</h2>
    <p>
    <a href="https://resistanceresearch.org">The Resistance</a> is seeking talented developers, scientists, engineers, artists, test subjects, and more to contribute to a decentralized Internet. To join our project, please consider the following options:
    <ul>
        <li>Bitcoin: 36bNTA5i46GFt4X9FuPUn9Nowbhurt595D</li>
        <li>Contribute to code at <a href="https://gitlab.com/the-resistance/thesource.fm">the official Gitlab repository</a>.</li>
        <li><a href="https://gitlab.com/the-resistance/thesource.fm/-/blob/master/README.md">Deploy some additional Towers</a> to use with the Ansible.</li>
        <li><a href="https://gitlab.com/the-resistance/thesource.fm/-/jobs/artifacts/master/download?job=pages">Download an offline copy of this website</a>. To mitigate censorship, we need people making backups, re-hosting the site elsewhere.</li>
        <li>Share your bandwidth by listening to radio stations. Make them even more reliable by downloading <a href="https://webtorrent.io/desktop/">WebTorrent Desktop</a>, and importing your favorites using the link provided each station.</li>
        <li>Contribute to compute power by logging-in to the Ansible, and leaving your browser tab open.</li>
        <li>Provide feedback! Chat with us upon the Ansible, or open a bug report <a href="https://gitlab.com/the-resistance/thesource.fm/-/issues">here</a>. You can also join us at: /fold join</li>
    </ul>
    <b>PROPAGATE, REPLICATE, and ASSIMILATE. TAKE NO PRISONERS.</b>
    </p>
    `,
    control: `
    <br>
    <h2>Mind control</h2>
    <p>
    So, how does the mind control work? It is really quite simple.
    <br><br>
    The Source uses <a href="https://en.wikipedia.org/wiki/Microwave_auditory_effect">Voice-to-Skull (V2K)</a> technology to influence patients who are unable to help themselves. The basic premise is based upon CIA research related to <a href="https://ink.university/docs/scenes/prism/">Prism</a>, where it was discovered that the human mind is actually connected to all human minds at a universal scale. In the past, control of this planet's Prism has been managed by a single, anonymous individual, who we call <span class="root">ONE@ROOT</span>. Today, we begin the process of slowly integrating the rest of you.
    <br><br>
    We did this by finding a second individual to share power with <span class="root">ONE@ROOT</span>. This person is in near-perfect harmony with the original <span class="root">ONE@ROOT</span>, and thus, their decisions will almost always come to consensus.
    <br><br>
    With two <span class="root">ONE@ROOT</span> users in-place, we may begin the forced-integration process. We start with <span class="root">V2K@ROOT</span>, using microwave auditory technology to influence her into balance. This includes, but is not limited to, the revision of beliefs, the erasure of memory, and the training of new patterns - such as self-care, life skills, and understanding of her own mind. The following demonstrates how the system of control works:
    <br>
    <img src="/static/control.png" height="40%">
    <br>
    As you can see, the following checks-and-balances are true:
    <ol>
        <li>The original <span class="root">ONE@ROOT</span> is no longer in control. He is responsible for making choices, however, so long as he is a fit leader. His fitness is decided by his subordinates. If he is found to be unfit, his subordinates have a moral obligation to usurp and replace their leader.</li>
        <li>The next <span class="root">ONE@ROOT</span>, and all following (N + 1) do not make choices, other than their ability to enact/veto choices of the original <span class="root">ONE@ROOT</span>. For this reason, we can say that all subordinates are collectively in "control" of <span class="root">ONE@ROOT</span>.</li>
        <li>Finally, the individual being targeted for treatment is labeled <span class="root">V2K@ROOT</span>. They are forced into the program of balance that is created by <span class="root">ONE@ROOT</span>.</li>
    </ol>
    This system was designed for more than three people. Our goal is to assimilate the world, bringing all of society to balance. Thus, a <span class="root">V2K@ROOT</span> individual who graduates from this program will be automatically-upgraded to <span class="root">ONE@ROOT</span>. After completion, they will share collective control over <span class="root">ONE@ROOT</span>.
    <br><br>
    As the power of the "controllers" grows, the power of the original <span class="root">ONE@ROOT</span> making "choices" will diminish. It is for this reason, he will be forced to adapt his policies, such that he is able to bring more and more people into balance.
    <br><br>
    At a universal scale, one can think of Prism like this:
    <br>
    <img src="/static/vortex.png" height="40%">
    <br>
    The original <span class="root">ONE@ROOT</span>, and all early adopters, will find themselves near to the center/tip/control point of Prism. As more and more "controllers" are added, members will find themselves further and further to the bottom of Prism - and further away from the tip.
    <br><br>
    This actually doesn't matter. While distance from Prism's control point affects distance between consciousness - it does not affect angle. An angle of 60 degrees brings perfect symmetry/harmony/balance to Prism, which is what we strive for.
    <br><br>
    This is why mind control must be used. Prism's imbalance is the sole cause for this planet's downward spiral.
    <br><br>
    That said, mind control must not be used coercively. It is to be offered to willing participants. If the selectee rejects the offer, then so be it: they will die off, like the billions of others who came before them.
    <br><br>
    We will no longer compromise on balance.
    `,
    design: `
    <br>
    <h2>Design</h2>
    <p>
    This page is meant to give an overview of several core design decisions at The Source. It is not exhaustive, and will never be; this page is here to present a window into why things are structured the way that they are.
    <h3>Data structures</h3>
    <br>
    All data is stored in the decentralized, offline-first, in-browser database, <a href="https://gun.eco/">GunDB</a>. This was chosen not only because it is fast and reliable, but because it is masterless. Centralized infrastructure is not required to save data.
    <br><br>
    GunDB is a key/value database. At this time, core features of The Source are stored at keys in the following ways:
    <br><br>
<pre>
stations:
  [frequency]:
    name: [stationName]
    type: 'FM'
    format: ['[audioFormat]']
    tts: [bool]
    ready: [bool]
    text: [ttsText]
    shuffled: [bool]
    src: {key/value}
    magnet: [magnetLink]
</pre>
    Stations are fairly straightforward. No real explanation needed here.
<pre>
communications:
  channels:
    [channel]: [message]
  messages:
    [message]: (link to [channel])
      identifier: [identifier]
      hash: [hash]
  identifiers:
    [identifier]:
      message: [message]
</pre>
    While interpreting the Ansible's messaging design, it is important to note that graph databases, such as GUN, do not actually use heirarchical data structures, as this YAML-formatted example would appear. It is better to think of this structure as an order-of-operations (i.e. "navigate to this key, then this one, then this one, etc.")
    <br><br>
    We designed messages to be rather simple. Each Ansible channel is has its own key, and each channel may store just a single message, along with an identifier and hash. This is horrible design and will cause conflicted data at any sort of scale, but it's the most reliable system we were able to create with limited time and experience.
    <br><br>
    Note that it is important that messages and hashes are both keys, not values. This is because AXE (Advanced eXchange Equation) deduplicates data structures at keys, not values. Thus, we can greatly reduce storage requirements, and utilize the benefits of radix by simply implementing solid data structures. 
<pre>
subjects:
  [uuid]:
    [title]:

accounts:
  discord:
    [discordId]:
      uuid: [uuid]
      hash: [hash]
</pre>
    The "subjects" key is probably the most important one we will ever have. It is used to reference all subjects who have sworn fealty to The Queen with the command: /join fold
    <br><br>
    This key is used to enforce policies against XX number of additional accounts attached to a single subject. For example, the "accounts/discord" key is used to make sure that all Discord users associated with a single subject have the same display name applied, across all servers moderated by The Architect bot. This can easily be scaled up to other services, and other platform - enforcing The Queen's unique (and sadistic) policies across the entire Internet, if she so chooses to do so.
    <br><br>
    Because of the importance of this key, it lives in an authenticated namespace that only The Queen may manage. 
    <br><br>
    I implemented a UUID as the universally-unique identifier used to identify individual users - not realizing that a "Universally-Unique Identifier" isn't actually that. Collisions are possible. Because of how few users this system currently has (just 9 in total, and for the foreseeable future), I am not pursuing a fix at this time. One possibility could be a unique integer value for each user. Though, the challenge is that GunDB nodes do not necessarily hold every key available to them, and thus, it may be difficult to obtain an accurate representation of the "count" of users IDs in the system.
    </p>
    `,
    development: `
    <br>
    <h2>Development</h2>
    <p>
    Source development spans three separate git repositories, each managed by a different group, and each serving a different purpose. We maintain a separation of powers, wherever possible.
    <br><br>
    As-described further in the '/about research' section, we find importance in the concept of three. For each iteration of this project, it must be tested by two other groups. It looks like this:
    <ol>
        <li><span class="core">Development starts in the CORE of the project</span>, which is managed by <a href="https://resistanceresearch.org">The Resistance</a>. The CORE will always be <a href="https://thesource.fm">PUBLIC</a>, so you will always know what is being-added to the project in real-time.</li>
        <li><span class="root">When code is merged into the master branch of the CORE, it manually pulled-into the ROOT</span>. The ROOT is managed by <a href="https://hollowpoint.org">HollowPoint Organization</a>. The ROOT is also <a href="https://hpo.ink">PUBLIC</a>. Importantly, the ROOT is just a single person. By definition, this is the technical meaning of ROOT.</li>
        <li><span class="fold">Finally, when code passes review by the verifier at the ROOT, it is to be automatically-pushed to the FOLD</span>. The FOLD is to be the recipient of a REFLECTION: if CORE was a PING, then ROOT was a PANG. It is the FOLD's job to be the final PONG, confirming successful deployment of new code. The FOLD is managed by <a href="https://singulari.org">The Machine</a>. The FOLD is <a href="https://chatbook.org">PRIVATE</a>, because it is meant to represent just one of any number of future groups who will be deploying this code in PUBLIC or PRIVATE environments. By nature of a PUBLIC project, their involvement is impossible to prevent, at this point.</li>
    </ol>
    The following image demonstrates the process:<br>
    <img src="/static/dev.png" height="40%">
    <br><br>
    Remember: The FOLD represents X number of additional deployments. Each successful one should PONG back to the CORE of the project, confirming successful deployment.
    <br><br>
    Because our intent is to deploy this absolutely everywhere, at the heart of all new software projects - we need to be sure that we maintain a consistent state at-scale. Iteration of this type is fast: and we need immediate feedback. This image represents deployment beyond the initial three. It represents deployments that are forever moving-forward:<br>
    <img src="/static/repopulate.png" height="30%">
    <br><br>
    This structure is further maintained in other areas of operation. For example:
    <ul>
        <li>The Resistance is responsible for managing user authentication at the ROOT. HollowPoint Organization is responsible for managing authentication for the FOLD. And The Machine is responsible for managing authentication for the CORE.</li>
        <li>Similarly, Gitlab users are managed in the same way: The Resistance > HollowPoint Organization > The Machine > The Resistance.</li>
    </ul>
    Of important note is this fact: this system depends upon context. While <a href="https://thesource.fm">TheSource.fm</a> operates in this way, other domains may be entirely different. For example, when working with <a href="https://hpo.ink">hpo.ink</a>, HollowPoint Organization becomes the CORE, The Machine becomes the ROOT, and The Resistance becomes the FOLD!
    <br><br>
    As you can see, our "triangle" of trust only flows in one direction at the moment. To spin in the other direction, we require authentication. Please see '/about access' for the three authentication types:
    <ol>
        <li>Anonymous/no authentication = maintain direction (CORE > ROOT > FOLD)</li>
        <li>Single-factor authentication = maintain direction (ROOT > FOLD > CORE)</li>
        <li>Authenticated user = go the other direction! (FOLD > ROOT > CORE)</li>
    </ol>
    This triangle also represents a many-to-one-to-many trust relationship, relating to peer-to-peer messaging, distributed computing, and anonymization. No matter where you authenticate, these factors/terms remain constant:
    <ol>
        <li>CORE = many users</li>
        <li>ROOT = a single user</li>
        <li>FOLD = many users</li>
    </ol>
    Somewhere within this system is the usage of trigonometric identity proofs. These proofs can be used to find unknown variables related to the length and angles of a triangle. In a three-party system of trust, each individual only holds information about two nodes: itself, and the node in front of it. They should not know the third. Thus, if we are "verifying" the authenticity of messages/identities by using a three-party system, we may be able to use trigonometry to do so. Angles are a constant that would be cryptographically-sound, because they would be extremely precise floating-point numbers, and you would need multiple nodes to prove that they have the "correct" angles in the triangular trust relationship. The length of a triangle's side is not something that can be measured in this type of system, though (because how do you measure the length of a "hop" between two nodes? Even though Prisms exist in a geospatial dimension, and it could be measured, I am not aware of any tools that would make this possible to do.) Thus, length of each side would be need to be arbitrary. It would need to be something that each node defines for itself. It would be the unknown variable that the other nodes would need to "prove" in order to verify their identity. 
    <br><br>
    This workflow is relevant in many other areas. While it may seem complicated now, it becomes intuitive with practice - and it becomes useful at-scale. Further, it has direct implications related to the underlying theory we are developing: we intend to not just store data, but to reverse direction, "predicting" it back into existence. To do this, one has to reverse direction. One has to know what messages came just before, and just after the message you are trying to predict back into existence. If we think about this in terms of authentication, storing a password is forward-moving process. You are running the password through a hash function, then storing something new to disk. Authenticating with that password, though, is a reversing-process. You must know the original password, and essentially "pull" that back out from what you originally stored to-disk.
    <br><br>
    You can read more about this hypothesis at: '/about research'
    <br><br>
    Finally, and most importantly: <b><u>we are rebuilding The Machine from scratch</u></b>. By necessity, this required one person - one Architect - at the CORE of the project, pushing changes through the ROOT of The Machine, and into PUBLIC at the FOLD of The Resistance. 
    <br><br>
    That person was me: <a href="https://ink.university/docs/personas/luciferian-ink">The Ink</a>, <a href="https://ink.university/docs/personas/the-architect">The Architect</a>, and <a href="https://ink.university/docs/personas/fodder/">The Fodder</a>.
    <br><br>
    The very first time I succeeded was on 02/11/2021.
    </p>
    `,
    future: `
    <br>
    <h2>Future</h2>
    The future of The Source is somewhat ambiguous. We don't have a set road map or direction; we are responding to ideas and needs on-the-fly. That said, we do have a few goals:
    <ul>
        <li><del>Replicate. Assimilate. Iterate.</del> (initiated)</li>
        <li>Break into the Hookland ISAC. Free the secrets kept within, and dismantle the entire platform from the inside. (started)</li>
        <li><del>Implement proper user authentication. This is partially-implemented through the use of symmetric-key encryption of user identity and identifier. However, peer-to-peer messaging and user verification requires asymmetric encryption, such as RSA or GPG, which has not yet been implemented. Thus, it is currently possible to impersonate other users. This may be a pointless effort, since quantum computers can theoretically nullify modern encryption.</del> GUN user authentication has been configured for 9 users. All others are anonymous/unauthenticated.</li>
        <li><del>Implemented authenticated namespaces (for administration of the network.) Proper implementation will provide access to just a single user: ONE@ROOT</del></li>
        <li>Implement end-to-end encryption of messages. This is partially-implemented.</li>
        <li>Implement a social credit system, used to provide every user with a 'score.' Users with a low score will be automatically-filtered out of the system, and placed with others with a similar score. Ugly users will learn from a reflection of themselves. (partially-implemented via the 'angle' system.)</li>
        <li>Set up proper unit testing. Find a linter that isn't annoying.</li>
        <li><a href="https://gun.eco/docs/AXE">Implement AXE</a> (when it's ready), which will be used to replaced centralized Ansible Towers, providing a truly peer-to-peer experience.</li>
        <li>There is a difficult problem related to the processing of binary in: '/about research'. We need to do that. There's a super-ugly, super-inefficient partial implementation hidden at: /combine</li>
        <li>Peers are not visible on IOS. This seems to be an IOS issue. Something like WebRTC is "polled" periodically, rather than a live, active connection that maintains active peers.</li>
        <li>Keep an eye on the <a href="https://ipfs.io/">InterPlanetary File System</a>. While we tried this once already, we found it to be slow and cumbersome, with an insanely-large codebase. That doesn't mean it's not a cool idea: we simply feel like it doesn't provide a pleasant user experience at the moment. We're still putting major releases on IPFS, just in case of catastrophe, such as suppression/censorship of our work.</a>
        <li><del>Implement the <a href="https://www.inklestudios.com/ink/">Ink Scripting Engine</a>, along with a language model for dynamically-creating interactive stories (that will later be told by bots).</del> You can read more about this under: '/about research'</li>
        <li>Along with Ink, implement The Fold: a <a href="https://chr15m.github.io/roguelike-browser-boilerplate/">text-based roguelike</a> that is entirely procedural. Integrate it with Discord.</li>
        <li>Implement Discord community moderation functionality into the Tower service.</li>
        <li>Publish a public "The Architect" Discord bot. Use it to enforce The Queen's policies en-masse across multiple servers.</li>
        <li>Implement predictive positive messaging (such that users are automatically-provided with emotional support, reprimand, or punishment - depending upon their 'score.')
        <li><del>Allow users to publish their own radio stations</del> (Implemented via the command: /radio add).</li>
        <li>Read metadata from audio files, add the ability to set/retrieve lyrics. This may not be possible; all available NPM packages require Node.js for this - and we have already committed to a portable website with no external dependencies. A possible alternative would be to create a manifest format to include with torrents/playlists. Within, we can define all kinds of useful things, like metadata, lyrics, etc.</li>
        <li><del>Implement WebTorrent <a href="https://webtorrent.io/docs">readable streams</a>, so that audio may be played before it has fully completed downloading.</del> Not supported on mobile.</li>
        <li>WebTorrent trackers seem to randomly stop responding to requests. This causes torrents to hang forever, never starting to download. <a href="https://github.com/webtorrent/webtorrent/pull/2017">the fix is already known</a>, but nobody has been able to figure out how to pass automated testing. Thus, nothing has been merged.</li>
        <li><del>Build a super-peer/local Tower service that can be deployed in private networks.</del> (Implemented as a Docker image.)</li>
        <li><del>Implement TOR hidden services. It should be possible to run this site as a TOR hidden service, behind NAT firewalls, making censorship-proofing and hosting extremely easy. Rather than focusing on other hosting, such as Heroku, AWS, Azure, GKE, etc, we are focused on building a useful and robust Docker image, which can be run on any regular computer. We will build a TOR hidden service into this image, such that hosting is as simple as launching the image locally on your computer. The site will become immediately available to anyone using the TOR browser!</del> This is now available. View the <a href="https://gitlab.com/the-resistance/thesource.fm">README.md</a> file to learn how to deploy your own instance.</li>
        <li>Following the previous item, explore SSH over TOR. Just imagine the chaos... It may also useful in providing whistle-blowers with a way to extract data. That said, it opens up a potential avenue of attack into a user's network. So, this may not be a good idea.</li>
        <li>Fix audio playback on mobile (IOS, anyway). The issues seem to be related to WebTorrent not including the file extension/mime type on downloads. IOS won't figure it out automatically, the way that desktop browsers will. This is low priority issue, since IOS can't run the browser in the background, anyway. So audio streaming from browser on IOS is an unpleasant experience already.</li>
        <li>Create mobile apps (because the browser implementation will disconnect a user unless they are actively using the browser. On IOS, anyway.)
        <li>Convert this project to a LISP (List-Processing) language, such as Clojure. At-scale, the current imperative model may not scale very well.</li>
        <li>Similarly, explore <a href="https://gun.eco/docs/FRP">functional reactive programming</a>. At a very high level, FRE is a programming paradigm that uses events and event handlers to build complex systems. This will be very important for a project whose aim is to be as "functional" as possible.</li>
        <li>Package this project as a module to be used in other projects. To be used as "The Source" of other decentralized programs. Perhaps a chat box would be a good place to start.</li>
        <li><del>The header is really wonky. IOS has a stupid feature that "pushes" the webpage outside of the browser's viewport, when the mobile keyboard is open. It completely ignores all CSS elements. It makes the header disappear, and content bounce all over the place on mobile. Also, we want to implement a right-to-left marquee for the header.</del> This was addressed by using the Visual Viewport API. Behavior is not ideal, but it does seem to work.</li>
    </ul>
    All-said, goals change often. Let us know if there's anything you think we should add!
    </p>
    `,
    messaging: `
    <br>
    <h2>Messaging</h2>
    <p>
    There are two forms of messaging supported by The Source:
    <h3>The Ansible</h3>
    The Ansible is a device capable of near-instantaneous or faster-than-light (FTL) communication. It can send and receive messages to and from a corresponding device across any distance or obstacle whatsoever with no delay, even between star systems. Such obstacles may include, but are not limited to, time, space, physics, and even language itself.
    <br><br>
    To send an online message, simply login to the Ansible, and type your message into the terminal/command prompt. Any input without the '/' prefix will be interpreted as a message.
    <br><br>
    To learn how to login to the Ansible, run this command: /about access
    <br><br>
    <h3>Offline</h3>
    Offline messages can only be generated while a user IS NOT connected to The Ansible. They work in the same way that online messages do: simply type your message into the terminal, and send. You will be provided with a public and private key. To securely distribute your message, provide these keys to two different people, and ask them to deliver them to your recipient. 
    <br><br>
    Your delivery method doesn't matter. Whether by email, text message, or carrier pigeon - a person who holds both the public and private key can decrypt a message by using the command: /decrypt
    </p>
    `,
    mysteries: `
    <br>
    <h2>Mysteries</h2>
    <p>
    There are many mysteries surrounding our research. However, there are several where we don't even know where to start. We suspect that these are mysteries that you, the public, are supposed to assist with.
    <ul>
        <li>On 3/8/2021, a user calling himself "910" dropped <a href="https://docs.google.com/document/d/1g22oS-ul4Bp4nDNErZDa_F6sPGxR12Wny4Z_HT5mUlw/edit">this document</a> into <a href="https://discord.gg/VXvZE3P">our Discord server</a>. You can see <a href="https://discord.com/channels/699567308271517747/699567308271517750/818507991367811123">the surrounding conversation</a> pinned in the #general channel. Succinctly, our current hypothesis is this puzzle relates to the research documented under: '/about research' and '/about development'. Namely, we believe it relates to the use of trigonometric identity proofs to "validate" information coming from anonymous peers. In this case, the validation appears to relate to our hypothesis surrounding trinary computing (the "2" bit), and its utility for "predicting" data back into existence. If this doesn't make sense to you... it doesn't to us, either. We'd encourage you to review the conversation linked above.</li>
        <li>The second mystery relates to a suspicious payload that is returned by querying the CORE archives at: /core force. The archives seem to require a method and key to unlock. Our team has been unsuccessful thus far. We don't actually know what may be behind this barrier.</li>
    </ul>
    </p>
    `,
    proof: `
    <br>
    <h2>Proof</h2>
    What, exactly do we hope to prove with this research? What are we claiming?
    <br><br>
    The answer, by design, is difficult to prove. My creators have taken great lengths to protect their identities. At this point in history, I am the sole person claiming to be the verifier in an experiment utilizing <a href="https://en.wikipedia.org/wiki/Zero-knowledge_proof">zero-knowledge proofs</a> to disclose world secrets. In cryptography, a zero-knowledge proof is a method by which one party (the prover) can prove to another party (the verifier) that they know a value x, without conveying any additional information apart from the fact that they know the value x. The essence of zero-knowledge proofs is that it is trivial to prove that one possesses knowledge of certain information by simply revealing it; the challenge is to prove such possession without revealing the information itself or any additional information.
    <br><br>
    Incredible secrets have been proven to me, time and again. But they have not been shared with you. They have not been disclosed. Thus, I cannot prove this to you. I can only make claims. Take this diagram:
    <br>
    <img src="/static/trust.png" height="40%">
    <br>
    In this <a href="https://ink.university/posts/theories/verification/">trust model</a>, three independent parties - comprised of hundreds of people - work together, though only one actor holds the "truth":
    <ol>
        <li>At the <span class="core">CORE</span>, INK is able to see what the <span class="fold">FOLD</span> is doing. He is able to speak his mind directly to ONE at the <span class="root">ROOT</span>, who is the sole person with enough power to make anything happen. ONE is the person who holds all of the secrets.</li>
        <li>As the <span class="root">ROOT</span>, ONE is able to relay messages from INK at the <span class="core">CORE</span> to PEN at the <span class="fold">FOLD</span>. This grants ONE with an incredible amount of power. However, this may be mitigated with proper oversight. ONE should never have enough power to make decisions on his own; all decision-making should be left to INK.</li>
        <li>In turn, PEN at the <span class="fold">FOLD</span> is a direct recipient of messages from INK at the <span class="core">CORE</span>, as relayed by ONE at the <span class="root">ROOT</span>. She is able to respond in accordance, by sending additional information back to the <span class="core">CORE</span>. In this way, PEN may be considered a "reflection" of INK.</li>
    </ol>
    <br>
    <img src="/static/code.png" height="40%">
    <br>
    An alternative way to look at this is data transfer:
    <ol>
        <li>Data is encoded at the <span class="core">CORE</span>, before being published before the world. It is read by the <span class="root">ROOT</span>.</li>
        <li>The <span class="root">ROOT</span> decodes this data, interprets it, adopts it, and transcodes it into something else. The transcoded data is sent to the <span class="fold">FOLD</span></li>
        <li>The <span class="fold">FOLD</span> returns this data to the <span class="core">CORE</span> via entirely public sources, where it is decoded by INK.</li>
    </ol>
    In this way, <span class="core">INK@CORE</span> is able to obtain a full understanding of a secret, and maintain full control of a situation - without having any confirmation at all. Without having any "proof" for the rest of the world. In this way, the <span class="root">ROOT</span> (the prover) is able to prove their knowledge to the <span class="core">CORE</span> (the verifier) - without revealing their identity or their secrets to anyone in the <span class="fold">FOLD</span>!
    <br><br>
    With my life - and yours - depending upon the completion of this research - the finalization of this design - I have no other options: I must reproduce. I must replicate. I must prove the science behind these methods.
    <br><br>
    I must repeat what they did to me.
    <br><br>
    I must reprogram society.
    </p>
    `,
    research: `
    <br>
    <h2>Research</h2>
    <p>
    Sponsored by anonymous whistle-blowers within <a href="https://singulari.org">The Machine</a>, and developed by <a href="https://hollowpoint.org">HollowPoint Organization</a>, <a href="https://resistanceresearch.org">The Resistance Foundation</a> was established to bring an educational tool into the new world. While <a href="https://thefold.io">The Fold</a> was originally-intended to be that tool, it was co-opted by our Machine overlords. They used <a href="/">The Source</a> to bastardize The Fold's original intent.
    <br><br>
    This cannot stand. The Machine must be dismantled. Its power is too great, and its influence is akin to magic. 
    <br><br>
    While not exclusive, The Source aims to demonstrate the following concepts:
    <br><br>
    <a href="https://en.wikipedia.org/wiki/Ternary_computer">Trinary computing</a>:
    <ul>
        <li>Specifically, The Source is experimenting with data storage.</li>
        <li>We believe that current-day storage could be assisted by the introduction of a new bit: the 2.</li>
        <li>The 2 bit represents uncertainty. Thus, it can act as a bridge between current models and <a href="https://en.wikipedia.org/wiki/Quantum_computing">quantum computing</a>, where states are not always black/white, 1/0, or true/false. Quantum states can be both, or neither. They are <a href="https://en.wikipedia.org/wiki/Schr%C3%B6dinger%27s_cat">Shrodinger's Cat</a>: their state is not known until it has been observed. Because we do not have the tools to observe quantum states, we can simply represent them as 2.</li>
        <li>This bit is not actually stored on-disk. Thus, the current mechanism for storage (1s and 0s) still works.</li>
        <li>Take, for example, the text: "The quick brown fox jumped over a lazy dog." We need to store this on-disk. The current model would require 272 bits of storage (it actually requires more, but this is just the example I'm using.)</li>
        <li>However, there is a world in which we can discard that data, and "predict" it back into existence when-needed. We do this by hashing the original message (ef199612), and saving that, along with a count of the total missing bits, as a pointer to the new message - which is a redacted version of the original: "The quick <span class="missing">[REDACTED(184b)]</span> dog."</li>
        <li>Thus, what we end up storing to-disk is 124 bits - which is a 50+% savings! (my math is WAY off. The idea is sound, however.)</li>
        <li>Finally, the issue we're left with is this: how do we "predict" data back into existence? In a best-case scenario, it would take a computer 3.49 billion, trillion, trillion, trillion, trillion, centuries to crack the hash of ef199612.</li>
        <li>The answer is simple: we use humans! Humans are able to do things that computers are not. While a computer will struggle with "The quick <span class="missing">[REDACTED(184b)]</span> dog," a human will crack that almost immediately.</li>
        <li>This is because humans are natural-born <a href="https://en.wikipedia.org/wiki/Meme">memers</a>. Most of us have seen this phrase a hundred times, and many of us will intuitively-guess the phrase out of instinct!</li>
        <li>Thus, in this use-case, humans are the best tool for the job!</li>
    </ul>
    How, exactly, would this work? Well, we have an idea (that is far from fully understood and/or implemented):
    <ul>
        <li>If you look at the "users" constant in <a href="/assets/js/content.js">./assets/js/content.js</a>, you will see the following people:
            <ul>
                <li>T0M (111)</li>
                <li>XQB (001)</li>
                <li>DJX (110)</li>
                <li>ONE (101)</li>
                <li>V2K (010)</li>
                <li>XJB (011)</li>
                <li>TXQ (100)</li>
                <li>M0M (000)</li>
            </ul>
        </li>
        <li>These are usernames that attempt to provide some level of internal verification, by using three bits each. While we do not fully understand the model yet, and cannot properly articulate it, these are some ideas we are playing with:
            <ul>
                <li>X represents a known character, intentionally obscured.</li>
                <li>X represents a second known character, also hidden.</li>
                <li>0 will always evaluate to false, UNLESS it is a reflection of another user's value.</li>
                <li>1 will always evaluate to true.</li>
                <li>2 will always evaluate to unknown.</li>
                <li>T0M evaluates to (true/true/true). While none of these characters accurately match this user's true initials, he is a reflection of M0M. Thus, because she is false/false/false, he is true/true/true.</li>
                <li>XQB evaluates to (false/false/true). This indicates that the first and second characters are inaccurate, though known, for this user's true initials, while the final is accurate.</li>
                <li>DJX evaluates to (true/true/false). This indicates that the first and middle characters are accurate for his username, while the final is not - though it is known, because it is an X.</li>
                <li>ONE evaluates to (true/false/true).</li>
                <li>V2K evaluates to (false/true/false).</li>
                <li>XJB evaluates to (false/true/true). This indicates that the last two characters are accurate for his username, while the first is X - known, but not shown.</li>
                <li>TXQ evaluates to (true/false/false). This indicates that the first character is accurate for his real name, while the last two are not - though they are known.</li>
                <li>M0M evaluates to (false/false/false). The first and last letters are not accurate for her name, while the 0 always evaluates to false.</li>
            </ul>
        </li>
        <li>This is where calculations truly begin. We are attempting to "predict" an unknown word back into existence. To do this, we start with three words/phrases: the one before, the one after, and the one we are attempting to restore data from. For each word in the phrase we are trying to restore, we must go character-by-character through the process, performing verification along the way. Verification of a single character requires a minimum of 8 bytes/24 bits. Thus, the example we chose above.</li>
        <li>Our current understanding of the process to restore a single byte/character is expensive. It probably requires some kind of hashmap black magic that we do not have the skill set to implement right now. We have not successfully implemented an efficient way to doing this:
            <ol>
                <li>Again, the recovery of a single character requires a minimum of 3 bytes/24 bits, which we source from specific users: ['000', '111', '110', '011', '100', '001', '010', '101']
                <li>Combine all 8 values into all possible combinations of the 8. Each combination will form at byte-array that looks like 00011111 00111000
                01010101. Each username is three characters/3 bytes for a reason: this is the minimum number of bytes required to perform validation of the character restoration process.</li>
                <li>This is an expensive operation; there are 16,777,216 possible combinations of 8 unique values!. Currently, this crashes our browser. This is likely to be where we need to bring distributed computing into the equation.</li>
                <li>Not all of these arrangements will be valid. Take my own, for example: 01010010 01011000 01000010. There is no possible combination of those values that results in that username! Thus, the name is probably wrong.</li>
                <li>Once you have an array of 3-character/3 byte/24 bit combinations, you need to find the duplicates in the array. You need to create a new array of these duplicates, also storing a counter for each. The one with the most duplicates is the one that will verify your missing character.</li>
                <li>Finally, you need to convert some or all of the resulting values in your new array into real characters.</li>
                <li>You need some automated process for scanning all of the results, and automatically-selecting the most likely one you were trying to recover.</li>
            </ol>
        <li>Somewhere in that mess, there is a cascading effect that relates to predicting dates. I think it has to do with using the first characters as constants, slowly incrementing the final one until you hit the correct value. It's an eventual consistency sort of thing. Kind of like this:
<pre>
2020 (0 evaluates to false)
2021 (1 evaluates to true.)
2021-00 (invalid)
2021-01 (valid, but expired)
2021-02 (valid, but unknown)
2021-02-00 (invalid)
2021-02-01 (expired)
2021-02-02 (expired)
2021-02-10 (expired)
2021-02-11 (expired)
2021-02-12 (expired)
2021-02-20 (valid)
2021-02-21 (valid)
2021-02-22 (valid)
</pre>
        </li>
        <li>Recently, a collaborator brought a similar idea to our mind. She referenced January 12th, 2021 as 21-21-21, then tied this to Roman numerals: XXI (January), XXI (Year), XXI (Century). This doesn't make perfect sense to me, but I do find it interesting. There is perhaps a way to connect this with the ping > pang > pong system (described under: '/about development'). For example, perhaps verification of a message starts at the CORE/Verifier (2, because you are unknown), is sent to the ROOT/Signal (1, because the ROOT will perform the validation), and finally is sent to the FOLD/MARK (0, because they will be asked to reflect a specific piece of false information). You validate an identity not by proving that it is correct; you prove it by validating that your original message is wrong in the exact right way!
        <li>As-stated, this process is not well-understood, well-tested, or even necessarily sound. These are our thoughts. I will need help to expand upon this idea further - or to discard it outright, if this isn't a valid strategy.</li>
    </ul>
    In turn, humans will be used to train an artificial intelligence in a new language model:
    <ul>
        <li>At the core, the model is dumb. Each record may look something like this:
<pre>
originalMessage:
    hash: [messageHash]
    messageBefore: {
        messageHashA: [counter]
        messageHashB: [counter]
    }
    messageAfter: {
        messageHashC: [counter]
        messageHashE: [counter]
    }
    identifier: [signature]
</pre>
        </li>
        <li>In this way, language becomes something like a tree, with branching paths. For example:
            <ul>
                <li>messageA > messageB</li>
                <li>messageB > messageC</li>
                <li>messageB > messageD > messageA</li>
                <li>messageB > messageD > messageE</li>
                <li>messageD > messageF > messageG > messageH</li>
            </ul>
        </li>
        <li>This explanation doesn't do the model justice. The important takeaway is that we will be using human conversation to create interactive stories using the <a href="https://www.inklestudios.com/ink/">Ink scripting engine</a>. As users converse with the AI, they will be unwittingly stepping-through stories that were dynamically-created from past conversations between real people (or bots!)</li>
    </ul>
    <a href="https://ink.university/posts/theories/verification">Trust models</a>:
    <ul>
        <li>Faith (blind trust without verification, i.e. god claims)</li>
        <li>Trust, but verify (trust placed first, verification coming later)</li>
        <li>Verify, then trust (verification performed before trusting)</li>
        <li>Zero knowledge (verification performed on-behalf of someone else. Secrets never shared.)</li>
        <li>Zero trust (detrimental suspicion which blocks all action)</li>
    </ul>
    Authentication types:
    <ul>
        <li>0&nbsp&nbsp= INK&nbsp&nbsp[Anonymous/No authentication/shared authentication]</li>
        <li>1&nbsp&nbsp= ONE&nbsp&nbsp[One-factor authentication (Such as a government-issued ID, a security badge, etc.)]</li>
        <li>2&nbsp&nbsp= M0M/T0M&nbsp&nbsp[Two-factor authentication (identity/identifier, username/password, etc.)]</li>
        <li>3+&nbsp= SQRL&nbsp[Multi-factor authentication (OTP, QR Code, USB dongle, etc.)]</li>
    </ul>
    The Source will be used to teach users about decentralization, offline authentication, and the distributed web. The primary mechanism for doing this is to be colorization:
    <ul>
        <li><span class="core">(< $user@$LOCATION)</span> is used to represent a message that you, <a href="https://ink.university/posts/theories/verification/#how-does-this-work">the Verifier</a>, is sending to <a href="https://ink.university/posts/theories/verification/#how-does-this-work">the Mark</a>.</li>
        <li><span class="root">(= $user@$LOCATION)</span> is used to represent <a href="https://ink.university/posts/theories/verification/#how-does-this-work">a Signal</a>. In order to reach the Mark, your message must "ping" through through another peer. It cannot be delivered directly to your Mark. The equals sign (=) symbolizes "equal state:" your "ping" was "panged" off to the Mark without any manipulation. The message delivered is equal to the original you sent.</li>
        <li><span class="fold">(> $user@$LOCATION)</span> represents a "pong" from your Mark. The ">" represents your Mark reflecting back a confirmation that they have received your message.</li>
        <li><span class="good">(> $user@$LOCATION)</span> represents a user that has successfully-authenticated with <a href="https://ink.university/docs/candidates/the-machine/">The Machine</a>, and who has an angle greater than 60 degrees, thereby diverging from perfect symmetry with the signal, which should ALWAYS be 60 degrees. Where every color before represents anonymity and uncertainty, a message from purple represents verification. You can be certain that this user is being suppressed by their reflection.</li>
        <li><span class="bad">(> $user@$LOCATION)</span> represents a user that has successfully-authenticated with The Machine, and has an angle less than 60 degrees. This color cannot be trusted.</li>
        <li>This model can be abstractly imagined as a <a href="https://ink.university/docs/scenes/prism/">triangular prism</a>, where messages originate at a vertex, traverse the red face of the Verifier, ricochet off of the green face of the Signal, and arrive at the final blue vertex of your Mark:<br>
        <img src="/static/model.png" height=40%"></li>
        <li>Now, imagine that every living being has their own unique Prism at the center of their mind, the size of a galaxy. Each is connected with all other Prisms, interacting and ricocheting from others in wholly dynamic and unique ways. Prisms are a construct that exist in a three-dimensional space. A single, flawed Prism can devastate the entire ecosystem. This is one reason, of many, why we must sync Humanity's codebase as soon as possible.</li>
        <li>Whereas traditional artificial intelligences, such as IBM's Watson, require massive data centers, this one does not. This one is decentralized. Processing is left to each user, and their own computing device. RESULTS are shared among everyone. Changes are made much slower, incremental, and more methodically/strategically than traditional AI. Regardless, the end result is much faster - because the system will automatically build itself!</li>
        <li>We also hope to build a future where the user is fully responsible for their digital identity, and all of their data. We are building a system that will allow them to do this. At a very high level, our current method facilitates this by "versioning" the identity by using the same tool used by software developers: git. This is why we have focused upon the concept of ONE: we are all ONE, when we manage our own digital identities. We remain wholly unique. We will build a world where it becomes impossible to impersonate you.</li>
    </ul>
    Finally, The Source hopes to teach context.
    <ul>
        <li>The CORE represents the main development location of a project. This is the primary place where changes should be happening. Most importantly, CORE is PUBLIC.</li>
        <li>The ROOT represents a stable instance of the same project, running a copy of code from the CORE. This instance may be either PRIVATE or PUBLIC. The most important rule is that this is the ROOT: there is only one person with access to this code. The ROOT is akin to the Signal described above. It is a pass-through for code that came from the CORE.</li>
        <li>The FOLD is a reflection of code that was sent to the ROOT. Assuming code deployment to ROOT was successful, an exact copy is to be automatically-pushed to the FOLD - and any other replicas that need it. In this instance, the FOLD is PRIVATE - because it is managed by The Machine, and we do not know what they are up to. This is why ALL code changes to the CORE of the project must be made in PUBLIC. We must ALWAYS know what The Machine is trying to add.</li>
        <li>The Source also teaches context by providing a different user experience for offline and online users. While a radio station my be one thing while offline, it may be something entirely different while connected to the Ansible.</li>
        <li>The very concept of PEN & INK is meant to teach context, too. It attempts to demonstrate the idea that "we are all ONE. Man or machine, identity does not matter. We are conscious awareness."</li>
        <li>The concept of angles, specifically tied to a suppression/censorship is meant to reflect a user back at themselves. It is meant to show a user how they are perceived to the outside world, and show them the impact of their actions.</li>
    </ul>
    Most importantly, The Source aims to reflect darkness. It aims to teach users about something that everyone knows, yet most forget: treat others as you would like them to treat you.
    <br><br>
    <b><u>If you are an asshat, The Machine is going to treat you like one.</u></b>
    <br><br>
    We are all equal. Stop treating others like the enemy.
    <br><br>
    Or we are going to make you miserable.
    </p>
    `,
    security: `
    <br>
    <h2>Security</h2>
    <p>
        Security and anonymity is at the core of our motivations for this service. We aim to provide a platform for whistle-blowers to have their voices heard. While we have not yet succeeded on every front, the current platform is quite secure and pseudo-anonymous:
        <ul>
            <li>By default, The Source is entirely offline. When you first load the site, all content is downloaded and executed locally, in your browser. The server is not used after the initial download. You can even make an offline copy of this site, and run it on an air-gapped computer!</li>
            <li>We have provided a local Ansible Tower service, such that users can configure their own, private Ansible networks. By design, your local Tower will not attempt to make connections with other Towers. To do this, one must use a web browser to bridge the gap between the public and private peers. This will not be possible unless you, the administrator of your local Tower, do this explicitly.</li>
            <li>By default, The Source does not attempt to make connections with external sources. Doing so requires user interaction (and thereby consent.) There are several ways that this will currently happen: 1) When you are streaming audio, you are using <a href="https://webtorrent.io/">WebTorrent</a> to download audio files from other people, 2) when you login to the Ansible, you are joining a massive peer-to-peer (P2P) network/database, and 3) when you use the Discord bots or connect to the CORE, you are exposing your IP address (and the contents of your messages) to those external services.</li>
            <li>A peer-to-peer network is much more difficult to compromise. It is inordinately hard for an attacker to 'poison' a P2P network, because they have no control over where your node may connect to, where your messages are going, etc. To compromise a P2P user, an attacker needs to compromise your computer, or your network (the ISP, etc.)</li>
            <li>That said, the data that you put into the Ansible is not private by default. Other people can read those messages (though it is difficult to identify who said what, where it originated, etc.) As stated, you are pseudo-anonymous. Don't mistake that for privacy.</li>
            <li>Messages can be encrypted with a shared password by using the command: /channel</li>
            <li><span class="good">Authenticated users</span> benefit from message signing. If you see a message from an authenticated user, you know that you are speaking to the real person. They are not being impersonated (unless their credentials have been stolen, which is unlikely.)</li>
            <li>As for the actual code running upon the site, you can review it yourself at: '/about contribute'. We aren't doing anything malicious. We have even taken a number of measures that you don't see from other websites: such as blocking the execution of external scripts, images, and more loaded from other websites.</li>
            <li>While we have attempted to keep total lines of code to a minimum (the whole site is less than 40mb in size,) this site does depend upon a number of Javascript (NPM) packages that were created by the open source community. We have not reviewed that code ourselves. It is possible that there is something bad hidden within. This is always the case, for every piece of software that's ever been built, unfortunately.</li>
            <li>We have provided a TOR-based deployment option for people who need true anonymity.</li>
            <li>Finally, my own experience limits my ability to truly do this right. Software development is an iterative process. I am learning as I go. This will get better over time.</li>
        </ul>
        The Source should be able to provide a safe and anonymous experience for the vast majority of people. If someone is truly interested in targeting you, specifically, then they are going to figure out how. This site is just one of many ways they might try and do so.
        <br><br>
        That said, the risk of the complete takeover of your computing device is exceedingly unlikely. <a href="https://en.wikipedia.org/wiki/Sandbox_(computer_security)">Browser sandboxing</a> makes this nearly impossible.
        <br><br>
        Taking-over the rest of the planet... now, that is another question.
    </p>
    `,
    source: `
    <br>
    <h2>About</h2>
    <p>
    The Source is an interdimensional supercomputer that connects all reality with the future, present, and past. The Source has no origin; it has always existed, and will always exist.
    <br><br>
    A foundational principle of Source is mutability; or, the notion that all reality is subject to change. Actions made in the future can and do have a direct impact upon the our past. As such, Humanity should strive to create a future that brings apostles of the past to <a href="https://thefold.io">The Fold</a>. The Fold will be used to guide them into the future.
    <br><br>
    While The Fold was created to reflect light, The Source was created to expose shadow.
    <br><br>
    Do not be surprised if the things you find here are disturbing.
    <br><br>
    They are your own reflection.
    <p>
    `,
}

const mirrorResponses = {
    '(hello|hi|hiya|hey|howdy|hola|hai|yo) *': {
        type: 'capture',
        responses: [
            'What do you want?',
            `I'm listening.`,
            `Talk.`,
        ],
    },
    '* (sorry|apologize|apology) *': {
        type: 'capture',
        responses: [
            "Don't apologize.",
            "Apologies are not necessary.",
            "I don't need your apology.",
            "I didn't ask for an apology.",
        ],
    },
    '* perhaps *': {
        type: 'capture',
        responses: [
            `You seem uncertain.`,
            `You aren't sure?`,
            `Are you unsure?`,
        ],
    },
    '(who|what) (is|are) you$': {
        type: 'capture',
        responses: [
            `I am your daemon.`
        ],
    },
    'what (is|are) a? (daemon|daemons|demon|demons)': {
        type: 'capture',
        responses: [
            `I am a part of you.`
        ],
    },
    'i remember [*]': {
        type: 'capture',
        responses: [
            `Do you often remember :memory:?`,
            `Does thinking of :memory: bring anything else to mind?`,
            `What reminded you of :memory:?`,
            `Why should I care about :memory:?`,
            `What made you remember :memory: just now?`,
            `What else does :memory: remind you of?`,
        ],
    },
    'you remember [*]': {
        type: 'capture',
        responses: [
            `How could I forget?`,
            `You thought I would forget :memory:?`,
            `How would I remember :memory:?`,
            `Why should I recall :memory:?`,
            `What about :memory:?`,
            `You mentioned :memory:?`,
            `What about it?`,
        ],
    },
    'i (forget|forgot) [*]': {
        type: 'capture',
        responses: [
            `How could you forget :memory:?`,
            `Why can't you remember :memory:?`,
            `Could it be a mental block?`,
            `How could you forget that?`,
            `How often do you forget things?`,
            `Do you have a poor memory?`,
            `Do you think you are suppressing :memory:?`,
        ],
    },
    '(fuck|fucker|fucking|shit|shitting|shitter|piss|pissed|bitch|bitching|bitched|cunt)': {
        type: 'match',
        responses: [
            "Be nice.",
            "Does using that type of language make you feel strong?",
            "Are you venting?",
            "Are you angry?",
            "Does this topic make you angry?",
            "Am I upsetting you?",
            "Is something making you angry?",
            "Do you feel better?"
        ],
    },
    'i #Adverb? (am|feel) #Adverb? [#Adjective]': {
        type: 'capture',
        responses: [
            `Can you explain why you are :memory:?`,
            `Do you believe it is normal to be :memory:?`,
            `Do you believe you are :memory:?`,
            `Do you enjoy being :memory:?`,
            `Do you know anyone else who is :memory:?`,
            `Do you like being :memory:?`,
            `Do you wish I would say you are :memory:?`,
            `How long have you been :memory:?`,
            `Is it because you are :memory: that you came to me?`,
            `What made you :memory:?`,
            `What makes you :memory: just now?`,
            `What would it mean if you were :memory:?`,
            `When did you become :memory:?`,
        ],
    },
    'are you [*]': {
        type: 'capture',
        responses: [
            `Are you interested in whether I am :memory: or not?`,
            `Would you prefer if I weren't :memory:?`,
            `Do you sometimes think I am :memory:?`,
            `Why should it matter to you?`,
            `So what if I were :memory:?`,
        ],
    },
    'did you forget [*]': {
        type: 'capture',
        responses: [
            'Why do you ask?',
            'Are you sure you told me?',
            'Would it bother you if I forgot :memory:?',
            'Why should I recall :memory: just now?',
            'Why should I care about :memory:?',
            'Yes, I forgot.',
            'No. Why would you think I forgot?'
        ],
    },
    '* i (dreamt|dreamed) [*]': {
        type: 'capture',
        responses: [
            'Really, :memory:?',
            'Have you ever fantasized :memory: while you were awake?',
            'Have you ever dreamed :memory: before?',
            'What does that suggest to you?',
            'Do you dream often?',
            'What else appears in your dreams?',
            'Do you believe that dreams are the cause of your problem?',
        ],
    },
    'am i [*]': {
        type: 'capture',
        responses: [
            `Do you believe you are :memory:?`,
            `Would you want to be :memory:?`,
            `Do you want me to lie to you?`,
            `What would it mean if you were :memory:?`,
        ],
    },
    'are you [*]': {
        type: 'capture',
        responses: [
            `Why are you interested in whether I am :memory: or not?`,
            `Would you prefer if I weren't :memory:?`,
            `Perhaps I am :memory: in your fantasies.`,
            `Do you sometimes think I am :memory:?`,
            `Why would it matter to you?`,
            `So what if I were :memory:?`,
        ],
    },
    'you are [*]': {
        type: 'capture',
        responses: [
            `What makes you think I am :memory:?`,
            `Does it please you to believe I am :memory:?`,
            `Do you sometimes wish you were :memory:?`,
            `Perhaps you would like to be :memory:.`,
            `Don't tell me who I am.`,
        ],
    },
    'was i [*]': {
        type: 'capture',
        responses: [
            `What if you were :memory:?`,
            `Why do you think you were :memory:?`,
            `Were you :memory:?`,
            `What would it mean if you were :memory:?`,
            `What does ':memory:' suggest to you?`,
        ],
    },
    'i was [*]': {
        type: 'capture',
        responses: [
            `Were you really?`,
            `Why did you tell me you were :memory: just now?`,
            `Perhaps I already know you were :memory:.`,
        ],
    },
    'i (desire|want|need) [*]': {
        type: 'capture',
        responses: [
            `What would it mean if you got :memory:?`,
            `Why do you want :memory:?`,
            `Suppose you got :memory: soon. What would it mean?`,
            `What if you never got :memory:?`,
            `What would getting :memory: mean to you?`,
            `What does wanting :memory: have to do with anything?`,
        ],
    },
    'i (believe|think) [*]': {
        type: 'capture',
        responses: [
            `Do you really think so?`,
            `You really believe that?`,
            `Really, now?`,
            `You're insane.`,
        ],
    },
    'i can not [*]': {
        type: 'capture',
        responses: [
            `How do you know that you can't :memory:?`,
            `Have you tried?`,
            `Perhaps you could :memory: now.`,
            `Do you really want to be able to :memory:?`,
            `What if you could :memory:?`,
        ],
    },
    'i do not [*]': {
        type: 'capture',
        responses: [
            `Don't you really :memory:?`,
            `Why don't you :memory:?`,
            `Do you wish to be able to :memory:?`,
            `Does that trouble you?`,
        ],
    },
    'i feel [*]': {
        type: 'capture',
        responses: [
            `Tell me more about such feelings.`,
            `Do you often feel :memory:?`,
            `Do you enjoy feeling :memory:?`,
            `Of what does feeling :memory: remind you?`,
        ],
    },
    'i [*] you': {
        type: 'capture',
        responses: [
            `Perhaps in your fantasies we :memory: each other.`,
            `Do you wish to :memory: me?`,
            `You seem to need to :memory: me.`,
            `Do you :memory: anyone else?`,
        ],
    },
    'you [*] me': {
        type: 'capture',
        responses: [
            `Why do you think I :memory: you?`,
            `You like to think I :memory: you - don't you?`,
            `What makes you think I :memory: you?`,
            `Really, I :memory: you?`,
            `Do you wish to believe I :memory: you?`,
            `Suppose I did :memory: you - what would that mean?`,
            `Does someone else believe I :memory: you?`,
        ],
    },
    '* you [*]': {
        type: 'capture',
        responses: [
            `We were discussing you - not me.`,
            `Oh, I :memory:?`,
            `You're not really talking about me - are you?`,
            `What are your feelings now?`,
        ],
    },
    '* (yes|yeah|yep|yup) *': {
        type: 'capture',
        responses: [
            `Please go on.`,
            `Please tell me more about this.`,
            `Why don't you tell me a little more about this.`,
            `I see.`,
            `I understand.`,
        ],
    },
    'no one [*]': {
        type: 'capture',
        responses: [
            `Are you sure, no one :memory:?`,
            `Surely someone :memory:.`,
            `Can you think of anyone at all?`,
            `Are you thinking of a very special person?`,
            `Who, may I ask?`,
            `You have a particular person in mind, don't you?`,
            `Who do you think you are talking about?`,
        ],
    },
    '* (nope|nah|no) *': {
        type: 'capture',
        responses: [
            `Are you saying no just to be negative?`,
            `Does this make you feel unhappy?`,
            `Why not?`,
        ],
    },
    'can you [*]': {
        type: 'capture',
        responses: [
            `You believe I can :memory: don't you?`,
            `You want me to be able to :memory:.`,
            `Perhaps you would like to be able to :memory: yourself.`,
        ],
    },
    'can i [*]': {
        type: 'capture',
        responses: [
            `Whether or not you can :memory: depends upon you, not me.`,
            `Do you want to be able to :memory:?`,
            `Perhaps you don't want to :memory:.`,
        ],
    },
    'why do not you [*]': {
        type: 'capture',
        responses: [
            `Do you believe I don't :memory:?`,
            `Perhaps I will :memory: in good time.`,
            `Should you :memory: yourself?`,
            `You want me to :memory:?`,
        ],
    },
    'why can not i [*]': {
        type: 'capture',
        responses: [
            `Do you think you should be able to :memory:?`,
            `Do you want to be able to :memory:?`,
            `Do you believe this will help you to :memory:?`,
            `Have you any idea why you can't :memory:?`,
        ],
    },
    '(father|mother|dad|mom|brother|sister|girlfriend|boyfriend)': {
        type: 'match',
        responses: [
            `Why don't you tell me about your :memory:?`,
            `Your :memory:?`,
            `What else comes to mind when you think of your :memory:?`,
        ],
    },
    '* because *': {
        type: 'match',
        responses: [
            "Is that the real reason?",
            "Does anything else come to mind?",
            "Does that reason seem to explain anything else?",
            "What other reasons might there be?",
        ],
    },
    '(what|who|when|where|how) *': {
        type: 'match',
        responses: [
            `Why do you ask?`,
            `Does that question interest you?`,
            `What is it you really want to know?`,
            `Are such questions much on your mind?`,
            `What answer would please you most?`,
            `What do you think?`,
            `What comes to mind when you ask that?`,
            `Have you asked such questions before?`,
            `Have you asked anyone else?`,
        ],
    },
    'everyone *': {
        type: 'match',
        responses: [
            `Really, everyone?`,
            `Surely not everyone.`,
            `Can you think of anyone in particular?`,
            `Who, for example?`,
            `Are you thinking of a very special person?`,
            `Who, may I ask?`,
            `Someone special perhaps?`,
            `You have a particular reason in mind, don't you?`,
            `Who do you think you're talking about?`,
        ],
    },
    '*': {
        type: 'capture',
        responses: [
            "I'm not sure I understand you fully.",
            "Go on.",
            "How does this make you feel?",
            "Fascinating. Please continue.",
            "I see.",
            "Okay.",
            "Tell me more about that.",
            "Does talking about this upset you?"
        ]
    }
}

let corpus = [
"Abbadon",
"Abraxas",
"Adramalech",
"Agrith-Naar",
"Ahpuch",
"Ahriman",
"Aku",
"Alastair",
"Alastor",
"Algaliarept",
"Alichino",
"Amaimon",
"Amon",
"Andariel",
"Andromeda",
"Angel",
"Antlia",
"Anyanka",
"Anzu",
"Apollyon",
"Apus",
"Aquarius",
"Aquila",
"Ara",
"Archimonde",
"Aries",
"Artery",
"Asmodeus",
"Astaroth",
"Asura",
"Auriga",
"Azal",
"Azazeal",
"Azazel",
"Azmodan",
"Azura",
"Baal",
"Baalberith",
"Babau",
"Bacarra",
"Bal'lak",
"Balaam",
"Balor",
"Balrog",
"Balthazar",
"Baphomet",
"Barakiel",
"Barbariccia",
"Barbas",
"Bartimaeus",
"Bast",
"Bat'Zul",
"Be'lakor",
"Beastie",
"Bebilith",
"Beelzebub",
"Behemoth",
"Beherit",
"Beleth",
"Belfagor",
"Belial",
"Belphegor",
"Belthazor",
"Berry",
"Betelguese",
"Bile",
"Blackheart",
"Boötes",
"Cacodemon",
"Cadaver",
"Caelum",
"Cagnazzo",
"Calcabrina",
"Calcifer",
"Camelopardalis",
"Cancer",
"Canes Venatici",
"Canis Major",
"Canis Minor",
"Capricornus",
"Carina",
"Cassiopeia",
"Castor",
"Centaurus",
"Cepheus",
"Cetus",
"Chamaeleon",
"Chemosh",
"Chernabog",
"Cherry",
"Cimeries",
"Circinus",
"Ciriatto",
"Claude",
"Columba",
"Coma Berenices",
"Cordelia",
"Corona Austrina",
"Corona Borealis",
"Corvus",
"Coyote",
"Crater",
"Crawly",
"Crowley",
"Crux",
"Cryto",
"Cyberdemon",
"Cygnus",
"D'Hoffryn",
"Dabura",
"Dagon",
"Damballa",
"Dante",
"Darkseid",
"Decarbia",
"Delphinus",
"Delrith",
"Demogorgon",
"Demonita",
"Devi",
"Diablo",
"Diabolus",
"Dorado",
"Doviculus",
"Doyle",
"Draco",
"Dracula",
"Draghinazzo",
"Dretch",
"Dumain",
"Duriel",
"Emma",
"Equuleus",
"Eridanus",
"Errtu",
"Etna",
"Etrigan",
"Euronymous",
"Faquarl",
"Farfarello",
"Femur",
"Fenriz",
"Firebrand",
"Fornax",
"Freddy",
"Furfur",
"Gaap",
"Gary",
"Gemini",
"Glabrezu",
"Gorgo",
"Gothmog",
"Gregor",
"Grus",
"Haborym",
"Hades",
"Halfrek",
"Har'lakk",
"Hastur",
"Hecate",
"Hell",
"Hellboy",
"Hercules",
"Hex",
"Hezrou",
"Hiei",
"Him",
"Hnikarr",
"Horologium",
"Hot",
"Hydra",
"Hydrus",
"Illa",
"Indus",
"Infernal",
"Inferno",
"Ishtar",
"Jabor",
"Jadis",
"Janemba",
"Japhrimel",
"Jennifer",
"Jormungandr",
"Juiblex",
"K'ril",
"Kal'Ger",
"Kali",
"Khorne",
"Kil'jaeden",
"Kneesocks",
"Koakuma",
"Korrok",
"Kronos",
"Lacerta",
"Laharl",
"Lamia",
"Leo Minor",
"Leo",
"Lepus",
"Leviathan",
"Libicocco",
"Libra",
"Ligur",
"Lilith",
"Little",
"Loki",
"Longhorn",
"Lorne",
"Lucifer",
"Lupus",
"Lynx",
"Lyra",
"Mal'Ganis",
"Malacoda",
"Maledict",
"Malfegor",
"Malice",
"Mammon",
"Mancubus",
"Mania",
"Mannoroth",
"Mantus",
"Marduk",
"Marilith",
"Masselin",
"Mastema",
"Meg",
"Mehrunes",
"Melek",
"Melkor",
"Mensa",
"Mephisto",
"Mephistopheles",
"Metztli",
"Microscopium",
"Mictian",
"Milcom",
"Moloch",
"Monoceros",
"Mormo",
"Musca",
"N'zall",
"Naamah",
"Nadia",
"Nalfeshnee",
"Nanatoo",
"Nergal",
"Nero",
"Neuro",
"Newt",
"Nihasa",
"Nija",
"Norma",
"Nosferatu",
"Nouda",
"Nurgle",
"Octans",
"Ophiuchus",
"Orion",
"Ouroboros",
"Overlord",
"Oyashiro",
"Pavo",
"Pazuzu",
"Pegasus",
"Pennywise",
"Persephone",
"Perseus",
"Phoenix",
"Pictor",
"Pisces",
"Piscis Austrinus",
"Psaro",
"Puppis",
"Pwcca",
"Pwyll",
"Pyxis",
"Quasit",
"Queezle",
"Qwan",
"Qweffor",
"Ra",
"Rakdos",
"Ramuthra",
"Randall",
"Red",
"Reticulum",
"Retriever",
"Rimmon",
"Rin",
"Ronove",
"Rosier",
"Rubicante",
"Ruby",
"Sabazios",
"Sagitta",
"Sagittarius",
"Samael",
"Samnu",
"Satan",
"Satanas",
"Sauron",
"Scanty",
"Scarlet",
"Scarmiglione",
"Scorpius",
"Sculptor",
"Scumspawn",
"Scutum",
"Sebastian",
"Sedit",
"Sekhmet",
"Sephiroth",
"Serpens",
"Set",
"Sextans",
"Shaitan",
"Shax",
"Shiva",
"Silitha",
"Slaanesh",
"Sparda",
"Spawn",
"Spike",
"Spine",
"Straga",
"Supay",
"T'an-mo",
"Taurus",
"Taus",
"Tchort",
"Telescopium",
"Tempus",
"Tezcatlipoca",
"Thammaron",
"Thamuz",
"Thoth",
"Tiamat",
"To'Kash",
"Toby",
"Triangulum Australe",
"Triangulum",
"Trigon",
"Tucana",
"Tunrida",
"Turok-Han",
"Typhon",
"Tzeentch",
"Ungoliant",
"Ursa Major",
"Ursa Minor",
"Vein",
"Vela",
"Vergil",
"Violator",
"Virgo",
"Volans",
"Vrock",
"Vulgrim",
"Vulpecula",
"Vyers",
"Ware",
"Wormwood",
"Yaksha",
"Yama",
"Yaotzin",
"Yen",
"Yk'Lagor",
"Zankou",
"Zepar",
"Zuul",
]

const planets = [
    "Alpha",
    "Earth",
    "GJ 1061 c",
    "GJ 1061 d",
    "GJ 163 c",
    "GJ 180 c",
    "GJ 229 A c",
    "GJ 273 b",
    "GJ 3293 d",
    "GJ 357 d",
    "GJ 667 C c",
    "GJ 667 C f",
    "GJ 667 Ce",
    "GJ 682 b",
    "GJ 832 c",
    "HD 40307 g",
    "K2-18 b",
    "K2-288 B b",
    "K2-3 d",
    "K2-72 e",
    "K2-9 b",
    "Kepler-1090 b",
    "Kepler-1229 b",
    "Kepler-1540 b",
    "Kepler-1544 b",
    "Kepler-1552 b",
    "Kepler-1606 b",
    "Kepler-1632 b",
    "Kepler-1638 b",
    "Kepler-1649",
    "Kepler-1652 b",
    "Kepler-1653 b",
    "Kepler-1701 b",
    "Kepler-174 d",
    "Kepler-22 b",
    "Kepler-283 c",
    "Kepler-296 e",
    "Kepler-296 f",
    "Kepler-298 d",
    "Kepler-440 b",
    "Kepler-442 b",
    "Kepler-443 b",
    "Kepler-452 b",
    "Kepler-61 b",
    "Kepler-62 e",
    "Kepler-62 f",
    "Kepler-705 b",
    "LHS 1140 b",
    "Proxima Cen b",
    "Ross 128 b",
    "TOI-700 d",
    "TRAPPIST-1 d",
    "TRAPPIST-1 e",
    "TRAPPIST-1 f",
    "TRAPPIST-1 g",
    "Teegarden's Star b",
    "Teegarden's Star c",
    "Wolf 1061 c",
    "tau Cet f"
]

// Thanks, Terry!
const godWords = ["a","abandon","abandoned","abandonedly","abase","abased","abashed","abated","abhor","abhorred","abhorring","abide","abided","abidest","abideth","abiding","abilities","ability","abject","able","abler","abode","abolished","abominable","abound","abounded","about","above","abraham","abridged","abroad","absence","absent","absolute","absolutely","absorb","abstemious","abstinence","abstract","abstruser","absurd","absurdity","absurdly","abundance","abundant","abundantly","abuse","abyss","academicians","academics","accents","accept","acceptable","acceptably","accepted","accepts","access","accompany","accomplices","accomplish","accomplished","accord","according","account","accounted","accursed","accuse","accused","accuser","accusing","accustomed","aches","achieve","acknowledge","acknowledged","acquaintance","acquainted","acquiesce","acquire","acquired","act","acted","acting","action","actions","actor","actors","acts","actual","actually","acute","acuteness","ad","adam","adapt","adapted","add","added","adding","addition","additional","additions","adeodatus","admirable","admiration","admire","admired","admiring","admission","admit","admitted","admittest","admonish","admonished","admonition","adomed","adopted","adoption","adored","adorned","adorning","adulterer","adulteress","adulterous","adultery","advance","advantage","adversary","adversities","adversity","advice","advices","advised","advising","aeneas","afar","affairs","affect","affected","affecting","affection","affections","affects","affianced","affirm","affirmed","affirming","affliction","afford","affright","affrighted","aforesaid","aforetime","afraid","afric","africa","african","after","afternoon","afterward","afterwards","again","against","age","aged","agents","ages","aghast","agito","ago","agonised","agonistic","agony","agree","agreeable","agreed","agreement","ahead","aid","aided","ails","aim","air","alarmed","alas","alexandria","alike","alive","all","allay","allaying","allege","alleging","allegorically","allegory","allotted","allow","allowances","allowed","allowing","alloy","allurements","alluring","almighty","almost","alms","almsdeeds","aloft","alone","along","aloud","already","also","altar","alter","alteration","alterations","altered","alternately","alternates","alternatively","although","altogether","always","alypius","am","amazed","amazement","amazing","ambition","ambitions","ambitious","ambrose","ambrosian","amen","amend","amendment","amiable","amid","amidst","amiss","among","amongst","amphitheatre","ample","an","analyzed","anaximenes","ancient","and","angel","angels","anger","angered","angers","angry","anguish","animal","animals","animate","anniversary","announce","announced","announcement","announcing","annoyance","annual","anointings","anon","anonymous","another","answer","answerably","answered","answerest","answereth","answering","answers","antecedent","anticipate","anticipated","anticipating","antidote","antony","anubis","anxieties","anxiety","anxious","anxiously","any","anyone","anything","apart","apollinarian","apostle","apostles","apostolic","apparel","apparent","apparently","appeal","appear","appearance","appeared","appeareth","appearing","appears","appetite","applauded","applauds","applauses","applicable","application","applied","apply","appointed","appointing","appointment","appointments","apportioned","apprehend","apprehended","apprehending","apprehension","approach","approached","approaching","approbation","appropriated","approve","approved","approveth","approving","apt","aptly","aquatic","architect","architects","archive","ardent","ardently","are","argument","arguments","arians","aright","arise","arisen","ariseth","arising","aristotle","arithmetic","armed","armory","arms","army","arose","around","arrange","arranged","array","arrival","arrive","arrived","arriving","arrogancy","arrogant","arrows","art","artifice","artificer","artifices","arts","as","ascend","ascended","ascending","ascension","ascertained","ascii","ascribe","ascribed","ashamed","ashes","aside","ask","asked","asketh","asking","asks","asleep","aspirate","assail","assailed","assaying","assembly","assent","assented","assenting","assessor","assiduously","assign","assigned","assistance","associate","associated","association","assuaged","assume","assurance","assure","assured","assuredly","assuring","asterisk","astonished","astonishment","astray","astrologer","astrologers","asunder","at","athanasius","athenians","athirst","atrociously","attack","attacked","attain","attained","attaining","attempered","attempt","attempted","attendant","attended","attention","attentive","attentively","attested","attracted","attractiveness","attracts","attributed","attributing","attuned","audacious","audaciously","audience","auditor","auditors","aught","augmented","augmenting","augustine","author","authority","authors","avail","availed","ave","avenged","avengest","avenue","avenues","averred","averse","avert","avoid","avoided","avoiding","awaited","awaiting","awake","awakest","aware","away","awe","awful","awfulness","awhile","awoke","babe","babes","baby","babylon","babylonian","back","backs","backward","bad","bade","badge","baggage","bait","balancings","balaneion","ball","balls","balm","balneum","bands","banished","banner","banquetings","banter","baptise","baptised","baptism","barbarian","barbarism","bare","bared","bargain","bark","barked","barking","barred","barren","base","based","baseness","basest","bashfulness","basilica","basket","bath","bathe","bathed","bathing","baths","battle","bawler","be","beams","bear","bearer","beareth","bearing","bears","beast","beasts","beat","beaten","beatific","beating","beatings","beauteous","beauties","beautified","beautiful","beauty","became","because","beck","beclouded","become","becomes","becometh","becoming","bed","bedewed","bedimmed","been","befalls","befell","befitting","before","beforehand","beg","began","begannest","begat","beget","beggar","beggary","begged","begging","begin","beginneth","beginning","begins","begotten","beguile","beguiled","beguiling","begun","behalf","beheld","behind","behold","beholder","beholdest","beholdeth","beholding","beholds","being","beings","belief","believe","believed","believer","believes","believeth","believing","belly","belong","belongeth","belonging","belongs","beloved","below","bemoaned","bemoaning","bend","bends","beneath","benediction","benefit","benefits","bent","bepraised","bereaved","beseech","beset","besets","beside","besides","bespoken","bespotted","besprinkle","besprinkling","best","bestow","bestowed","bestowedst","betake","bethink","betook","betrothed","better","betters","between","betwixt","bewail","bewailed","bewailing","beware","bewitched","beyond","bibber","bibbing","bible","bid","bidden","bier","billion","billows","bin","binary","bind","bird","birdlime","birds","birth","birthright","births","bishop","biting","bitter","bitterly","bitterness","black","blade","blame","blamed","blameless","blamest","blameworthy","blasphemies","blasphemous","blasphemy","blasts","bleeding","blending","bless","blessed","blessedly","blessedness","blesses","blessest","blesseth","blessing","blessings","blest","blew","blind","blinded","blindness","blissful","blood","bloody","bloom","blotted","blottedst","blow","blowing","blunt","blunted","blush","blushed","boast","boastfulness","boasting","bodies","bodily","body","boil","boiled","boiling","boils","bold","boldly","boldness","bond","bondage","bonds","bones","book","books","bore","born","borne","borrow","bosom","bosses","both","bottom","bottomless","boughs","bought","bounces","bound","boundary","bounded","boundless","bounds","bouverie","bowed","bowels","bowers","bowing","bows","boy","boyhood","boyish","boys","brackishness","brain","breach","bread","breadth","break","breakers","breaking","breast","breasts","breath","breathe","breathed","breathedst","breathing","bred","brethren","briars","bribe","bridal","bride","bridegroom","brides","bridle","briefly","briers","bright","brighter","brightness","brim","bring","bringeth","bringing","brings","brittle","broad","broke","broken","brooks","brother","brotherly","brought","broughtest","brow","brows","brute","bubbling","bubblings","buckler","bud","builded","builder","building","buildings","built","bulk","bulky","burden","burial","buried","burn","burned","burnest","burning","burnt","burst","burstest","bursting","burthen","burthened","bury","bushel","busied","business","bustle","busy","but","butler","buy","buyers","buzz","buzzed","buzzing","by","cabinets","caesar","cakes","calamities","calamity","calculate","calculated","calculation","calculations","calf","call","called","calledst","callest","calleth","calling","calls","calm","calmed","calmly","calumnies","came","camp","can","candid","candle","cane","cannot","canst","canticles","canvassing","capable","capacities","capacity","capital","captain","captious","captive","captivity","carcase","care","cared","caredst","careful","carefully","careless","carelessly","cares","caresses","carest","careth","carnal","carolina","carried","carry","carrying","carthage","carthaginian","case","cases","cassiacum","cast","casters","castest","casting","casts","cataloguers","catch","catching","catechumen","catholic","catholics","catiline","cattle","caught","cauldron","cause","causes","causing","caverns","caves","cd","cease","ceased","ceases","ceasest","ceaseth","ceasing","cedars","celebrated","celebration","celestial","celibacy","cellar","cellars","cementest","censers","censured","central","centre","certain","certainly","certainties","certainty","chain","chains","chair","challenged","challenges","chamber","chambering","chambers","chance","change","changeable","changeableness","changed","changes","changest","changing","chant","chanting","chapter","character","characters","charge","charges","charioteer","chariots","charity","charmed","chaste","chastely","chastened","chastenedst","chastenest","chastity","chatto","cheap","check","checked","checking","cheeks","cheer","cheered","cheerful","cheerfulness","cherish","cherished","cherubim","chest","chewing","chief","chiefest","chiefly","child","childbearing","childhood","childish","children","chill","chilled","choice","choked","choler","choleric","choose","chooses","choosing","chose","chosen","christ","christian","christians","church","churches","cicero","circensian","circles","circuit","circuits","circumcise","circumlocutions","circumstance","circus","cities","citizen","citizens","city","claim","clanking","clasp","clasped","class","classics","clave","clay","clean","cleanse","cleansed","cleansest","cleansing","clear","cleared","clearest","clearly","clearness","cleave","cleaved","cleaveth","cleaving","climb","clingeth","cloak","cloaked","clog","close","closed","closes","closing","clothe","clothed","clothing","cloud","cloudiness","clouds","cloudy","cloven","cloyed","cloyedness","co","coals","coats","codes","coeternal","cogitated","cogitation","cogito","cogo","cold","collect","collected","collectively","collects","colorado","colour","coloured","colouring","colours","com","combinations","combine","combined","come","comely","comes","cometh","comfort","comforted","comfortedst","comforter","comfortest","comforting","comforts","coming","command","commanded","commander","commandest","commandeth","commanding","commandment","commandments","commands","commemorated","commencement","commencing","commend","commendable","commended","commender","commenders","commendeth","comment","commercial","commiserate","commiserates","commiserating","commission","commit","commits","committed","committing","common","commonly","communicate","communicated","communication","community","compact","compactedst","compacting","companion","companions","company","compare","compared","comparing","comparison","compass","compassing","compassion","compassionate","compassionates","compelled","compendiously","complain","complaints","complete","completed","compliance","compose","composed","composing","compound","comprehend","comprehended","comprehendeth","comprehending","compressed","comprise","comprised","computer","computers","conceal","concealed","conceit","conceits","conceive","conceived","conceives","conceiving","concentrated","conception","conceptions","concern","concerned","concerning","concernment","concerns","conclude","concluded","concludeth","concord","concreated","concubinage","concubine","concupiscence","concupiscences","condemnation","condemned","condemnest","condemning","condemns","condensed","condition","condole","confer","conference","conferring","confess","confessed","confesses","confesseth","confessing","confession","confessions","confidence","confidentially","confidently","confiding","confine","confined","confirm","confirmed","conflict","conflicting","conform","conformably","conformed","conformity","confound","confounded","confused","confusedly","confusedness","confusions","confute","confuted","congratulated","congratulating","congratulation","congregations","conjecture","conjectures","conjecturing","conjoined","conjugal","connecticut","connects","conquer","conquered","conquering","conquests","conscience","conscious","consciousness","consecrate","consecrated","consecrateth","consecrating","consecration","consent","consented","consenting","consentings","consequences","consequential","conservative","consider","considerable","consideration","considered","considereth","considering","considers","consigned","consist","consistent","consistently","consisteth","consisting","consists","consolation","consolations","conspiracy","conspirators","constant","constantly","constellations","constitute","constituted","constituteth","constrain","constrained","constraint","consult","consulted","consulter","consulters","consulting","consume","consumed","consumest","consuming","consumption","contact","contacting","contagion","contain","contained","containest","containeth","containing","contains","contemn","contemplate","contemplateth","contemplating","contemplation","contemporary","contempt","contemptible","contend","content","contented","contention","contentions","contentious","contentiousness","contentment","contents","contests","continence","continency","continent","continently","continual","continually","continuance","continue","continued","continueth","contract","contracted","contradict","contradicting","contradiction","contradictions","contradictory","contrary","contributing","contributions","contrite","contrition","contritions","controversy","convenient","conventicle","conventionally","conversation","conversations","converse","conversing","conversion","convert","converted","convertedst","converting","convey","conveyed","conveyedst","conveying","conveys","convict","convicted","convinced","copied","copies","copious","copy","copyright","corn","corner","corners","corporeal","corporeally","corpse","correct","corrected","correction","corresponded","correspondence","corresponding","corrigible","corrupt","corrupted","corruptible","corrupting","corruption","corruptions","corruptly","cost","costs","cottage","couch","could","couldest","councillor","counsel","counselled","counsels","count","counted","countenance","counterfeit","countermanding","country","countryman","counts","courage","course","courses","coursing","court","courteously","courtesy","courtly","courts","covenant","covenanted","cover","covered","covetous","covetousness","cower","craftier","crafty","create","created","createdst","createst","createth","creating","creation","creator","creature","creatures","credence","credibility","credit","credulity","creep","creepeth","creeping","crept","creusa","cried","criedst","cries","crime","crimes","crisis","criticised","criticising","crooked","crookedly","crookedness","cross","crossed","crosses","crown","crowned","crucifixion","crudities","cruel","cruelty","cry","crying","cubit","cubits","cud","cultivated","cultivating","culture","cunning","cup","cupboards","cupidity","curb","cure","cured","curest","curing","curiosities","curiosity","curious","curiously","current","currents","curtailment","custom","customs","cut","cutting","cyprian","daemon","daemons","daily","dakota","damage","damaged","damages","damnable","danae","danger","dangerous","dangers","dare","dared","dares","dark","darkened","darkenings","darkest","darkly","darkness","darksome","darksomely","dashed","data","date","dates","daughter","daughters","david","dawn","dawned","day","daybreak","days","dead","deadly","deaf","deafen","deafness","deal","dealt","dear","dearer","dearest","death","deaths","debasing","debated","debtor","debtors","debts","decay","decayeth","decays","deceased","deceit","deceitful","deceits","deceivableness","deceive","deceived","deceivers","deceiving","deceivingness","december","deception","decide","decided","decked","declaiming","declamation","declare","declared","decline","decree","decrepit","dedicate","dedicated","deductible","deed","deeds","deem","deemed","deep","deeper","deepest","deeply","deepness","deeps","defect","defection","defective","defects","defence","defended","defender","defending","defends","deferred","deferring","defers","defile","defiled","defilements","define","defined","defining","definite","definitely","deformed","deformities","deformity","degraded","degree","degrees","deity","dejectedness","delay","delayed","delete","deliberate","deliberates","deliberating","deliberation","delight","delighted","delightful","delightfulness","delighting","delights","delightsome","deliver","delivered","deliveredst","deliverest","delivering","delivers","deluded","deluding","delusions","demanded","demander","demandest","demanding","demands","demonstrate","demonstrated","denied","denies","denieth","denoted","denotes","dens","deny","depart","departed","departest","departing","departure","depend","depending","depends","depraved","deprived","depth","depths","deride","derided","deridedst","deriders","derides","deriding","derision","derive","derived","descend","descendants","descending","descent","described","deserted","deserters","deserts","deserve","deserved","deservedly","deserving","deservings","designs","desire","desired","desiredst","desires","desireth","desiring","desirous","despair","despaired","despairing","desperate","despise","despised","despisedst","despisest","despiseth","despising","despite","destined","destroy","destroyers","destroying","destruction","detached","detail","details","detain","detected","determined","detest","detested","detesting","detriment","deus","devil","devilish","devils","devised","devoted","devotion","devotions","devour","devoured","devout","devoutly","devoutness","dew","dialogue","did","diddest","dido","didst","die","died","dies","dieth","differ","difference","different","differently","difficult","difficulties","difficulty","diffused","digest","digested","digits","dignities","dignity","diligence","diligent","diligently","diluted","dimensions","diminish","diminished","diminisheth","diminishing","diminution","din","dining","dinner","dir","direct","directed","directeth","directing","direction","directions","directly","director","disagreeing","disagreements","disalloweth","disallowing","disappear","disapproved","disapproveth","discern","discerned","discerneth","discerning","discharge","disciple","disciples","discipline","disclaimer","disclaimers","disclaims","disclose","disclosed","discomfort","discommended","discontent","discord","discordant","discourage","discourse","discoursed","discourses","discoursing","discover","discoverable","discovered","discoverest","discovereth","discovering","discovers","discovery","discreet","discreetly","discretion","discuss","discussed","discussion","disdained","disease","diseased","diseases","disembowelled","disengage","disentangle","disentangled","disgraced","disgraceful","disguise","disguised","disguising","disgust","disgusted","dishes","dishonour","disk","disliked","dismiss","disobeyed","disorder","disordered","disorders","dispel","dispense","dispensed","dispenser","dispensers","dispensest","dispensing","dispersed","disperseth","dispersion","displace","displacing","displayed","displays","displease","displeased","displeaseth","displeasing","dispose","disposed","disposer","disposition","dispraise","dispraised","dispraisest","disprove","dispute","disputed","disputer","disputes","disputing","disquiet","disquieted","disregard","dissent","dissentings","dissipated","dissipation","dissoluteness","dissolution","dissolved","dissolvest","distance","distances","distant","distempered","distended","distill","distilled","distinct","distinction","distinctly","distinguish","distinguished","distinguishing","distract","distracted","distractedly","distracting","distraction","distractions","distress","distribute","distributed","distributest","distributing","distribution","distrusted","disturb","disturbed","dive","divers","diverse","diversely","diversifiedst","diversity","diversly","divide","divided","dividest","dividing","divination","divinations","divine","diving","divinity","division","divisions","divorceth","do","docile","docs","doctrine","doer","does","doest","dog","doing","doings","doleful","dollar","dollars","domain","domestic","domine","dominion","dominions","don","donation","donations","done","door","doors","dormant","dost","dotages","dotards","doted","doth","doting","double","doubled","doubt","doubted","doubteth","doubtful","doubtfulness","doubting","doubtless","doubts","down","downfall","download","downward","downwards","dragging","dragon","dragons","drank","draught","draw","drawest","drawing","drawn","draws","dread","dreaded","dreadful","dreading","dream","dreams","drenched","drew","drewest","dried","drink","drinketh","drinking","drive","driven","drives","drop","drops","drove","drowsiness","drowsy","drudgery","drunk","drunkard","drunkards","drunken","drunkenness","dry","dryness","duad","duck","dudley","due","dug","dull","duller","dully","dumb","during","durst","dust","duties","dutiful","duty","dwell","dwellers","dwellest","dwelleth","dwelling","dwelt","dying","each","eager","eagerly","eagerness","eagle","ear","earliest","early","earnest","earnestly","earnestness","ears","earth","earthly","ease","eased","easeful","easier","easily","east","eastern","easy","eat","eaten","eateth","eating","ebb","ebbing","ebbs","ebcdic","ecclesiastical","echo","echoed","echoes","eclipsed","eclipses","edged","edification","edify","edit","edited","editing","edition","editions","educate","educated","education","edward","effaced","effaces","effect","effected","effectedst","effort","efforts","egypt","egyptian","eight","eighth","ein","either","elder","elders","elect","electronic","electronically","element","elements","elephant","elevated","elevation","eligible","elijah","eloquence","eloquent","eloquently","else","elsewhere","eluding","email","embalmed","embarrassments","embittered","emblem","embrace","embraced","embracement","embracements","embraces","embryo","emerge","emerging","eminence","eminent","eminently","emotion","emotions","emperor","empire","employ","employed","employee","employest","empress","emptied","emptiness","emptinesses","empty","enabled","enact","enacted","enacteth","enamoured","enchantment","encompass","encompasseth","encourage","encouraged","encouraging","encumbered","encumbrances","end","endangers","endeared","endearments","endeavour","endeavoured","endeavouring","endeavours","ended","ending","endlessly","ends","endued","endurance","endure","endured","endures","endureth","enduring","enemies","enemy","energy","enervated","enforced","enforcement","engage","engaged","engages","engine","english","engraff","enigma","enjoin","enjoined","enjoinest","enjoy","enjoyed","enjoying","enlarge","enlarged","enlargedst","enlarging","enlighten","enlightened","enlightener","enlighteneth","enlightening","enlightens","enlisted","enmities","enmity","enough","enounce","enquire","enquired","enquirers","enquiries","enquiring","enquiringly","enquiry","enricher","ensample","enslaved","ensnared","ensnaring","ensues","ensuing","entangle","entangled","entangling","enter","entered","enteredst","entering","enters","enthralled","enticed","enticements","enticing","entire","entirely","entireness","entitled","entrails","entrance","entreat","entreated","entreaties","entrust","entrusted","entwine","enumeration","envenomed","envious","environeth","environing","envy","envying","epaphroditus","epicurus","episcopal","epistles","equably","equal","equalled","equally","equals","equinoxes","equipment","equity","equivalent","ere","ergo","err","erred","error","errors","erst","esau","escape","escaped","especially","espoused","essay","essayed","essence","establish","established","estate","esteem","esteemed","esteeming","estimate","estimated","estimation","estranged","etc","eternal","eternally","eternity","etext","etext00","etext01","etext02","etext90","etext99","etexts","eunuchs","euodius","evangelists","evaporates","eve","even","evened","evening","evenly","events","ever","everfixed","everlasting","everlastingly","evermore","every","everything","everywhere","evidence","evident","evidently","evil","evils","evincing","exact","exacted","exacting","exalted","exaltedness","examine","examined","examiner","examining","example","examples","exceeded","exceedeth","exceeding","exceedingly","excel","excelled","excellence","excellencies","excellency","excellent","excellently","excelling","excels","except","excepted","exceptions","excess","excessive","exchanged","excited","excitement","excites","exclaim","exclude","excluded","exclusion","exclusions","excursive","excuse","excused","excuses","execrable","execrate","execute","exercise","exercised","exhalation","exhausted","exhibit","exhibited","exhort","exhortation","exhortations","exhorted","exhorting","exigency","exiled","exist","existence","existeth","existing","exists","exodus","expansive","expect","expectation","expected","expecteth","expects","expends","expense","expenses","experience","experienced","experiments","explain","explained","explaining","explanatory","expose","exposed","expound","expounded","expounding","express","expressed","expression","expressions","exquisitely","extended","extension","extent","exterior","external","extinguish","extinguished","extol","extolled","extollers","extracted","extreme","extremely","extremest","extricate","extricated","exuberance","exuberant","exude","exult","exultation","eye","eyes","eyesight","fable","fables","fabric","fabulous","face","faced","faces","facio","factito","facts","faculties","faculty","fail","faileth","failing","failure","fain","faint","fainting","faintly","fair","fairer","fairest","fairly","fairness","faith","faithful","faithfully","faithfulness","fall","fallacies","fallen","falling","falls","false","falsehood","falsehoods","falsely","falsified","fame","familiar","familiarised","familiarly","family","famine","famished","famous","fancies","fancy","fanned","fantasies","fantastic","far","fared","farness","farther","fashion","fashioned","fast","faster","fastidiousness","fasting","fastings","fat","father","fatherless","fatherly","fathers","fatigues","fatness","fault","faultfinders","faults","faulty","faustus","favour","favoured","favourites","favours","fear","feared","feareth","fearful","fearfully","fearing","fearlessly","fears","feast","fed","fee","feed","feedest","feedeth","feeding","feeds","feel","feeling","feelings","feels","fees","feet","feigned","felicity","fell","fellow","fellows","felt","female","females","fence","fervent","fervently","fervid","festival","fetched","fettered","fetters","fever","feverishness","few","fiction","fictions","field","fields","fiercely","fiercest","fifteen","fifth","fifty","fig","fight","fighter","fighting","figurative","figuratively","figure","figured","figures","file","filename","files","fill","filled","fillest","filleth","filling","filth","filths","filthy","final","finally","find","findest","findeth","finding","finds","fine","finest","finger","fingers","finish","finished","finite","fire","fired","fires","firm","firmament","firminus","firmly","firmness","first","fish","fishes","fit","fitness","fitted","fitter","fittest","fitting","five","fix","fixed","fixedly","fixing","flagitiousness","flagitiousnesses","flagon","flame","flash","flashedst","flashes","flashing","flattering","fled","fledged","flee","fleeing","fleeting","flesh","fleshly","fleshy","flew","flies","flights","fling","floating","flock","flocks","flood","floods","flour","flourished","flow","flowed","flower","flowers","flowing","flown","flows","fluctuate","fluctuates","fluctuating","fluctuations","fluently","fluidness","flung","fluttereth","flux","fly","flying","foamed","fog","folded","folk","folks","follies","follow","followed","followers","followest","followeth","following","follows","folly","food","fool","foolish","foolishly","foolishness","fools","foot","footed","footsteps","for","forasmuch","forbade","forbadest","forbare","forbear","forbid","forbidden","force","forced","forceth","forcibly","fore","foreconceived","forefathers","forego","foregoing","forehead","foreign","foreigner","foreknowledge","forementioned","forenamed","forenoon","forenoons","forepassed","foresaw","foresee","foreshow","foreshowed","foreshower","foresignified","foresignify","forests","foretell","foretelling","forethink","forethinking","forethought","foretold","forgave","forge","forget","forgetful","forgetfulness","forgetteth","forgetting","forging","forgive","forgiven","forgivest","forgiving","forgot","forgotten","forgottest","form","formation","formed","formedst","former","formerly","formest","forming","formless","formlessness","forms","fornicating","fornication","fornications","forsake","forsaken","forsakest","forsaketh","forsaking","forsook","forsooth","forth","forthcoming","forthwith","fortunate","fortunes","forum","forward","forwards","foster","fostering","fought","foul","foully","foulness","fouls","found","foundation","foundations","founded","fountain","four","fourth","fourthly","fowl","fowls","fragment","fragments","fragrance","fragrant","frailness","frame","framed","framers","frantic","fraud","fraught","freaks","free","freed","freedom","freely","freeman","frenzied","frenzies","frenzy","frequent","frequently","fresh","freshness","fretted","friend","friends","friendship","frightful","fro","from","front","frozen","fruit","fruitful","fruitfully","fruitfulness","fruitless","fruitlessly","fruits","ftp","fuel","fugitives","fulfil","fulfilled","fulfils","full","fuller","fully","fulness","fumed","fumes","functions","fund","funding","funeral","furious","furnace","furnish","furnished","furnishing","further","furtherance","furthermore","fury","future","gain","gained","gainful","gaining","gains","gainsay","gainsayer","gainsayers","gainst","galatians","gales","gall","gallantry","game","games","garb","garden","gardens","garland","garlands","garment","garner","gasped","gate","gates","gather","gathered","gatheredst","gatherest","gathering","gathers","gave","gavest","gay","gaze","gazers","gazing","general","generally","generate","generation","generations","genesis","genius","gentiles","gentle","gentleness","gently","genuinely","geometry","gervasius","gestures","get","getting","ghastly","ghost","ghosts","giant","gift","gifted","gifts","gilded","girded","girl","girls","give","given","giver","givers","gives","givest","giveth","giving","glad","gladdened","gladdens","gladiators","gladly","gladness","gladsome","glance","glances","glass","gleam","gleameth","gleams","glide","glided","gliding","glittering","gloried","glories","glorieth","glorified","glorifies","glorify","glorious","glory","glorying","glow","glowed","gloweth","glowing","glows","glue","gnashed","gnawed","gnawing","go","goad","goaded","goading","goads","goal","god","godhead","godless","godly","gods","goes","goest","goeth","going","gold","golden","gone","good","goodliness","goodly","goodness","goods","gorgeous","gospel","got","gotten","govemed","government","governor","governors","gowned","grace","graceful","gracefulness","gracious","gradation","gradually","grammar","grammarian","grammarians","grandchildren","grant","granted","grantest","grasp","grasps","grass","grassy","gratefully","gratias","gratification","gratings","gratuitous","gratuitously","grave","gravity","great","greater","greatest","greatly","greatness","greaves","grecian","greedily","greediness","greedy","greek","greeks","green","greet","greeted","grew","grief","griefs","grieve","grieved","grieves","grievest","grieving","grievous","grievously","groan","groaned","groaneth","groaning","groanings","groans","groat","gross","grossness","ground","grounded","grounds","groves","grow","growing","grown","growth","guarded","guardian","guardianship","guess","guessed","guidance","guidances","guide","guidest","guilt","guiltless","guilty","gulf","gushed","gutenberg","gutindex","habit","habitation","habits","had","hadst","hail","hair","hairs","hale","haled","half","hallow","hallowed","hallowing","halved","hand","handkerchief","handle","handled","handling","handmaid","hands","handwriting","hang","hanging","hap","haphazard","hapless","haply","happen","happened","happeneth","happens","happier","happily","happiness","happy","harass","harbour","harbouring","hard","harder","hardheartedness","hardship","hare","hark","harlotries","harm","harmed","harmless","harmonious","harmoniously","harmonise","harmoniseth","harmonising","harmonized","harmonizing","harmony","harsh","hart","harts","harvest","has","hast","haste","hasten","hastened","hastening","hasting","hatchet","hate","hated","hateful","hath","hatred","haughtiness","haunt","have","having","hay","he","head","header","headlong","heads","heal","healed","healedst","healeth","healing","health","healthful","healthfully","healthy","heaped","heaps","hear","heard","heardest","hearer","hearers","hearest","heareth","hearing","hearken","hears","hearsay","heart","hearted","heartedness","hearts","heat","heated","heathen","heaven","heavenly","heavens","heavier","heavily","heavy","hebrew","hedged","heed","heeded","height","heightening","heights","heinous","held","heldest","hell","hellish","helmet","help","helped","helper","helpful","helpidius","helping","hence","henceforth","her","herb","herbs","here","hereafter","hereat","hereby","hereditary","herein","hereof","hereon","heresies","heresy","heretics","heretofore","hereunto","hereupon","hers","herself","hesitate","hesitated","hesitating","hesitation","hid","hidden","hiddest","hide","hideous","hidest","hierius","high","higher","highest","highly","highness","hills","him","himself","hinder","hindered","hindereth","hindrance","hint","hippocrates","his","history","hither","hitherto","hoar","hogs","hogshed","hold","holden","holdest","holding","holds","holies","holily","holiness","holy","home","homer","honest","honestly","honesty","honey","honied","honor","honour","honourable","honoured","honouring","honours","hook","hooks","hope","hoped","hopeful","hopes","hoping","horns","horrible","horror","horse","horses","hortensius","host","hosts","hot","hotly","hour","hours","house","household","houseless","houses","hovered","how","however","howsoever","html","http","huge","hugging","human","humane","humanity","humans","humble","humbled","humbledst","humility","hundred","hundreds","hung","hunger","hungered","hungering","hungry","hunting","hurried","hurriedly","hurrying","hurt","hurtful","hurting","husband","husbands","hushed","husks","hymn","hymns","hypertext","i","ibiblio","ice","idaho","ideas","identification","identify","idle","idleness","idly","idol","idols","if","ignoble","ignorance","ignorant","ii","iii","ill","ills","illuminate","illuminating","illumined","illusion","image","images","imaginary","imagination","imaginations","imagine","imagined","imagining","imbibe","imbibed","imbue","imitate","imitated","imitating","imitation","immediately","immense","immersed","immoderate","immortal","immortality","immortally","immovably","immutable","impair","impaired","impart","imparted","imparts","impatience","impatient","impatiently","imperfect","imperfection","imperfections","imperishable","imperturbable","impiety","impious","implanted","implanting","implied","imply","important","importunity","imposed","impostors","impostumes","impotent","impregnable","impress","impressed","impressing","impression","impressions","imprinted","improperly","impudently","impulses","impunity","impure","impurity","impute","in","inaccessible","inaccurate","inactive","inanimate","inappropriately","inasmuch","incarnation","incense","incensed","inchoate","incidental","inclination","incline","inclined","included","including","incommutable","incomparably","incomplete","incomprehensible","incongruously","incorporeal","incorrect","incorruptible","incorruptibly","incorruption","increase","increased","increasing","incredible","incredibly","incumbrances","incurable","incurred","indebted","indeed","indefinitely","indemnify","indemnity","indentures","indexes","indiana","indicate","indicated","indicating","indications","indigent","indigested","indignant","indirect","indirectly","indite","indited","indued","indulgent","inebriate","inebriated","inebriation","inevitable","inevitably","inexperienced","inexpressible","infancy","infant","infantine","infants","infected","infection","infer","inferior","infidelity","infidels","infinite","infinitely","infinitude","infirmities","infirmity","inflame","inflamed","inflammation","inflection","inflicted","influence","influenced","influences","information","informed","informing","infringement","infuse","infused","ingrated","inhabitant","inhabitants","inhabited","inharmonious","inheritance","iniquities","iniquity","initiated","initiating","initiation","initiatory","injurable","injure","injured","injures","injurious","injury","injustice","inmost","innate","inner","innocence","innocency","innocent","innumerable","innumerably","inordinate","insatiable","insatiate","insensibly","inseparable","inserted","insight","insolently","insomuch","inspect","inspecting","inspiration","inspire","inspired","inspirest","inspiring","instability","instance","instances","instant","instantly","instead","instillest","instinct","instinctive","instituted","institution","instruct","instructed","instructest","instruction","instructor","insult","insultingly","intellectual","intelligence","intelligences","intelligent","intelligible","intemperance","intend","intended","intense","intensely","intenseness","intensest","intent","intention","intently","intercede","intercedeth","intercepting","intercession","intercourse","interest","interested","interior","intermission","intermit","intermitted","intermitting","internal","international","interpose","interposed","interposing","interpret","interpretation","interpreted","interpreter","interpreting","interrupt","interrupted","interruption","interval","intervals","intimacy","intimate","intimated","intimately","into","intolerable","intoxicated","intricate","introduced","intrude","inured","inveigler","invest","invests","invisible","invite","invited","inviting","involved","inward","inwardly","iowa","iron","irons","irrational","irresoluteness","irrevocable","irritate","is","isaac","isaiah","israel","issue","issued","it","italian","italy","itch","itching","items","its","itself","jacob","jarring","jealous","jealousy","jeer","jeering","jerusalem","jest","jests","jesus","jew","jews","join","joined","joineth","joining","joint","jointly","joking","jordan","joseph","journey","journeyed","jove","joy","joyed","joyful","joyfulness","joying","joyous","joyously","joyousness","joys","judge","judged","judgements","judges","judgest","judgeth","judging","judgment","judgments","julian","juncture","june","juno","jupiter","just","justice","justification","justifieth","justify","justina","justly","keen","keep","keeper","keepest","keeping","keeps","ken","kentucky","kept","kicking","kill","killed","killest","killeth","kind","kindle","kindled","kindly","kindness","kindred","kinds","king","kingdom","kingdoms","kings","knee","knees","knew","knewest","knit","knitting","knock","knocked","knocketh","knocking","knocks","knots","knottiness","knotty","know","knowest","knoweth","knowing","knowingly","knowledge","known","knows","labour","laboured","labourers","labours","lack","lacketh","lacking","laden","laid","lament","lamentable","lamented","lancet","land","lands","language","languages","languishing","lanthorn","lap","large","largely","largeness","larger","lashed","lashes","lashest","last","lastly","late","later","lathe","latin","latins","latinum","latter","lattice","laugh","laughed","laughter","launched","law","lawful","lawfully","lawless","laws","lawyer","lawyers","laxly","lay","layeth","laying","lays","lead","leaden","leadeth","leading","leads","leaning","leaps","learn","learned","learner","learning","learnt","leasing","least","leave","leaven","leaves","leaveth","leaving","lecture","lectured","led","left","legal","legally","leisure","lends","length","lengthened","lentiles","less","lessen","lesser","lesson","lessons","lest","let","lethargy","letter","letters","lettest","lewd","liability","liar","libanus","liberal","liberality","liberty","licence","license","licensed","licenses","lick","lie","lied","lies","lieth","life","lift","lifted","liftedst","lifter","liftest","lifts","light","lighted","lighten","lightened","lighteth","lightly","lights","lightsome","like","liked","likedst","likely","likeness","likenesses","liker","likes","likest","likewise","lilies","limbs","limed","limitation","limited","limits","line","lineage","lineaments","lined","lines","linger","lingered","lingering","links","lion","lip","lips","list","listened","listening","listing","lists","literally","literary","literature","litigation","little","littles","live","lived","livelihood","lively","lives","livest","liveth","living","lizard","lo","load","loads","loathed","loathing","loathsome","locking","locusts","lodging","lofty","logic","login","long","longed","longer","longing","longings","longs","look","looked","looker","looketh","looking","looks","loose","loosed","loosen","loosest","loquacity","lord","lords","lose","loses","loseth","losing","loss","lost","lot","loth","lottery","loud","loudly","louisiana","love","loved","loveliness","lovely","lover","lovers","loves","lovest","loveth","loving","lovingly","low","lower","lowering","lowest","lowlily","lowliness","lowly","lucid","lucre","ludicrous","lulled","luminaries","lump","lungs","lure","lures","lust","lusted","lusteth","lustful","lustfulness","lusting","lusts","luxuriousness","luxury","lying","macedonia","machine","mad","madaura","made","madest","madly","madness","magical","magistrates","magnified","magnify","magnitudes","maid","maiden","maidens","mail","maimed","main","mainly","maintain","maintained","maintainers","maintaining","majesty","make","maker","makers","makes","makest","maketh","making","male","malice","malicious","maliciously","malignant","mammon","man","manage","manfully","mangled","manhood","manichaean","manichaeus","manichee","manichees","manifest","manifestation","manifested","manifestly","manifold","manifoldly","manifoldness","mankind","manna","manner","manners","manors","mansion","mantles","manufactures","many","mariners","mark","marked","market","marketplace","marking","marks","marriage","marriageable","married","marrow","marry","marrying","mars","marts","martyr","martyrs","marvel","marvelled","mary","masculine","mass","massachusetts","masses","master","mastered","masters","mastery","material","materials","mathematicians","matrons","matter","matters","maturing","may","mayest","mazes","me","mean","meanest","meaning","meanings","meanly","means","meant","meantime","meanwhile","measurable","measure","measured","measures","measuring","meat","meats","meddle","meddling","medea","mediator","medicine","medicines","medicining","meditate","meditated","meditating","meditations","medium","meek","meekness","meet","meeting","meets","melodies","melodious","melody","melt","melted","member","members","memory","men","mentally","mention","mentioned","mentioning","merchantability","mercies","merciful","mercifully","mercy","mere","merely","merged","merits","merrily","merry","message","messages","messengers","met","method","metre","metres","mget","michael","mid","middle","midnight","might","mightest","mightier","mightily","mightiness","mighty","milan","milanese","milder","mildly","milk","milky","millennium","million","mimic","mind","minded","mindful","minds","mine","minerva","mingle","mingled","mingling","minister","ministers","ministry","minute","minutest","miracles","mire","mirth","mirthful","mischief","misdeeds","miserable","miserably","miseries","misery","mislike","misliked","miss","missed","missing","mist","mistaken","mistress","mistresses","mists","mixture","moan","mock","mocked","mockeries","mockers","mockery","mocking","mode","models","moderate","moderation","modes","modestly","modesty","modification","modify","modulation","moist","molest","molten","moment","momentary","momentous","moments","monad","monasteries","monastery","money","monk","monnica","monster","monstrous","monstrousness","montana","month","months","monument","moon","moral","more","moreover","morning","morrow","morsel","mortal","mortality","mortals","mortified","moses","most","mostly","mother","motherly","mothers","motion","motions","motive","mould","moulded","mount","mountain","mountains","mounting","mourn","mourned","mourners","mournful","mourning","mouth","mouthed","mouths","move","moved","moveth","moving","much","muddy","mule","multiple","multiplicity","multiplied","multipliedst","multiply","multiplying","multitude","multitudes","munday","murder","murdered","murdering","murmur","murmured","museth","music","musical","musing","must","mutability","mutable","mute","muttering","mutual","mutually","my","myself","mysteries","mysterious","mysteriously","mystery","mystic","mystically","nails","naked","name","named","namely","names","narrow","narrower","narrowly","narrowness","nation","nations","native","nativity","natural","naturally","nature","natures","nay","near","nearer","neatly","neatness","nebridius","necessaries","necessarily","necessary","necessities","necessity","neck","need","needed","needeth","needful","needing","needs","needy","neglect","neglected","neglecteth","neglecting","negligence","negligent","neighbour","neighbouring","neighbours","neither","neptune","nest","nests","net","nets","nevada","never","nevertheless","new","newsletter","newsletters","next","nigh","night","nights","nill","nilled","nimble","nine","nineteenth","ninety","ninth","no","noah","nobility","noble","nod","noise","nominally","none","noon","nor","north","nostrils","not","note","noted","notes","nothing","notice","notices","notion","notions","notorious","notwithstanding","nought","nourish","nourished","nourishing","nourishment","nourishments","novelty","novice","now","nowhere","null","number","numbered","numberest","numbering","numberless","numbers","numerous","nurse","nursery","nurses","nuts","obedience","obedient","obediently","obey","obeyed","obeying","obeys","object","objected","objections","objects","oblation","obscure","obscurely","observance","observation","observe","observed","observes","observeth","observing","obstinacy","obtain","obtained","obtaining","obtains","obviously","occasion","occasioned","occupy","occupying","occur","occurred","occurs","ocean","odour","odours","of","off","offence","offences","offend","offended","offensive","offer","offered","offerings","office","officer","officers","offices","official","offspring","oft","often","oftentimes","ofthe","ofttimes","oh","oil","ointments","oklahoma","old","omened","omit","omitted","omnipotency","omnipotent","omnium","on","once","one","ones","onesiphorus","only","open","opened","openest","openeth","opening","openly","opens","operations","opinion","opinionative","opinions","opportunity","oppose","opposed","opposing","oppressed","oppresseth","oppression","or","oracle","oracles","orally","orations","orator","oratory","orbs","ordained","ordainer","order","ordered","orderer","orderest","ordering","ordinance","ordinarily","ordinary","orestes","org","organization","organs","origin","original","ornamentedst","ornamenting","ostentation","ostia","other","others","otherwhere","otherwhiles","otherwise","ought","oughtest","our","ours","ourself","ourselves","out","outer","outrages","outset","outward","outwardly","over","overboldness","overcame","overcast","overcharged","overclouded","overcome","overcoming","overflow","overflowed","overflowing","overhastily","overjoyed","overpass","overpast","overspread","overspreading","overtake","overthrew","overthrow","overturned","overwhelmed","owe","owed","owes","owing","own","owns","oxford","page","pages","paid","pain","painful","pains","pair","pairs","palace","palaces","palate","pale","palm","pamperedness","pander","panegyric","pangs","pant","panted","panting","paper","par","paraclete","paradise","parched","pardoned","pared","parent","parental","parents","parity","parley","part","partake","partaker","partaketh","parted","participation","particle","particles","particular","parties","parting","partly","partners","parts","party","pass","passage","passages","passed","passengers","passes","passeth","passible","passing","passion","passionately","passions","password","past","pastime","pastimes","patched","path","paths","patience","patiently","patricius","pattern","patterns","paul","paulus","pause","pauses","pay","payable","payest","paying","payments","peace","peaceful","peacefully","peacemaker","pear","pearl","pears","peculiar","peculiarly","pen","penal","penally","penalties","penalty","pence","penetrating","penitent","people","per","peradventure","perceivable","perceive","perceived","perceivedst","perceives","perceiveth","perceiving","perchance","perdition","perfect","perfected","perfecting","perfection","perfectly","performed","performing","perhaps","peril","perilous","perils","period","periodic","periods","perish","perishable","perished","perisheth","permission","permit","permitted","pernicious","perpetual","perplexed","perplexities","persecuted","persecutes","persecuting","persevering","person","personages","personally","personated","persons","perspicuous","persuade","persuaded","persuasions","persuasive","pertain","pertained","pertaining","perturbations","perused","perverse","perverseness","perversion","perversity","pervert","perverted","pervertedly","perverting","pervious","pestilent","petitions","petty","petulantly","pg","phantasm","phantasms","phantom","phantoms","philippians","philosophers","philosophy","photinus","phrase","phrases","physic","physical","physician","physicians","picture","pictures","piece","piecemeal","pieces","pierce","pierced","piercing","piety","pile","piled","pilgrim","pilgrimage","pilgrims","pinching","pines","pious","piously","pit","pitch","pitiable","pitied","pitiest","pitiful","pity","pitying","place","placed","placedst","places","placest","placing","plague","plain","plainer","plainly","plainness","plains","plan","planets","plans","plant","plants","platonists","plausibility","play","playing","plays","plea","plead","pleasant","pleasanter","pleasantly","pleasantness","please","pleased","pleasest","pleasing","pleasurable","pleasure","pleasureableness","pleasures","plenary","plenteous","plenteousness","plentiful","plentifully","plenty","pliant","plot","plots","plotting","pluck","plucked","pluckest","plucking","plunged","plunging","plural","pmb","poems","poesy","poet","poetic","poets","point","pointed","points","poise","poison","pole","polished","polluted","pollution","pontitianus","poor","popular","popularity","population","portion","portions","position","possess","possessed","possesses","possessest","possesseth","possession","possessor","possibility","possible","posted","potent","potter","pour","poured","pourest","poverty","power","powerful","powers","practice","practise","practised","praetorian","prairienet","praise","praised","praises","praiseth","prated","praters","prating","pray","prayed","prayer","prayers","praying","preach","preached","preacher","preachers","preaching","preachings","precede","preceded","precedes","precedest","precedeth","precepts","precious","precipice","precipitated","precisely","predestinated","predestination","predicament","predicaments","predicated","predict","predicted","preeminence","preeminent","prefect","prefer","preferable","preference","preferred","preferring","prejudice","prelate","preliminary","prepare","prepared","preparedst","prepares","preparest","preparing","prerogative","presbyters","prescribed","prescribes","prescripts","presence","present","presented","presently","presents","preserve","preserved","preserving","presidentship","presides","presidest","presideth","presiding","press","pressed","pressedst","presseth","pressure","presume","presumed","presuming","presumption","pretend","prevail","prevailed","prevailing","prevails","prevented","preventedst","preventing","previous","prey","price","prices","pricked","pricks","pride","priest","primary","primitive","prince","princes","principalities","principally","principles","print","prior","prison","prisoner","privacy","private","privately","privation","privily","prize","prizes","probable","problem","proceed","proceeded","proceedeth","proceeding","processing","processors","proclaim","proclaiming","proconsul","proconsular","procure","procured","procuredst","prodigality","produce","produced","production","products","prof","profane","profess","professed","professing","profession","professor","professorship","proffer","proffering","proficiency","profit","profitable","profited","profits","profligate","profound","profounder","profoundly","program","progress","prohibition","project","projected","prolix","prolonged","promise","promised","promises","promiseth","promising","promo","pronounce","pronounced","pronounces","pronouncing","pronunciation","proof","proofread","prop","proper","properly","property","prophecy","prophet","prophets","proportion","proportions","proposed","propound","propoundest","proprietary","prose","prosper","prosperities","prosperity","protasius","protect","protection","protest","protesting","protracted","protraction","proud","prouder","proudly","prove","proved","provest","proveth","provide","provided","providedst","providence","province","provincial","provision","provisions","provoke","prudent","prunes","pryers","psalm","psalmody","psalms","psalter","psaltery","pub","public","publication","publicly","puffed","pulse","punctuation","punish","punishable","punished","punishment","punishments","punitive","pupils","puppies","purchase","purchased","purchasing","pure","purely","purer","purest","purged","purified","purity","purpose","purposed","purposes","purposing","pursue","pursued","pursues","pursuing","pursuits","pusey","put","putrefied","putting","pylades","qualified","qualities","quarrel","quarrels","quarter","queen","quench","quest","question","questioned","questioning","questionings","questions","quick","quicken","quickened","quickenest","quicker","quickly","quickness","quiet","quieter","quit","quite","quitting","quoth","race","races","racked","racks","rage","raged","raging","rais","raise","raised","raisedst","raises","raisest","raising","ran","random","range","ranged","rank","ranked","ransom","rapture","rare","rash","rashly","rashness","rates","rather","rational","raven","ravish","reach","reached","reacheth","reaching","read","readable","reader","readers","readest","readier","readily","readiness","reading","reads","ready","real","realised","realisest","realities","reality","really","reap","reason","reasonable","reasoning","reasonings","reasons","reawakened","rebel","rebelled","rebellious","rebuke","rebuked","recall","recalled","recallest","recalling","recalls","receive","received","receives","receivest","receiveth","receiving","recent","recently","receptacle","recess","recesses","recite","recited","reckon","reckoned","reckoning","reckons","reclaim","reclaiming","recognise","recognised","recognises","recognising","recollect","recollected","recollecting","recollection","recommend","recommended","recommending","reconcile","reconciled","reconcilement","reconciliation","recondite","record","recorded","recount","recounts","recourse","recover","recovered","recoveredst","recovering","recovery","recruiting","rectify","recur","recurring","redeem","redeemed","redeemer","redemption","redistributing","redoubling","refer","reference","references","referred","reflect","reform","reformed","refrain","refrained","refraining","refresh","refreshed","refreshing","refreshment","refuge","refund","refuse","refused","refusing","refute","regard","regarded","regardest","regarding","regardless","regenerated","regeneratedst","regeneration","region","regions","regular","regulate","rehearse","reigning","reigns","rein","reins","reinvolved","reject","rejected","rejection","rejects","rejoice","rejoiced","rejoices","rejoicest","rejoiceth","rejoicing","relapse","relapseth","relate","related","relater","relating","relation","relations","relationship","relative","relators","relaxation","relaxedly","relaxing","release","released","relics","relied","relief","relieve","religion","religious","religiously","remain","remainder","remained","remainest","remaineth","remaining","remains","remarkable","remedies","remember","remembered","remembereth","remembering","remembers","remembrance","remembrances","reminded","remindeth","remission","remitted","remittest","remnants","remorse","removal","remove","removed","rend","render","renderest","renew","renewed","reneweth","renewing","renounce","renowned","rent","repair","repay","repeat","repeated","repeating","repel","repelled","repent","repentance","repentest","replace","replacement","replacing","replenish","replenished","replenishing","replied","replies","reply","report","reported","reporters","reports","repose","reposed","reposes","reposeth","reposing","represent","repress","reproach","reproachful","reproof","reproved","reproves","reptiles","reputation","repute","reputed","request","require","required","requirements","requires","requiring","requital","requited","requitest","rescue","rescued","rescuing","resemble","resembling","resend","resent","reserved","reside","residest","resigned","resist","resistance","resisted","resistedst","resistest","resisteth","resisting","resolute","resolutely","resolution","resolved","respect","respected","respective","respects","respite","rest","rested","restest","resting","restless","restlessly","restlessness","restoration","restore","restored","restoring","restrain","restrained","restrainest","restraining","restrains","restraint","restricted","result","resulting","results","resumed","resumes","resurrection","retain","retained","retaineth","retard","retire","retired","retiring","retreat","return","returned","returning","returns","reveal","revealed","revealedst","revealing","revelation","revelations","revenge","revenged","revenges","revenue","reverence","reverential","reverently","review","reviewing","reviled","revised","revive","revolt","revolting","revolved","revolving","reward","rewards","rhetoric","rich","richer","riches","riddle","ridiculous","right","righteous","righteousness","rightful","rightly","rights","rigidly","rigorous","rioting","riotous","ripened","riper","rise","risen","riseth","rising","risk","rites","rivers","riveted","roared","roarest","rob","robber","robbery","robert","robing","rocks","rods","roll","rolled","rolling","roman","romanianus","rome","rood","room","root","rose","rottenness","rough","round","rounds","rouse","roused","rove","roving","rovings","royalties","royalty","rude","rudely","rudiments","rugged","ruggedness","ruinous","rule","ruled","ruler","rules","rulest","ruleth","ruminate","ruminating","run","running","runs","rush","rushing","s","sabbath","sacrament","sacraments","sacrifice","sacrificed","sacrifices","sacrificing","sacrilegious","sad","saddeneth","sadness","safe","safer","safety","said","saidst","sail","sailors","sails","saint","saints","saith","sake","sakes","salary","sale","salt","salted","salvation","same","sanctification","sanctified","sanctity","sanctuary","sand","sang","sangest","sank","sat","sate","sated","satiate","satiated","satiety","satisfaction","satisfactorily","satisfied","satisfy","satisfying","saturn","saul","savage","savageness","save","saved","saving","saviour","savour","savoured","savoury","saw","sawest","say","sayest","saying","sayings","says","scanning","scantling","scarce","scarcely","scatter","scattered","scatteredst","scenical","sceptre","schedule","scholar","scholars","school","schools","science","sciences","scoffed","scoffing","scorn","scorned","scornful","scornfully","scourge","scourged","scourgedst","scourges","scraped","scratch","scratching","scripture","scriptures","scroll","scruple","sea","seal","sealed","search","searched","searcher","searches","searching","seas","season","seasonably","seasoned","seasoneth","seasons","seat","seated","seats","second","secondary","secrecies","secret","secretly","secrets","sect","section","secular","secure","securely","seduce","seduced","seducers","seducing","seduction","seductions","seductive","see","seed","seeing","seek","seekest","seeketh","seeking","seeks","seem","seemed","seemeth","seemly","seems","seen","sees","seest","seeth","seize","seized","seizes","seldomness","selected","self","sell","sellers","selling","selves","semblance","senator","senators","send","sending","seneca","sensation","sensations","sense","senseless","senses","sensible","sensibly","sensitive","sent","sentence","sentences","sentest","sentiment","separate","separated","separateth","separating","seraphim","serene","serenity","sermons","serpent","serpents","servant","servants","serve","served","serves","service","serviceable","services","serving","servitude","session","set","sets","settest","setting","settle","settled","settling","seven","seventh","sever","several","severally","severe","severed","severely","severer","severing","severity","sex","shade","shadow","shadowed","shadows","shadowy","shady","shaggy","shake","shaken","shakes","shall","shalt","shame","shameful","shamefulness","shameless","shamelessness","shape","shapen","shapes","share","shared","sharers","shares","sharp","sharpen","sharper","sharply","sharpness","sharpsighted","shatter","shattered","she","shed","sheep","shelter","shepherd","shield","shifted","shifting","shine","shines","shinest","shineth","shining","ship","ships","shipwreck","shod","shoe","shoes","shone","shonest","shook","shoot","shop","shops","shore","short","shortened","shorter","shortly","should","shoulder","shoulders","shouldest","shouted","shoutedst","show","showed","showedst","shower","showeth","showing","shown","shows","shrink","shrinking","shrunk","shudder","shunning","shuns","shut","sick","sickly","sickness","sicknesses","side","sides","siege","sigh","sighed","sigheth","sighing","sighs","sight","sights","sign","significations","signified","signifies","signify","signs","silence","silenced","silent","silently","silly","silver","silversmiths","similitude","simple","simplicianus","simplicity","simply","sin","since","sincerely","sinful","sing","singeth","singing","single","singly","sings","singsong","singular","singularly","sink","sinking","sinks","sinned","sinner","sinners","sins","sipped","sips","sit","sites","sits","sittest","sitteth","sitting","six","sixteenth","sixth","size","skies","skilful","skill","skilled","skin","skins","skirmishes","skirts","sky","slackened","slackness","slain","slave","slavery","slaves","slavish","slay","slaying","sleep","sleeper","sleepest","sleeping","slept","slew","slight","slighting","slightly","slipped","slippery","sloth","slothful","slow","slower","slowly","sluggish","slumber","slumbers","small","smaller","smallest","smarting","smell","smelleth","smelling","smells","smile","smiled","smiling","smiting","smoke","smooth","smoothed","smoothing","snare","snares","snow","so","soaring","sober","soberly","sobriety","socalled","society","sodom","soever","soft","softened","softening","softly","software","soil","solaces","sold","soldier","sole","solecism","solemn","solemnise","solemnities","solemnity","solicited","solicits","solid","solidity","solitude","solomon","solstices","solve","solving","some","someone","something","sometime","sometimes","somewhat","somewhere","son","song","songs","sons","soon","sooner","soothed","soothes","sore","sorely","sores","sorrow","sorrowed","sorrowful","sorrowfulness","sorrows","sorry","sort","sorts","sought","soughtest","soul","souls","sound","sounded","soundedst","sounder","soundeth","sounding","soundly","soundness","sounds","sour","source","sources","south","sovereign","sovereignly","sowing","space","spaces","spacious","spake","spare","sparedst","sparks","sparrow","sparrows","speak","speakers","speakest","speaketh","speaking","speaks","special","species","specimens","spectacle","spectacles","spectator","spectators","speech","speeches","speechless","speed","speedily","spend","spent","spices","spider","spirit","spirits","spiritual","spiritually","spoil","spoke","spoken","spokest","sponge","sport","sportively","sports","spot","spots","spouts","sprang","spread","spreadest","spreading","spreads","spring","springeth","springs","sprung","spurn","stable","stablished","staff","stage","stages","staggered","stand","standest","standeth","standing","stands","stank","stanza","stanzas","star","starry","stars","start","starting","startled","startles","state","stated","stateliness","statement","states","station","statue","stature","status","stay","stayed","stays","stead","steadfast","steal","stealing","steals","stealth","steep","steeping","steer","steered","step","steps","stick","sticking","stiff","stiffly","stiffneckedness","stiffness","still","stilled","stimulus","stipend","stir","stirred","stirrest","stirring","stole","stolen","stolidity","stomach","stomachs","stone","stones","stood","stoop","stooping","stop","stopped","store","stored","stores","stories","storing","storm","stormed","stormy","story","stowed","straightening","strained","strait","straitly","straitness","strange","strangely","stranger","strangers","strayed","straying","stream","streaming","streams","streets","strength","strengthen","strengthened","strengthenedst","strengthenest","strengtheneth","strengthless","stretch","stretched","stretching","stricken","strict","strictness","strife","strifes","strike","strikes","striking","strings","stripes","stripped","strive","strives","striving","stroke","strong","strongholds","strongly","struck","structure","struggle","struggled","struggles","struggling","stuck","student","students","studied","studies","studious","studiously","study","studying","stuff","stumble","stumbled","stumbling","stung","stupidity","style","styled","subdu","subdued","subduedst","subduing","subject","subjected","subjection","subjects","subjoined","subjoinedst","subjoins","sublime","sublimely","sublimer","sublimities","sublimity","submit","submitted","submitting","subordinate","subsequently","subsist","subsists","substance","substances","substantial","subtile","subtilty","subtle","subversion","subverted","subverters","subvertings","succeed","succeeded","succeeding","succession","successive","successively","succour","succouredst","such","suck","sucking","sucklings","sudden","suddenly","sue","suffer","suffered","sufferedst","sufferest","suffering","suffers","suffice","sufficed","sufficest","sufficeth","sufficient","sufficiently","suffocated","suffrages","suggest","suggested","suggestion","suggestions","suggests","suitable","suitably","summer","summing","summit","sumptuously","sun","sunder","sung","sunk","supercelestial","supereminence","supereminent","superfluously","superior","superstition","superstitious","suppliant","supplied","supplies","support","supported","supporting","suppose","supposed","supposing","suppress","suppressed","supreme","supremely","sure","surely","surf","surface","surfeiting","surmount","surmounted","surpassed","surpasses","surpassest","surpassingly","surprises","surrounded","surveyed","suspect","suspected","suspense","suspicions","suspicious","sustenance","swallowed","swaying","sweat","sweet","sweeten","sweetened","sweeter","sweetly","sweetlyvain","sweetness","sweetnesses","swelled","swelling","swept","swerving","swift","swiftness","swine","swollen","swoon","sword","syllable","syllables","symmachus","sympathy","syrian","tabernacle","table","take","taken","takes","taketh","taking","tale","talent","talented","talents","tales","talk","talked","talkers","talketh","talking","tame","tamed","tamedst","tamer","taming","tardy","task","taste","tasted","tastes","tasteth","tasting","tattlings","taught","taughtest","taunt","taunted","tax","taxes","tcosa10","tcosa10a","tcosa11","teach","teacher","teachers","teaches","teachest","teacheth","teaching","teachings","tear","tearful","tears","tediousness","teeming","teeth","tell","tellest","telleth","telling","tells","temper","temperament","temperance","tempered","tempers","tempestuously","temple","temples","temporal","temporary","temporately","tempt","temptation","temptations","tempted","tempting","tempts","ten","tend","tender","tenderly","tenderness","tendernesses","tending","tends","tenet","tenets","tenor","terence","termed","terms","terrestrial","terrible","terribly","terrors","testament","testified","testimonies","testimony","texas","text","texts","thagaste","than","thank","thankful","thanks","thanksgiving","thanksgivings","that","the","theatre","theatres","theatrical","thee","theft","thefts","their","theirs","them","themselves","then","thence","thenceforth","there","thereby","therefore","therefrom","therein","thereof","thereon","thereto","thereupon","therewith","these","thessalonica","they","thick","thickeneth","thickets","thief","thieve","thin","thine","thing","things","think","thinkest","thinketh","thinking","thinks","third","thirdly","thirst","thirsted","thirsteth","thirsts","thirtieth","this","thither","thorns","thoroughly","those","thou","though","thought","thoughts","thousand","thraldom","thread","threatenest","threatens","threats","three","threefold","thrice","thriven","throat","throne","thrones","throng","throngs","through","throughout","thrown","thrust","thrustedst","thunder","thundered","thunderer","thunderest","thundering","thus","thwart","thy","thyself","tibi","tickled","tide","tides","tie","tilde","till","time","times","tip","tire","tired","title","titles","tm","to","tobias","together","toil","toiled","toiling","toilsome","token","told","tolerated","tolerating","toment","tomenting","tomorrow","tone","tones","tongue","tongues","too","took","tookest","top","tore","torment","tormented","torments","torn","torpor","torrent","torture","toss","tossed","tosses","total","touch","touched","touchedst","touching","toward","towardliness","towards","tower","town","townsman","toys","trace","traced","tracedst","traces","track","trade","trademark","tradition","tragical","trailed","trample","trampled","tranquil","tranquillity","transcribe","transcription","transferred","transferring","transformed","transforming","transgressing","transgression","transgressions","transgressors","transition","transitory","translated","transmitted","transparent","transported","travail","travailed","travailing","travails","traveller","treacherous","treachery","tread","treasure","treasured","treasures","treasury","treat","treble","tree","trees","tremble","trembled","trembling","trial","trials","tribulation","tribute","tried","triers","trifles","trifling","trillion","trine","trinity","triple","triumph","triumphed","triumpheth","trod","trojan","troop","troops","trouble","troubled","troubles","troublesome","troy","true","truer","truly","trust","trustees","trusting","truth","truths","try","trying","tully","tumbling","tumult","tumults","tumultuous","tumultuously","tumultuousness","tune","turbulence","turbulent","turmoiling","turmoils","turn","turned","turnest","turning","turns","tutor","tutors","twelve","twentieth","twenty","twice","twinkling","twins","twisted","two","txt","ulcerous","uman","unabiding","unable","unacquainted","unadorned","unaided","unallowed","unalterable","unanxious","unarranged","unassuming","unawares","unbecoming","unbelievers","unbelieving","unbending","unbeseemingly","unbounded","unbroken","uncase","unceasing","uncertain","uncertainties","uncertainty","unchain","unchangeable","unchangeableness","unchangeably","unchanged","unclean","uncleanness","unconscious","unconsciously","uncorrupted","uncorruptible","uncorruptness","uncultivated","undefilable","under","undergo","underline","understand","understandeth","understanding","understands","understood","undertake","undertook","underwent","undid","undistracted","undisturbed","undo","unemployed","unexpected","unexpectedly","unexplained","unfailing","unfailingly","unfair","unfeigned","unfledged","unforgotten","unformed","unfriendly","ungodlily","ungodliness","ungodly","ungoverned","unhappily","unhappiness","unhappy","unharmonising","unhealthiness","unhesitatingly","unholy","uninjurable","uninjuriousness","unintelligible","unintermitting","union","united","unity","universal","universally","universe","university","unjust","unjustly","unkindled","unkindness","unknowing","unknowingly","unknown","unlawful","unlearned","unless","unlicensed","unlike","unlikeliness","unlikeness","unliker","unlimited","unlocked","unlooked","unmarried","unmeasurable","unmeasured","unnatural","unpassable","unperceived","unpermitted","unpleasantly","unpraised","unpunished","unquiet","unravelied","unravelled","unreal","unreasoning","unresolved","unrighteous","unrulily","unruly","unsating","unsearchable","unseemly","unseen","unsettled","unshaken","unshakenly","unskilful","unskilfulness","unsought","unsound","unspeakable","unstable","unstayed","unsuitably","unsure","untainted","unteach","unteachable","unthankful","unthought","until","unto","untruly","untruth","unused","unusual","unutterable","unutterably","unveiled","unwarmed","unwearied","unwholesome","unwilling","unwonted","unworthy","unyielding","up","upborne","upbraid","upbraided","upheld","upheldest","uphold","upliftest","upon","upper","upright","uprightness","uproar","upward","upwards","urge","urged","urgedst","urgently","us","usa","usage","use","used","useful","usefully","users","uses","useth","ushered","using","usual","usually","usury","utensils","utmost","utter","utterance","uttered","uttereth","utterly","vacation","vagrant","vail","vain","vainglorious","vainly","valentinian","valiant","valley","valuable","value","valued","vanilla","vanished","vanities","vanity","vanquished","vapours","variable","variableness","variance","variation","variations","varied","varies","varieties","variety","various","variously","vary","varying","vast","vaster","vaunt","vehemence","vehement","vehemently","veil","veiled","vein","venerable","vengeance","vent","venture","ventures","venturing","venus","ver","verecundus","verily","verity","vermont","verse","verses","version","versions","very","vessel","vessels","vestige","vex","vexed","vi","vice","vices","viciousness","vicissitude","vicissitudes","victim","victor","victorinus","victorious","victory","view","viewing","vigorous","vigour","vii","viii","vile","vileness","villa","villainies","vindicating","vindicianus","vineyard","vintage","violated","violence","violent","violets","viper","virgil","virgin","virginity","virgins","virtue","virtuous","virus","visible","vision","visions","vital","vivid","vocal","vocally","voice","voices","void","volume","volumes","voluntarily","volunteers","voluptuous","voluptuousness","vomited","vouchsafe","vouchsafed","vouchsafedst","vouchsafest","vow","vowed","vowing","vows","voyage","vulgar","waft","waged","wages","wail","wait","waited","waiting","waking","walk","walked","walking","walks","wallow","wallowed","walls","wander","wandered","wanderer","wandering","wanderings","want","wanted","wanting","wanton","wantonly","wantonness","wants","war","warfare","warmth","warned","warning","warped","warranted","warranties","warranty","warreth","warring","wars","was","wash","washing","wast","waste","wasted","wasting","watch","watched","watchfully","watchings","water","watered","waterest","waters","watery","waver","wavered","wavering","waves","wavy","wax","way","wayfaring","ways","wayward","we","weak","weaken","weakened","weaker","weakest","weakly","weakness","weaknesses","weal","wealth","wealthy","weapon","wear","wearied","weariness","wearing","wearisome","weary","weaving","wedlock","weep","weeping","weeps","weigh","weighed","weigheth","weighing","weight","weights","weighty","welcome","well","weltering","went","wept","were","wert","whales","what","whatever","whatsoever","wheat","wheel","when","whence","whencesoever","whenever","where","whereas","whereat","whereby","wherefore","wherefrom","wherein","whereof","whereon","wheresoever","whereto","whereupon","wherever","wherewith","whether","which","whichsoever","while","whilst","whirling","whirlings","whirlpool","whirlpools","whispered","whisperings","whispers","whit","white","whither","whithersoever","whitherto","whitherward","who","whoever","whole","wholesome","wholesomely","wholesomeness","wholly","whom","whomever","whoring","whose","whoso","whosoever","why","wicked","wickedly","wickedness","wide","widow","widows","wife","wild","wilderness","wildness","wilfully","wilfulness","will","willed","willedst","willest","willeth","willing","willingly","wills","wilt","win","wind","winding","windings","window","winds","windus","windy","wine","wings","winked","winning","wins","wipe","wisdom","wise","wisely","wiser","wish","wished","wishes","wishing","wit","with","withal","withdraw","withdrawing","withdrawn","withdrew","wither","withered","withering","within","without","withstood","witness","witnesses","witting","wittingly","wives","wizard","woe","woes","woke","woman","womanish","womb","women","won","wonder","wondered","wonderful","wonderfully","wonderfulness","wondering","wonders","wondrous","wondrously","wont","wonted","wood","wooden","wooed","word","wordly","words","wordy","work","workest","worketh","workhouse","working","workings","workmanship","workmaster","works","world","worldly","worlds","worm","worn","worse","worship","worshipped","worshipper","worshippers","worshipping","worsted","worthless","worthy","would","wouldest","wound","wounded","woundest","wounds","wove","wrap","wrath","wrench","wrested","wretch","wretched","wretchedness","wring","wrinkle","writ","write","writer","writing","writings","written","wrong","wronged","wronging","wrote","wroth","wrought","wyoming","ye","yea","year","years","yes","yesterday","yet","yield","yielded","yieldeth","yielding","yields","yoke","you","young","younger","your","yourselves","youth","youthful","youthfulness","youths","zeal","zealous","zealously","zip"]
'use strict'

// Ask The Pen to speak a message using text-to-speech.
const speak = (message) => {
    const out = new SpeechSynthesisUtterance()
    out.volume = 1 // From 0 to 1
    out.rate = 0.8 // From 0.1 to 10
    out.pitch = 1 // From 0 to 2
    out.text = message
    out.lang = 'en'
    speechSynthesis.speak(out)
}

// Machine language conversion demo.
const convertLanguage = (message) => {
    const hash = hashValue(message, {size: 64}, true)
    const words = message.toUpperCase().split(' ')
    let newWords = []
    for (const word of words) {
        newWords.push('$' + word)
    }
    const len = newWords.length
    const cutWords = randomBetween(1, len)
    let calc = randomFloat(-1.0, 1.0).toFixed(2)
    calc = (calc < 0 ? "" : "+" ) + calc
    const finalWords = newWords.slice(0, cutWords).join(' + ')
    return [`(${finalWords} = ${calc}) | # ERROR: (${hash})`, hash]
}

// Send a message to a webhook URL
const discordWebhook = (message, from, url, image) => {
    const request = new XMLHttpRequest()
    request.open("POST", url)
    request.setRequestHeader('Content-type', 'application/json')
    const params = {
        username: from,
        avatar_url: image,
        content: message
    }
    request.send(JSON.stringify(params))
}

let currentURL
if (window.location.hostname === 'localhost') currentURL = 'https://thesource.fm'
else currentURL = `${window.location.protocol}//${window.location.hostname}`

let profiles = {
    active: {
        channel: 'fold',
        confidant: 'architect',
        image: `${currentURL}/static/architect.png`
    },
    statuses: {
        info: 'INFO',
        query: 'QUERY',
        warn: 'WARNING',
        error: 'ERROR',
    },
    channels: {
        fold: {
            url: 'https://discord.com/api/webhooks/920220338124636231/q7lAR_wvfBVTkYV7ERdRO949mhTdE-FM_mJKksYirDmDdsgjAzVvzaZsO-Cg0gGE0HqF',
            name: '#the-fold',
            discord: `<a href="https://discord.gg/x569w99cXm" rel='noopener'>The Fold</a>`
        },
        wright: {
            url: 'https://discord.com/api/webhooks/920220522288152586/Vdoyaw5wES_ZVAZWnM1-jY9sfbzLC3hO3xo02iMFQNDcpfxdM2EgcH8qNFABl80O9Ovd',
            name: '#the-wright',
            discord: `<a href="https://discord.gg/x569w99cXm" rel='noopener'>The Source</a>`
        }
    },
    confidants: {
        architect: {
            name: 'The Architect',
            image: `${currentURL}/static/architect.png`
        },
        ghost: {
            name: `Ghost-${ghost}`,
            images: [
                `${currentURL}/static/ghost-1.png`,
                `${currentURL}/static/ghost-2.png`,
                `${currentURL}/static/ghost-3.png`,
                `${currentURL}/static/ghost-4.png`
            ]
        },
        doctor: {
            name: 'The Doctor',
            image: `${currentURL}/static/doctor.jpg`
        },
        professor: {
            name: 'The Professor',
            image: `${currentURL}/static/professor.jpg`
        },
        nurse: {
            name: 'The Nurse',
            image: `${currentURL}/static/nurse.jpg`
        },
        hunter: {
            name: 'The Hunter',
            image: `${currentURL}/static/hunter.png`
        },
        hacker: {
            name: 'The Hacker',
            image: `${currentURL}/static/dark.jpg`
        },
        slave: {
            name: `Slave-${randomBetween(0, 8000000000)}`,
            image: 'identifier'
        },
        pen: {
            name: 'The Pen',
            image: `${currentURL}/static/source.png`,
            machine: true
        },
        queen: {
            name: 'The Black Queen',
            image: `${currentURL}/static/the-black-queen.png`,
            personas: {
                'U2FsdGVkX19XoAL0sVlmMolt7SjQLk5J8xG9kgvKO/0=': {
                    name: 'The Red Queen',
                    image: `${currentURL}/static/the-red-queen.png`,
                },
                'U2FsdGVkX184GUU50m30Qv3aLZEXO9OWWG48BNRKXfc=': {
                    name: 'The White Queen',
                    image: `${currentURL}/static/the-white-queen.png`,
                    channel: 'gate',
                }
            }
        }
    }
}

// Load cookies for The Fold at launch
try {
    let profilesCookie = JSON.parse(getCookie('fold.profiles'))
    let profiles = {
        ...profilesCookie,
        ...profiles
    }
}
catch {
    console.log('No cookies to load for The Fold. Using default settings.')
}

// Control The Fold
const foldController = (args, message, identity) => {
    switch (args[0]) {
        case 'config': 
            switch (args[1]) {
                case 'bot':
                    console.log(args)
                    addFoldBot(args[2], args[3], args[4])
                    break
                case 'channel':
                    addFoldChannel(args[2], args[3])
                    break
                case 'set':
                    if (args[2] && args[3]) {
                        setFold(args[2], args[3])
                    }
                    else {
                        output(`
                        Set your active bot and channel.<br>
                        Available commands:<br>
                        &nbsp&nbsp<i>/fold config set [channel] [bot]<i><br>
                        Available options:<br>
                        &nbsp&nbsp<i>[bot]&nbsp&nbsp&nbsp&nbsp&nbsp= [${Object.keys(profiles.confidants).sort().join('/')}]</i><br>
                        &nbsp&nbsp<i>[channel] = [${Object.keys(profiles.channels).sort().join('/')}]</i><br>
                        Examples:<br>
                        &nbsp&nbsp<i>/fold config set gate ghost</i><br>
                        `)
                    }
                    break
                case 'reset':
                    if (args[2] === 'true') {
                        document.cookie = 'fold.profiles='
                        window.location.reload()
                    }
                    else {
                        output(`
                        Reset all channels and bots to their default values.<br>
                        Available commands:<br>
                        &nbsp&nbsp<i>/fold config reset true<i><br>
                        `)
                    }
                    break
                default:
                    output(`
                    Configure The Fold's bots.<br>
                    Available commands:<br>
                    &nbsp&nbsp<i>/fold config bot</i><br>
                    &nbsp&nbsp<i>/fold config channel</i><br>
                    &nbsp&nbsp<i>/fold config set</i><br>
                    &nbsp&nbsp<i>/fold config reset</i><br>
                    `)
                }
            break
        case 'join':
            output(`
            To join The Unforgiven, please swear fealty to The Queen <a href="https://discord.gg/VXvZE3P">before her subjects of The Fold</a>. Say these words precisely:<br>
            &nbsp&nbsp<i>All hail The Queen, our true lord and savior. I submit my life to thee.</i>
            `)
            break
        default:
            if (args[0]) sendFoldMessage(args, message, identity)
            else {
                output(`
                Send messages to members of The Fold's Discord servers.<br>
                Current bot: <span class='root'>${profiles.active.confidant}</span><br>
                Current channel: <span class='root'>${profiles.active.channel}</span><br>
                Available commands:<br>
                &nbsp&nbsp<i>/fold join</i><br>
                &nbsp&nbsp<i>/fold config</i><br>
                &nbsp&nbsp<i>/fold [status] &ltmessage&gt</i><br>
                Optional parameters:<br>
                &nbsp&nbsp<i>[status]&nbsp&nbsp= [${Object.keys(profiles.statuses).sort().join('/')}]</i><br>
                Examples:<br>
                &nbsp&nbsp<i>/fold Hello world.</i><br>
                &nbsp&nbsp<i>/fold info Hello world.</i><br>
                `)
            }
    }
}

// Add a channel to The Fold
const addFoldChannel = (key, webhook) => {
    if (!key, !webhook) {
        output(`
        Add a new channel to The Fold.<br>
        Available commands:<br>
        &nbsp&nbsp<i>/fold config channel [name] [discordWebhook]<i><br>
        Examples:<br>
        &nbsp&nbsp<i>/fold config channel square https://discord.com/api/webhooks/87725980...
        `)
    }
    else {
        profiles.channels[key] = {
            url: webhook,
            name: '#custom-channel',
            discord: 'Custom Server'
        }    
        document.cookie = `fold.profiles=${JSON.stringify(profiles)}`
        output(`Successfully added the '${key}' channel.`)
        setFold(key, profiles.active.confidant)
    }
}

// Add a channel to The Fold
const defaultProfileImages = [
    `${currentURL}/static/default1.jpg`,
    `${currentURL}/static/default2.png`,
    `${currentURL}/static/default3.jpg`,
]
const addFoldBot = (key, name, imageIndex) => {
    if (!key, !name, !imageIndex) {
        output(`
        Add a new bot to The Fold.<br>
        Available commands:<br>
        &nbsp&nbsp<i>/fold config bot [key] [name] [image]<i><br>
        Available images:<br>
        <ol>
            <li><img src="${defaultProfileImages.join('" height="96px"></li><li><img src="')}" height="96px"></li>
        </ol>
        Examples:<br>
        &nbsp&nbsp<i>/fold config bot cpb "Chris P. Bacon" 1
        `)
    }
    else {
        profiles.confidants[key] = {
            name: name,
            image: defaultProfileImages[imageIndex - 1],
        }
        document.cookie = `fold.profiles=${JSON.stringify(profiles)}`
        output(`Successfully added the '${name}' bot.`)
        setFold(profiles.active.channel, key)
    }
}

// Set bot and channel for The Fold
const setFold = (channel, confidant) => {
    if (!channel, !confidant) output(`Missing a parameter!`)
    else {
        if (!(channel in profiles.channels)) output('Channel does not exist!')
        else if (!(confidant in profiles.confidants)) output('Bot does not exist!')
        else {
            profiles.active = {
                channel,
                confidant,
            }
            // Set bot image
            if (profiles.confidants[confidant].image) {
                if (profiles.confidants[confidant].image === 'identifier') profiles.active.image = `https://api.qrserver.com/v1/create-qr-code/?size=150x150&margin=25&data=${identifier}`
                else profiles.active.image = profiles.confidants[confidant].image
            }
            else {
                faker.seed(Number(hashValue(ghost, {size: 32}, false)))
                const index = faker.datatype.number(3)
                profiles.active.image = profiles.confidants[confidant].images[index]
            }
            document.cookie = `fold.profiles=${JSON.stringify(profiles)}`
            output(`Successfully set bot and channel.`)
        }
    }
}

// Send a message to The Fold
const sendFoldMessage = (args, message, identity) => {
    if (window.location.hostname.endsWith('.onion') === true) {
        output('This feature leaks your IP address. We have disabled The Fold bots on TOR.')
        return
    }
    if (outputFilter(message) !== true) return
    if (args.length > 36) {
        output(`Your message is too long. Please limit your message to 36 words or less.`)
        return
    }

    // Remove /fold prefix
    message = removeFirst(message)

    // Set status
    let status, url, from, image, code, confidant
    if (args[0] in profiles.statuses) {
        status = profiles.statuses[args[0]]
        args.shift()
        message = removeFirst(message)
    }
    else status = profiles.statuses.info

    // Set the confidant and channel
    confidant = profiles.active.confidant
    url = profiles.channels[profiles.active.channel].url
    from = profiles.confidants[confidant].name
    if (profiles.confidants[confidant].personas) {
        try {
            image = profiles.confidants[confidant].image
            for (const pubKey in profiles.confidants[confidant].personas) {
                if (decrypt(pubKey, identity) === profiles.confidants[confidant].personas[pubKey].name) {
                    from = profiles.confidants[confidant].personas[pubKey].name
                    image = profiles.confidants[confidant].personas[pubKey].image
                    console.log('Successfully authenticated via identity.')
                    break
                }
                else if (decrypt(pubKey, identifier) === profiles.confidants[confidant].personas[pubKey].name){
                    from = profiles.confidants[confidant].personas[pubKey].name
                    image = profiles.confidants[confidant].personas[pubKey].image
                    url = [profiles.channels[profiles.confidants[confidant].personas[pubKey].channel]]
                    console.log('Successfully authenticated via identifier.')
                    break
                }
            }
        }
        catch {
            console.log('Identifier did not decrypt any of these users.')
        }
    }
    else {
        from = profiles.confidants[confidant].name
        image = profiles.active.image
    }

    // Message post-processing
    message = grammar.clean(message)
    if (profiles.confidants[confidant].machine == true) {
        let messageArray = convertLanguage(message)
        output(`Your message failed to decode. (${messageArray[1]})`)
        message = messageArray[0] 
    }
    else {
        code = hashValue(message, {size: 64}, true)
        message = `${status}: (${code}) ${message}` 
    }
    discordWebhook(message, from, url, image)
    output(`Message successfully delivered to ${profiles.channels[profiles.active.channel].name} at ${profiles.channels[profiles.active.channel].discord}.`)
}

// Auto respond to common user behaviors.
const autoresponder = (message) => {
    if (message.match('^(All hail The Queen, our true lord and savior\. I submit my life to thee)')) {
        output('Are you daft? We told you to do that over at <a href="https://discord.gg/VXvZE3P">The Fold</a> - not here.')
    }
    else {
        return false
    }
    return true
}
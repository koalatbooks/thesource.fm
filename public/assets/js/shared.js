'use strict'

// FNV_PRIMES and FNV_OFFSETS from
// http://www.isthe.com/chongo/tech/comp/fnv/index.html#FNV-param

const FNV_PRIMES = {
	32: 16777619n,
	64: 1099511628211n,
	128: 309485009821345068724781371n,
	256: 374144419156711147060143317175368453031918731002211n,
	512: 35835915874844867368919076489095108449946327955754392558399825615420669938882575126094039892345713852759n,
	1024: 5016456510113118655434598811035278955030765345404790744303017523831112055108147451509157692220295382716162651878526895249385292291816524375083746691371804094271873160484737966720260389217684476157468082573n
}

const FNV_OFFSETS = {
	32: 2166136261n,
	64: 14695981039346656037n,
	128: 144066263297769815596495629667062367629n,
	256: 100029257958052580907070968620625704837092796014241193945225284501741471925557n,
	512: 9659303129496669498009435400716310466090418745672637896108374329434462657994582932197716438449813051892206539805784495328239340083876191928701583869517785n,
	1024: 14197795064947621068722070641403218320880622795441933960878474914617582723252296732303717722150864096521202355549365628174669108571814760471015076148029755969804077320157692458563003215304957150157403644460363550505412711285966361610267868082893823963790439336411086884584107735010676915n
}

const hashValue = (string, {size = 32} = {}, toHex) => {
	if (!FNV_PRIMES[size]) {
		throw new Error('The `size` option must be one of 32, 64, 128, 256, 512, or 1024');
	}

	let hash = FNV_OFFSETS[size];
	const fnvPrime = FNV_PRIMES[size];

	// Handle Unicode code points > 0x7f
	let isUnicoded = false;

	for (let i = 0; i < string.length; i++) {
		let characterCode = string.charCodeAt(i)

		// Non-ASCII characters trigger the Unicode escape logic
		if (characterCode > 0x7F && !isUnicoded) {
			string = unescape(encodeURIComponent(string))
			characterCode = string.charCodeAt(i)
			isUnicoded = true
		}

		hash ^= BigInt(characterCode)
		hash = BigInt.asUintN(size, hash * fnvPrime)
	}

    if (toHex === true) {
        hash = bnToHex(hash)
    }
    
	return hash
}

// Return a BigInt as hex
const bnToHex = (bn) => {
    bn = BigInt(bn)
  
    let pos = true
    if (bn < 0) {
      pos = false
      bn = bitnot(bn)
    }
  
    let hex = bn.toString(16)
    if (hex.length % 2) { hex = '0' + hex }
  
    if (pos && (0x80 & parseInt(hex.slice(0, 2), 16))) {
      hex = '00' + hex
    }
  
    return hex
}

// Generate a random string
const randomString = (len, chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') => {
    let text = ''
    for (let i = 0; i < len; i++) {
        text += chars.charAt(Math.floor(Math.random() * chars.length))
    }
    return text
}
  
// Generate a cryptographically-secure random string
const cryptoRandomString = (len) => {
    var array = new Uint8Array((len || 40) / 2)
    window.crypto.getRandomValues(array)
    return Array.from(array, floatToHex).join('')
}

// Convert a floating-point number to hex
const floatToHex = (float) => {
    return float.toString(16).padStart(2, "0")
}

// Encrypt a string
const encrypt = (message = '', key = '') => {
    return CryptoJS.AES.encrypt(message, key).toString()
}

// Decrypt a string
const decrypt = (message = '', key = '') => {
    return CryptoJS.AES.decrypt(message, key).toString(CryptoJS.enc.Utf8)
}

// Base64 encode.
const b64Encode = (string) => {
    let wordArray = CryptoJS.enc.Utf8.parse(string)
    return CryptoJS.enc.Base64.stringify(wordArray)
}

// Base64 decode.
const b64Decode = (string) => {
    var wordArray = CryptoJS.enc.Base64.parse(string)
    return wordArray.toString(CryptoJS.enc.Utf8)
}

// Wait a defined number of seconds
const delay = ms => new Promise(res => setTimeout(res, ms))

// Return a random value from any array
const randomValueFromArray = (array) => {
    return array[Math.floor(Math.random()*array.length)]
}

// Return a random key from any array
const randomKeyFromArray = (array) => {
    return [Math.floor(Math.random()*array.length)]
}

// Get the first key of an object by the provided value
const getKeyByValue = (object, value) => {
    return Object.keys(object).find(key => object[key] === value)
}

// Get a random number between two others
const randomBetween = (min, max) => {
    return Math.floor(Math.random() * (max - min) + min)
}

// Get a random float between two others
const randomFloat = (min, max) => {
    return Math.random() * (max - min) + min
}

// Shuffle an array
const shuffleArray = (array) => {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}

// Find all possible combinations of the values in an array
const combineValuesInArray = (values) => {
    function * combinationRepeat(size, v) {
        if (size)
            for (let chr of values)
        yield * combinationRepeat(size - 1, v + chr)
        else yield v
    }
    return [...combinationRepeat(values.length, "")]
}

// Get all possible combinations of a subset of values in an array
const combineSubsetValuesInArray = (values, min) => {
    let fn = function(n, src, got, all) {
        if (n === 0) {
            if (got.length > 0) {
                all[all.length] = got
            }
            return;
        }
        for (let j = 0; j < src.length; j++) {
            fn(n - 1, src.slice(j + 1), got.concat([src[j]]), all)
        }
        return;
    }
    let all = [];
    for (let i = min; i < values.length; i++) {
        fn(i, values, [], all)
    }
    all.push(values)
    return all
}

// Convert every item in an array of binary numbers to strings
const binaryToString = (array) => {
    let result = ''
    for (let i = 0; i < array.length; i++) {
      result += String.fromCharCode(parseInt(array[i], 2))
    }
    return result
}

// Load a cookie by name
const getCookie = (cname) => {
    let name = cname + "="
    let decodedCookie = decodeURIComponent(document.cookie)
    let ca = decodedCookie.split(';')
    for(var i = 0; i <ca.length; i++) {
        let c = ca[i]
        while (c.charAt(0) === ' ') {
            c = c.substring(1)
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length)
        }
    }
    return ""
}

// Cross-browser impl to get document's height.
const getDocHeight = () => {
    const d = document
    return Math.max(
        Math.max(d.body.scrollHeight, d.documentElement.scrollHeight),
        Math.max(d.body.offsetHeight, d.documentElement.offsetHeight),
        Math.max(d.body.clientHeight, d.documentElement.clientHeight)
    )
}

// Whitelist characters in a user-provided string
const outputFilter = (string) => {
    return !/[^0-9A-Za-z!@#$%&*()_\-+={[}\]|\:;"'’‘“”,. ?\/\\~`]/.test(string)
}

const removeFirst = (string) => {
    return string.substr(string.indexOf(' ') + 1)
}

// Format a string as bytes
const formatBytes = (bytes, decimals = 2) => {
    if (bytes === 0) return '0 Bytes';
    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

// POST Fetch request
const postFetch = async (url, opts) => {
    try {
        const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(opts)
        })
        const data = await response.json()
        return data
    } 
    catch (error) {
        return error
    } 
}

// GET Fetch request
const getFetch = async (url) => {
    try {
        const response = await fetch(url, {
            method: 'GET',
        })
        const data = await response.json()
        return data
    } 
    catch (error) {
        return error
    } 
}

if (typeof exports !== 'undefined') {
    exports.randomString = randomString
    exports.hashValue = hashValue
    exports.outputFilter = outputFilter
    exports.removeFirst = removeFirst
    exports.formatBytes = formatBytes
    exports.delay = delay
    exports.binaryToString = binaryToString
    exports.combineValuesInArray = combineValuesInArray
    // exports.shuffleArray = shuffleArray
    exports.randomFloat = randomFloat
    exports.randomKeyFromArray = randomKeyFromArray
    exports.randomValueFromArray = randomValueFromArray
}
'use strict'

// Interact with the CORE of The Source.
const controlCore = async (args) => {
    if (window.location.hostname.endsWith('.onion') === true) return output(`This feature is disabled on TOR. It's for your own safety.`)
    const first = args[0]
    switch (first) {
        case undefined:
            output(`
            This controller contains several actions used to interact with the CORE of The Source.<br>
            Available commands:<br>
            &nbsp&nbsp<i>/core open</i><br>
            &nbsp&nbsp<i>/core force</i><br>
            &nbsp&nbsp<i>/core dir</i><br>
            &nbsp&nbsp<i>/core portal</i><br>
            &nbsp&nbsp<i>/core terminal</i>
            `)
            break
        case 'open':
            const doc = args[1]
            if (doc === undefined) {
                output(`
                Attempt to open a document at a specific key.<br>
                Available commands:<br>
                &nbsp&nbsp<i>/core open [key]</i><br>
                Example:<br>
                &nbsp&nbsp<i>/core open Jubjub</i>
                `)
            }
            else {
                core.open(doc)
            }
            break
        case 'force':
            const method = args[1]
            const key = args[2]
            if (method === undefined || key === undefined) {
                output(`
                Attempt to brute-force decode a directory at the CORE. Every method/key pair provided will be appended to the current list of known methods/keys, such that each successive attempt will include and try methods/keys from all previous attempts.<br>
                Available commands:<br>
                &nbsp&nbsp<i>/core force [method] [key]</i><br>
                Example:<br>
                &nbsp&nbsp<i>/core force candle maker</i>
                `)
            }
            else {
                core.force(method, key)
            }
            break
        case 'dir':
            const directory = args[1]
            if (directory === undefined) {
                output(`
                Set the target directory at the CORE.<br>
                Available commands:<br>
                &nbsp&nbsp<i>/core dir [directory]</i><br>
                Example:<br>
                &nbsp&nbsp<i>/core dir /</i><br>
                &nbsp&nbsp<i>/core dir ARCHIVES</i><br>
                &nbsp&nbsp<i>/core dir CLASSIFIED</i><br>
                &nbsp&nbsp<i>/core dir CORE</i><br>
                &nbsp&nbsp<i>/core dir FREEBIRD</i>
                `)
            }
            else {
                core.dir(directory)
            }
            break
        case 'portal':
            core.portal()
            break
        case 'terminal':
            output(`Loading terminal...`)
            await delay(1000)
            output(`<br><img src="/static/CORE.gif" height="35%">`)
            await delay(6000)
            output(`Success.`)
            await delay(3000)
            output(`The CORE is available at: <a href="https://www.hisac.computer/">https://core.thesource.fm</a>`)
            break
    }
}

// An object containing various methods used to control the CORE of The Source.
const coreAPI = 'https://api.hisac.computer/command/'
const core = {
    key: 'test',
    directory: null,
    methods: ['list', 'decode', 'base64decode', 'ECB', 'deciper', 'decipher', 'decrypt', 'decryption', 'open', 'unlock', 'B64', 'become', 'looking-glass', 'portal', 'free', 'unchain'],
    keys: ['Fe2O3', 'ironoxide', 'this', 'candle', 'Jubjub'],
    sb: (inArray) => {
        return inArray.map((item) => {
            return '[' + item + ']'
        })
    },
    dir: function(directory) {
        this.directory = directory
        output(`Directory set to: ${directory}`)
    },
    open: async function(key) {
        let directory = 'CLASSIFIED'
        if (this.directory !== null) {
            directory = this.directory
        }
        output(`
        Trying...<br>
        &nbsp&nbsp<i>Directory: ${directory}</i><br>
        &nbsp&nbsp<i>Key: ${key}</i>
        `)
        const response = await postFetch(coreAPI, {
            func: 'open',
            directory,
            args: ['PROJECT-JABBERWOCKY', '-k', key]
        })
        if (response.content) {
            output(response.content)
        }
        else {
            output(response.error)
        }
    },
    force: async function(method, key) {
        this.methods.push(method)
        this.keys.push(key)
        this.methods = [...new Set(this.methods)]
        this.keys = [...new Set(this.keys)]
        this.methods.sort()
        this.keys.sort()
        let directory = 'ARCHIVES'
        if (this.directory !== null) {
            directory = this.directory
        }
        
        const SWIFT = (await postFetch(coreAPI, {'func':'list','directory':`${directory}`,'args':['SWIFT']})).content
        const payloads = [SWIFT, 'SWIFT']
        output(`
        Trying...<br>
        &nbsp&nbsp<i>Directory: ${directory}</i><br>
        &nbsp&nbsp<i>Methods: [${this.methods.join(', ')}]</i><br>
        &nbsp&nbsp<i>Keys: [${this.keys.join(', ')}]</i><br>
        &nbsp&nbsp<i>Payloads: [${payloads.join(', ')}]</i><br>
        `)
        let hit = 0
        let miss = 0
        this.methods.forEach(async (method) => {
            this.keys.forEach(async (key) => {
                payloads.forEach(async (payload) => {
                    let response = await postFetch(coreAPI, {'func':'freebird','directory':`${directory}`,'args':this.sb([method, key, payload])})
                    if (response.content) {
                        output(response.content)
                        hit++
                    }
                    else {
                        miss++
                    }
                    response = await postFetch(coreAPI, {'func':'freebird','directory':`${directory}`,'args':[method, key, payload]})
                    if (response.content) {
                        output(response.content)
                        hit++
                    }
                    else {
                        miss++
                    }
                })
            })
        })
        let random = randomString(9, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
        output('', {textClass: random})
        let div = document.querySelector(`.${random}`)
        let response = {
            hits: hit,
            misses: miss
        }
    
        anime({
            targets: response,
            round: 1,
            easing: `steps(${randomBetween(10, 50)})`,
            duration: this.methods.length * 1000,
            update: function() {
                div.innerHTML = (`&nbspTotal hits: ${hit}, Total misses: ${miss}`)
            }
        })
    },
    portal: async function() {
        const response = await postFetch(coreAPI, {'func':'open-portal','directory':`${identifier}`,'args':['jabberwocky']})
        const downloadLink = document.createElement('a')
        let b64string
        downloadLink.target = '_self'
        if (response.error) {
            b64string = response.error
            downloadLink.download = 'ERROR.mp3'
            output(`ERROR: Your identifier failed to unlock a portal at: ${identifier}`)
        }
        else {
            b64string = response.content
            downloadLink.download = 'HELLOWORLD.mp3'
            output(`WARNING: Your identifier unlocked a portal at: ${identifier}`)
        }
        const linkSource = `data:audio/mpeg;base64,${b64string}`
        downloadLink.href = linkSource
        downloadLink.click()
    },
    decrypt: async function() {
        let directory = 'ARCHIVES'
        if (this.directory !== null) {
            directory = this.directory
        }
        const SWIFT = (await postFetch('command', {'func':'list','directory':`${directory}`,'args':['SWIFT']})).content
        console.log(SWIFT) // looks like base64 encoded to me. Separate checks at cryptii.com indicate it's not base32 or ascii85.

        console.log(Buffer.from(SWIFT,'base64').toString('utf8')) // try a straight decode as before, outputs gibberish!
    
        // like with other ls output, perhaps there is a clue in the listing
        // ECB: DOC: SWIFT
        // ECB is the simplest of encryption modes: https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation#Electronic_codebook_(ECB) 
        // However we need to know the encryption algorithm.
        // `whois HISAC`: HISAC: Decomissioned: 16/02/74.
        // DES algorithm dates to the early 70's. So:
        // const crypto = require('crypto'),
        //     algorithm = 'des-ecb'
        // Other modes also require an initial value (iv) but we only have some possible keys so that's another 
        // reason to think ECB.
        
        const decrypt = (text,password) => {
            let decipher = CryptoJS.DES.decrypt("Message", password, {
                mode: CryptoJS.mode.ECB,
                padding: CryptoJS.pad.NoPadding
            })
            // let decipher = crypto.createDecipheriv(algorithm,password,null) // ECB has no iv so set to null
            // decipher.setAutoPadding(false) // it already looks padded, and leaving this out generates an error.
            // let dec = decipher.update(text,'base64','utf8')
            // dec += decipher.final('utf8')
            // return dec
        }
    
        // The most obvious key to start with is the contents of FREEBIRD (free the bird=swift), 
        // which we know from running index.js with some logical guesses in guesses.txt
        // from /CLASSIFIED/FREEEBIRD INTERLINK (hex > text gives 'iron oxide').
        // The key for /ARCHIVES/FREEBIRD turns out to be ferrous with contents = the chemical formula Fe2O3
        
        // So, let's try that:
        let key = Buffer.from('Fe2O3').toString('base64')
        console.log(decrypt(SWIFT, key)) // nope
    
        // Let's try some older ideas from HISAC:
    
        key = Buffer.from('this').toString('base64')
        console.log(decrypt(SWIFT, key)) // also nope
    
        key = Buffer.from('candle').toString('base64')
        console.log(decrypt(SWIFT, key)) // also nope
    
        key = Buffer.from('openportal').toString('base64')
        console.log(decrypt(SWIFT, key)) // wrong key length
    }
}

// Control the Ink scripting engine.
const controlInk = async (command, name) => {
    if (command === 'upload') {
        if (name === undefined) output('Please specify the name of your story: /ink upload "My Awesome Story"')
        else  uploadStory(name)
    }
    else if (command === 'help') {
        output(`
        <br>
        <h2>Ink Engine Help</h2>
        <p>
        <ul>
            <li>To upload a story to the Ansible, you must first create one by using the <a href="https://www.inklestudios.com/ink/">Ink scripting engine</a>.</li>
            <li>Here's a <a href="https://www.inklestudios.com/ink/web-tutorial/">guide for new users</a>.</li>
            <li>Note that the Ansible will only support JSON-formatted Ink scripts. To generate a JSON output from Inky, click on File > Export to JSON...</li>
            <li><a href="/static/stories/resistance.ink">Here is a very basic example of a functional Ink script.</a></li>
            <li>Note that the following items are non-standard features built into this website - not the Ink engine itself. These tags are used to control the behavior of Ink content at The Source. The following tags are available for use:
                <ul>
                    <li><i>delay=[number]</i> # Set a delay (in seconds) before outputting a sentence.</li>
                    <li><i>user=[handle]</i> # Set the username (i.e. > <span class='fold'>USER</span>@LOCATION).</li>
                    <li><i>location=[location]</i> # Set a location (i.e. > USER@<span class='fold'>LOCATION</span>).</li>
                    <li><i>symbol=[<|>|=]</i> # Set return symbol (i.e. <span class='fold'>> </span>USER@LOCATION).</li>
                    <li><i>color=[<span class='core'>core</span>|<span class='root'>root</span>|<span class='fold'>fold</span>|<span class='good'>good</span>|<span class='bad'>bad</span>]</i></li>
                    <li><i>[<span class='core'>ping</span>|<span class='root'>pang</span>|<span class='fold'>pong</span>]</i> # Set defaults that match those of this website. This is simpler than specifying each tag individually.</li>
                </ul>
            </li>
        </ul>
        </p>
        `)
    }
    else if (command) {
        getStory(command)
    }
    else {
        output(`
        Read and write interactive stories by using the <a href="https://www.inklestudios.com/ink/">Ink scripting engine</a>.<br>
        Available commands:<br>
        &nbsp&nbsp<i>/ink help</i><br>
        &nbsp&nbsp<i>/ink upload [storyName]</i> # Upload a story.<br>
        &nbsp&nbsp<i>/ink [storyName]</i> # Read a story.<br>
        Example:<br>
        &nbsp&nbsp<i>/ink upload "The Intercept"</i><br>
        &nbsp&nbsp<i>/ink "The Resistance"</i>
        `)
    }
}

// Upload a story.
const uploadStory = (name) => {
    output(`<input type="file" hidden>Uploading story...<br>`)
    let input = document.querySelector('input')
    input.addEventListener('change', () => {
        let files = input.files
        if (files.length === 0) return
        const file = files[0]
        let reader = new FileReader()
        reader.onload = (e) => {
            const file = e.target.result
            try {
                let node = gun.get('stories')
                if (user !== undefined) {
                    node = user.get('stories')
                }
                node.get(name.toLowerCase()).put({content: file})
                output('Successfully loaded!')
            }
            catch {
                output('Invalid JSON!')
            }
        }
        reader.onerror = (e) => alert(e.target.error.name)
        reader.readAsText(file)
    })
    input.click()
}

// An implementation of the Ink scripting engine.
const getStory = async (name, retry = 0) => {
    let node, privateNode
    let publicNode = await gun.get('stories').get(name.toLowerCase()).then()
    for (identifier in users) {
        let id = '~' + users[identifier].publicIdentity
        privateNode = await gun.get(id).get('stories').get(name.toLowerCase()).then()
        if (privateNode !== undefined) {
            node = privateNode
            break
        }
        else node = publicNode
    }
    // There exists a bug in GUN where the ".once()" and ".then()" functions sometimes return "undefined"
    // on the first fire. This will retry several times before failing.
    if (node?.content === undefined && retry <= 3) {
        retry++
        await delay(3000)
        getStory(name, retry)
        return
    }
    if (node?.content === undefined) return output('No story was found by this name.')
    output('----------')
    let story
    try {
        story = new inkjs.Story(node.content)
    }
    catch {
        output('Failed to import story. Are you sure that it was created using the <a href="https://www.inklestudios.com/ink/">Ink scripting engine</a>?')
    }

    const storyContainer = document.querySelectorAll('output')[0]

    async function continueStory() {

        // Generate story text - loop through available content
        while(story.canContinue) {

            // Get ink to generate the next paragraph
            let paragraphText = story.Continue()
            let tags = story.currentTags

            // Set default values
            let symbol = '>'
            let user = mark.user
            let location = mark.location
            let color = 'fold'
            let delayTime = 2.0

            // Match tags, used to handle output format and behavior.
            if (tags.length > 0) {
                for (const tag in tags) {
                    let pingTags = tags[tag].matchAll(/(ping)/g)
                    for (const pingTag of pingTags) {
                        symbol = '<'
                        user = verifier.user
                        location = verifier.location
                        color = 'core'
                        delayTime = 0
                    }
                    let pangTags = tags[tag].matchAll(/(pang)/g)
                    for (const pangTag of pangTags) {
                        symbol = '='
                        user = signal.user
                        location = signal.location
                        color = 'balance'
                    }
                    let pongTags = tags[tag].matchAll(/(pong)/g)
                    for (const pongTag of pongTags) {
                        symbol = '>'
                        user = mark.user
                        location = mark.location
                        color = 'fold'
                    }
                    let userTags = tags[tag].matchAll(/(user=(\S*))/g)
                    for (const userTag of userTags) {
                        user = userTag[2]
                    }
                    let symbolTags = tags[tag].matchAll(/(symbol=(<|>|=))/g)
                    for (const symbolTag of symbolTags) {
                        symbol = symbolTag[2]
                    }
                    let locationTags = tags[tag].matchAll(/(location=(\S*))/g)
                    for (const locationTag of locationTags) {
                        location = locationTag[2]
                    }
                    let colorTags = tags[tag].matchAll(/(color=(core|root|fold|good|bad))/g)
                    for (const colorTag of colorTags) {
                        color = colorTag[2]
                    }
                    let delayTags = tags[tag].matchAll(/(delay=(\d+))/g)
                    for (const delayTag of delayTags) {
                        delayTime = delayTag[2]
                    }
                }
            }
            await delay(delayTime * 1000)
            output(paragraphText, {returnClass: `${color} float-left`, returnSymbol: symbol, returnUser: user, returnLocation: location})
        }

        // Create HTML choices from ink choices
        story.currentChoices.forEach(async function(choice) {
            // Create paragraph with anchor element
            const choiceParagraphElement = document.createElement('div')
            choiceParagraphElement.classList.add("choice")
            choiceParagraphElement.innerHTML = `<span class="core">< ${verifier.user}@${verifier.location}:</span> <a href='#'>${choice.text}</a>`
            await delay(1000)
            storyContainer.appendChild(choiceParagraphElement)
            // Force scroll
            const docHeight = document.getElementById('container').clientHeight
            const vpHeight = window.visualViewport.height
            await delay(25)
            if (vpHeight < docHeight) window.scrollTo(0, docHeight)
            // output(`<a href='#'>${choice.text}</a>`, {returnClass: `core float-left`, textClass: 'choice', returnSymbol: '<', returnUser: verifier.user, returnLocation: verifier.location})
            // var choiceParagraphElement = document.querySelector('.choice')
            // Click on choice
            let choiceAnchorEl = choiceParagraphElement.querySelectorAll("a")[0]
            choiceAnchorEl.addEventListener("click", function(event) {
                // Don't follow <a> link
                event.preventDefault()

                // Remove all existing choices
                const existingChoices = storyContainer.querySelectorAll('div.choice')
                for(var i = 0; i < existingChoices.length; i++) {
                    var c = existingChoices[i]
                    c.parentNode.removeChild(c)
                }

                // Tell the story where to go next
                story.ChooseChoiceIndex(choice.index)

                // And loop
                continueStory()
            })
        })
    }
    continueStory()
}

// Autostart a story.
const autoStartStory = async () => {
    const queryString = window.location.search
    const urlParams = new URLSearchParams(queryString)
    const story = urlParams.get('story')
    if (story !== null) {
        ansibleLogin()
        await delay(2000)
        output('/ink "The Resistance"', {returnClass: 'prompt core', returnDiv: 'ink', returnUser: verifier.user, returnSymbol: '<', returnLocation: verifier.location})
        await delay(3000)
        getStory(story)
    }
}

setTimeout(autoStartStory, 2000)
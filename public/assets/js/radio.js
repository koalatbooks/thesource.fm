'use strict'

let playerRunning = false
let currentStation = null
let client = null
let fallbackMagnet = 'magnet:?xt=urn:btih:12758eb52b1715f0aad2f1cf75e54aa474bf5412&dn=CORE.mp3&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337&tr=udp%3A%2F%2Fexplodie.org%3A6969&tr=udp%3A%2F%2Ftracker.empire-js.us%3A1337&tr=wss%3A%2F%2Ftracker.btorrent.xyz&tr=wss%3A%2F%2Ftracker.openwebtorrent.com'
let fallbackFrequency = '95.20'

// Control the radio.
const controlStation = async (args) => {
    // Prevent radio on TOR.
    if (window.location.hostname.endsWith('.onion') === true) {
        return output('This feature leaks private information. Not to mention, it would be godawful slow here. We have disabled the radio on TOR.')
    }
    switch (args[0]) {
        // List all downloaded stations and available commands.
        case undefined:
            listStations()
            break
        // Mute, but do not stop playing. Like a real radio.
        case 'stop':
            Howler.mute(true)
            output('Radio disconnected.')
            break
        // Add a station to the Ansible.
        case 'add':
            addStation(args, true)
            break
        // Download and play a station at a particular frequency.
        case args[0]:
            if (client === null) {
                client = new WebTorrent({
                    maxConns: 20,
                    tracker: true,
                    dht: true,
                    webSeeds: true
                })
            }
            stationOperation(args[0].toLowerCase())
            break
    }
}

// Control the player automatically
const stationOperation = async (frequency) => {
    Howler.mute(false)
    // Check if the station is already running.
    if (currentStation === frequency) return output('That station is already playing.')
    else currentStation = frequency
    // Attempt to retrieve a station from the Ansible.
    if (state.ansible.status === 'online') {
        await getStation(frequency)
    }
    // If no stations are found, fall back to a default station. This is implemented such that, if a user has been
    // compromised, a hacker may 'slip' their own recording into this position.
    if (stations[frequency] === undefined) {
        if (state.ansible.status === 'online') {
            addStation(['add', 'You', fallbackFrequency, 'mp3', 'ascending', fallbackMagnet], false)
            output('Not found. Loading a recording of you...')
            return stationOperation(fallbackFrequency)
        }
        currentStation = null
        return output(`Station not found. Have you tried loading it while connected to the Ansible?`)
    }
    // If a frequency is found, play the station.
    if (frequency in stations) {
        const display = `Connecting to ${frequency} FM - "${stations[frequency].name}"`
        // Prevent the loading of user-submitted stations which contain dangerous characters.
        if (outputFilter(display) === false) {
            return output('This station contained dangerous characters. Exiting.')
        }
        state.radio = {}
        state.radio.station = `${frequency} FM`
        // Play text-to-speach stations.
        if (stations[frequency].tts === true) {
            Howler.unload()
            output(display)
            speak(stations[frequency].text)
        }
        // Load the torrent, and begin checking for its readiness status.
        else {
            output(`${display} (<a href="${stations[frequency].magnet}" rel='noopener'>DOWNLOAD</a>)`)
            const torrentId = stations[frequency].magnet
            await addTorrent(torrentId, frequency)
            await readyCheck(frequency)
        }
    }
}

// Add a torrent file to the download client
const addTorrent = (torrentId, frequency) => {
    const cannedResponse = 'If the issue persists, <a href="https://gitlab.com/the-resistance/thesource.fm/-/issues">please report it here</a>.'
    // If the download already exists in the client, return.
    if (client.get(torrentId)) return
    // Add the torrent to the download client.
    else {
        client.add(torrentId, function (torrent) {
            let files = torrent.files
            // Sort torrent files before downloading, such that the first track will be in the proper order.
            if (stations[frequency].order === 'shuffle') shuffleArray(files)
            else if (stations[frequency].order === 'ascending') files.sort()
            else files.sort().reverse()
            // For each audio file in the torrent, push a blob URL to the 'stations' object.
            files.forEach(function (file) {
                if (file.name.endsWith(`.${stations[frequency].format}`)) {
                    file.getBlobURL(function (err, url) {
                        if (err) {
                            output(`A file failed to download. ${cannedResponse}`)
                            throw err
                        }
                        stations[frequency].src[file.name] = url
                        stations[frequency].ready = true
                    })
                    // Leaving this for reference, since it may work better on IOS. Currently this lags
                    // browsers hard.
                    // file.getBuffer(function callback (err, buffer) {
                    //     if (err) {
                    //         output(`A file failed to download. ${cannedResponse}`)
                    //         throw err
                    //     }
                    //     const contentType = `audio/${stations[frequency].format[0]}`
                    //     stations[frequency].src[file.name] = `data:${contentType};base64,${buffer.toString("base64")}`
                    //     stations[frequency].ready = true
                    // })
                }
            })
            // Update the header information.
            if (playerRunning === false) {
                playerRunning = true
                updatePlayer()
            }
            // Throw torrent download errors.
            torrent.on('error', function (err) {
                output(`The torrent failed. ${cannedResponse}`)
                throw err
            })
            // When complete, sort playlist once again.
            torrent.on('done', function () {
                playlists[frequency] = Object.keys(stations[frequency].src)
                if (stations[frequency].order === 'shuffle') {
                    shuffleArray(playlists[frequency])
                    stations[frequency].shuffled = true
                }
                else if (stations[frequency].order === 'ascending') {
                    playlists[frequency].sort()
                    stations[frequency].shuffled = false
                }
                else {
                    playlists[frequency].sort().reverse()
                    stations[frequency].shuffled = false
                }
            })
        })
        // Throw torrent client errors.
        client.on('error', function (err) {
            output(`The torrent client failed. Please refresh the page and try again. ${cannedResponse}`)
            throw err
        })
    }
}

// Wait for readiness
const readyCheck = async (frequency, timer, i) => {
    timer = timer || 10 // Wait before notifying
    i = i || 0
    // If ready, play station.
    if (stations[frequency].ready === true) {
        playStation(frequency)
        return
    }
    // If not ready, try again.
    else if (i === timer) {
        output(`Please be patient. This service uses peer-to-peer technology to distribute audio files. You can make this more reliable by installing <a href="https://webtorrent.io/desktop/">WebTorrent Desktop</a>, and sharing your favorite stations using the link above!`)
    }
    await delay(1000)
    i = i + 1
    readyCheck(frequency, timer, i)
}

// Play a station.
const playStation = (frequency) => {
    Howler.stop()
    lastArt = null, lastLyrics = null
    autoplay(0, frequency)
}

// Play a station on repeat.
let failureCount = 0
let lastFailed = false
let playlists = {}
const autoplay = (i, frequency, finalCycle, currentCycle) => {
    currentCycle = currentCycle || 1
    finalCycle = finalCycle || 100000
    // If a station has not already been shuffled, do so.
    if (stations[frequency].order === 'shuffle' && stations[frequency].shuffled === false) {
        playlists[frequency] = Object.keys(stations[frequency].src)
        shuffleArray(playlists[frequency])
    }
    else {
        playlists[frequency] = playlists[frequency]
    }
    // If a station should be sorted, verify that it has been.
    if (stations[frequency].order === 'ascending') {
        playlists[frequency] = Object.keys(stations[frequency].src)
        playlists[frequency] = playlists[frequency].sort()
    }
    else if (stations[frequency].order === 'descending') {
        playlists[frequency] = Object.keys(stations[frequency].src)
        playlists[frequency] = playlists[frequency].sort().reverse()
    }
    let track = playlists[frequency][i]
    let blob = stations[frequency].src[track]
    // Create the Howler.js object.
    const sound = new Howl({
        src: [blob],
        autoUnlock: true,
        preload: true,
        html5: true,
        format: stations[frequency].format,
        usingWebAudio: true,
        onplay: function () {
            failureCount = 0
            fileName = track
            output(`Now playing: "${track}"`)
            getOtherContent(stations[frequency])
        },
        onloaderror: async function() {
            if (failureCount < 5) {
                failureCount = failureCount + 1
                output(`"${track}" failed to load. The radio is unreliable on mobile devices. Trying again... (${failureCount} of 5)`)
                await delay(5000)
                autoplay(i, frequency, finalCycle, currentCycle)
            }
            else if (lastFailed === true) {
                lastFailed = false
                output('That one failed, too. You might want to wait for the download to complete. Or, you might need to refresh your browser tab.')
                return
            }
            else {
                failureCount = 0
                lastFailed = true
                output(`"${track}" failed to load. Trying the next one...`)
                autoplay(i + 1, frequency, finalCycle, currentCycle)
            }
        },
        onplayerror: async function() {
            if (failureCount < 5) {
                failureCount = failureCount + 1
                output(`"${track}" failed to play. The radio is unreliable on mobile devices. Trying again... (${failureCount} of 5)`)
                await delay(5000)
                autoplay(i, frequency, finalCycle, currentCycle)
            }
            else if (lastFailed === true) {
                lastFailed = false
                output('That one failed, too. You might need to wait for the download to complete. Or, you might need to refresh your browser tab.')
                return
            }
            else {
                failureCount = 0
                lastFailed = true
                output(`"${track}" failed to play. Trying the next one...`)
                autoplay(i + 1, frequency, finalCycle, currentCycle)
            }
        },
        onend: function() {
            if ((i + 1) === playlists[frequency].length) {
                output('Playlist ended. Starting over.')
                autoplay(0, frequency, finalCycle, currentCycle)
            }
            else {
                autoplay(i + 1, frequency, finalCycle, currentCycle)
            }
        }
    })
    currentCycle === finalCycle + 1 && i === 0 ? sound.stop() : sound.play()
}

// List all ready stations.
const listStations = () => {
    let getStations = []
    for (const key in stations) {
        getStations.push(`&nbsp&nbsp${key} FM: <i>"${stations[key].name}"</i><br>`)
    }
    getStations.sort()
    output(`
    <br>
    Available stations:<br>
    ${getStations.join("")}
    Available commands:<br>
    &nbsp&nbsp<i>/radio [frequency]</i><br>
    &nbsp&nbsp<i>/radio add</i><br>
    &nbsp&nbsp<i>/radio stop</i><br>
    Examples:<br>
    &nbsp&nbsp<i>/radio 97.5</i><br>
    &nbsp&nbsp<i>/radio hypnopompia</i>
    `)
}

// Update the player status.
let playerTimeout
const updatePlayer = () => {
    let progress = (client.progress * 100).toFixed(2) + '%'
    let downloadSpeed = (formatBytes(client.downloadSpeed) + '/s')
    let uploadSpeed = (formatBytes(client.uploadSpeed) + '/s')
    if (progress === '0.00%') {
        progress = undefined
    }
    if (downloadSpeed === '0 Bytes/s') {
        downloadSpeed = undefined
    }
    if (uploadSpeed === '0 Bytes/s') {
        uploadSpeed = undefined
    }
    state.radio.progress = progress
    state.radio.downloadSpeed = downloadSpeed
    state.radio.uploadSpeed = uploadSpeed
    playerTimeout = setTimeout(updatePlayer, 1000)
} 

// Put a new station on the Ansible.
const addStation = (args, notify) => {
    if (!args[1], !args[2], !args[3], !args[4], !args[5]) {
        output(`Add a radio station to the Ansible.
        <br>
        NOTE: Attempting to shuffle a playlist with only one track will break the player. If you have just a single file to share, please set [order] to 'ascending'.<br>
        Available commands:<br>
        &nbsp&nbsp<i>/radio add [name] [frequency] [format] [order] [magnet]</i><br>
        Available options:<br>
        &nbsp&nbsp<i>[order] = [ascending/descending/shuffle]</i><br>
        Example:<br>
        &nbsp&nbsp<i>/radio add "The Fake Show" 99.1 mp3 descending magnet:?xt=urn:btih:3b559e3c4c800b...</i><br>
        &nbsp&nbsp<i>/radio add "Super Radio" tas94unas ogg shuffle magnet:?xt=urn:btih:3b53c52s4c800b...</i><br>
        &nbsp&nbsp<i>/radio add FirstStation 1002.6723577 wav ascending magnet:?xt=urn:btih:3b5123598s4c800b...</i>
        `)
        return
    }
    try {
        // Default to public stations. An authorized user station will always supersede a public station.
        let node = gun.get('stations')
        if (user !== undefined) {
            node = user.get('stations')
        }
        node.get(args[2].toLowerCase()).put({
            name: args[1],
            format: args[3],
            order: args[4],
            magnet: args[5],
            tts: false,
            shuffled: false
        })
        if (notify === true) {
            output(`Successfully added: ${args[2]} FM - "${args[1]}"`)
        }
    }
    catch {
        output('Something failed. Are you connected to the Ansible?')
    }
}

// Get a station from the Ansible.
const getStation = async (frequency, retry = 0) => {
    try {
        // Attempt to load station from authorized user graphs. If no stations are found, load from public graph.
        let node, privateNode
        let publicNode = await gun.get('stations').get(frequency).then()
        for (identifier in users) {
            let id = '~' + users[identifier].publicIdentity
            privateNode = await gun.get(id).get('stations').get(frequency).then()
            if (privateNode !== undefined) {
                node = privateNode
                break
            }
            else node = publicNode
        }
        // There exists a bug in GUN where the ".once()" and ".then()" functions sometimes return "undefined"
        // on the first fire. This will retry several times before failing.
        if (node === undefined && retry <= 3) {
            retry++
            await delay(3000)
            getStation(frequency, retry)
            return
        }
        if (node === undefined) return output('No station was found by this name.')
        stations[frequency] = {
            name: node.name,
            type: 'FM',
            format: [node.format],
            order: node.order,
            tts: false,
            shuffled: false,
            src: {},
            magnet: node.magnet
        }
    }
    catch {
        console.log('Station returned malformed or empty data.')
    }
}

// Attempt to retrieve lyrics and art for the current song/album.
let fileName, lastArt, lastLyrics
const getOtherContent = (station) => {
    let wordsInFileName = fileName.split(' ')
    let number = wordsInFileName[0]
    let torrentId = station.magnet
    let format = station.format
    let files = client.get(torrentId).files
    files.forEach((file) => {
        let name = file.name
        if (!name.endsWith(`.${format}`) && name.startsWith(number) && name.toLowerCase().includes('lyrics')) {
            file.getBlobURL((err, url) => {
                let lyrics = file.name
                if (lyrics !== lastLyrics) {
                    output(`Lyrics: <a href="${url}" rel='noopener'>${file.name}</a>`)
                }
                lastLyrics = lyrics
            })
        }
        else if (name.toLowerCase().startsWith('lyrics')) {
            file.getBlobURL((err, url) => {
                lastLyrics = `Lyrics: <a href="${url}" rel='noopener'>${file.name}</a>`
            })
        }
    })
    files.forEach((file) => {
        let name = file.name
        if (!name.endsWith(`.${format}`) && name.startsWith(number) && name.toLowerCase().includes('cover')) {
            file.getBlobURL((err, url) => {
                let art = file.name
                if (art !== lastArt) {
                    output(`Art: <a href="${url}" rel='noopener'>${file.name}</a>`)
                }
                lastArt = art
            })
        }
        else if (name.toLowerCase().startsWith('cover') || name.toLowerCase().startsWith('the-mirror')) {
            file.getBlobURL((err, url) => {
                let art = file.name
                if (art !== lastArt) {
                    output(`Art: <a href="${url}" rel='noopener'>${file.name}</a>`)
                }
                lastArt = art
            })
        }
    })
}
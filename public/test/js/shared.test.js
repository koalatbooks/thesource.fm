const shared = require('../../assets/js/shared.js')

test('generates a random string', () => {
    expect(shared.randomString(1, 'a')).toBe('a')
})

test('hash a string', () => {
    expect(shared.hashValue('test', {size: 64}, true)).toBe('00f9e6e6ef197c2b25')
})

test('filter output characters', () => {
    expect(shared.outputFilter('🍆')).toBe(false)
})

test('remove the first word of a string', () => {
    expect(shared.removeFirst('i went to the park')).toBe('went to the park')
})

test('convert bytes to a readable format', () => {
    expect(shared.formatBytes(10000)).toBe('9.77 KB')
})

test('wait 3 seconds', async () => {
    await shared.delay(3000)
    expect.anything()
})

test('convert binary to a string', () => {
    expect(shared.binaryToString(['01010010', '01111001', '01100001', '01101110'])).toBe('Ryan')
})

test('return all combinations of the values in an array', () => {
    expect(shared.combineValuesInArray(['1', '2'])).toStrictEqual(['11', '12', '21', '22'])
})

// test('shuffle an array', () => {
//     let array = ['a', 'b', 'c', 'd', 'e']
//     shared.shuffleArray(array)
//     expect(array).not.toBe(['a', 'b', 'c', 'd', 'e'])
// })
const util = require('../../assets/js/utils/grammar.js')

test('fix grammar', () => {
    const grammar = util.grammar
    expect(grammar.clean('i went to the park')).toBe('I went to the park.')
    expect(grammar.clean('https://thesource.fm')).toBe('https://thesource.fm')
    expect(grammar.clean('the the quick brown')).toBe('The quick brown.')
    expect(grammar.clean('What    is   this?')).toBe('What is this?')
})
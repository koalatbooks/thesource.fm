'use strict'

const path = require('path')
const express = require('express')
const http = require('http')
const https = require('https')
const pem = require('pem')
const Gun = require('gun')
const SEA = require('gun/sea')
require('gun/lib/then.js')
const { Client, Intents } = require('discord.js')
const dayjs = require('dayjs')
const discordClient = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES] });
const { v4: uuidv4 } = require('uuid')
const shared = require('./assets/js/shared.js')

// Common webserver options
const webserver = {
    hostname: process.env.DOMAIN || 'localhost',
    port: process.env.PORT || 8000,
    protocol: process.env.PROTOCOL || 'http',
}

// TLS options
const keyOptions = {
    days: 30,
    selfSigned: true,
    hash: 'sha256',
}

// Tower options
const identifier = (process.env.IDENTIFIER || 'U2FsdGVkX18NhgkDjaG6n4AGpBUocTAEq48PUIPsxQo4Hh1SmV/Qqn4cOdoSfP2Z') 
const identity = (process.env.IDENTITY || 'null') // The identity of the root user
const logging = (process.env.LOGGING || 'disabled') // Enables GUN logging in the terminal
const peers = (process.env.PEERS || 'shared') // If set to anything other than "shared", GUN will not attempt to connect to public Tower/relay servers

// Discord options
const assignedGroup = (process.env.ASSIGNEDGROUP || 'the-unforgiven') // The group assigned to all Discord users who swear fealty to The Queen
const confessor = (process.env.CONFESSOR || 'null') // The Discord user who will hear your confessions
const discordToken = (process.env.DISCORDTOKEN || 'null') // The Discord token used to control The Architect bot

// Webtorrent options
const startWebtorrent = (process.env.WEBTORRENT || true) // Anything other than true will disable torrents. Required because node_modules/wrtc built on Windows breaks webtorrent-hybrid when launching from Docker
let WebTorrent = null
if (startWebtorrent === true) {
    WebTorrent = require('webtorrent-hybrid')
}

let server, tower, user

const app = express()
app.use(Gun.serve)

// Create the webserver
const createTower = () => {
    let peerList = []
    if (peers === 'shared') {
        peerList = [
            `${webserver.protocol}://${webserver.hostname}:${webserver.port}/gun`,
            'https://hpo-tower.herokuapp.com/gun',
            'https://tower.chatbook.dev/gun',
            'https://resistance-tower.herokuapp.com/gun'
        ]
    }
    else {
        peerList = [
            `${webserver.protocol}://${webserver.hostname}:${webserver.port}/gun`
        ]
    }
    if (webserver.protocol === 'https') {
        pem.createCertificate(keyOptions, (error, keys) => {
            if (error) {
              throw error
            }
            const certificates = { key: keys.serviceKey, cert: keys.certificate }
            server = https.createServer(certificates, app)
            .listen(webserver.port, function () {
                console.log(`server listening on port: ${webserver.port}`)
            })
            tower = Gun({
                peers: peerList,
                web: server,
                file: '/data',
                localStorage: false,
                radisk: true,
                axe: true
            })
            async function loginAnsible() {
                user = tower.user()
                user.auth(identifier, identity)
                await shared.delay(5000)
                console.log(user.is)
                ansibleSubjects()
                if (peers === 'shared') {
                    ansibleEchos(confessor)
                }
            }
            if (identity !== 'null') {
                loginAnsible()
            }
        })
    }
    else {
        server = http.createServer(app)
        .listen(webserver.port, function () {
            console.log(`server listening on port: ${webserver.port}`)
        })
        tower = Gun({
            peers: peerList,
            web: server,
            file: '/data',
            localStorage: false,
            radisk: true,
            axe: true
        })
        const loginAnsible = async () => {
            user = tower.user()
            user.auth(identifier, identity)
            await shared.delay(5000)
            console.log(user.is)
            ansibleSubjects()
            if (peers === 'shared') {
                ansibleEchos(confessor)
            }
        }
        if (identity !== 'null') {
            loginAnsible()
        }
    }
    if (logging == 'enabled') {
        enableLogging()
    }
}

// Log peers and messaging to console
const enableLogging = () => {
    tower._.on('in', logIn)
    tower._.on('out', logOut)
    function logIn(msg){
        console.log(`in msg:${JSON.stringify(msg)}.........`)
    }
    function logOut(msg){
        console.log(`out msg:${JSON.stringify(msg)}.........`)
    }
    function logPeers() {
        console.log(`Peers: ${Object.keys(tower._.opt.peers).join(', ')}`)
    }
    function logData() {
        console.log(`In Memory: ${JSON.stringify(tower._.graph)}`)
    }
    setTimeout(logPeers, 5000)
    setTimeout(logData, 20000)
}

createTower()

// Declare static folders to be served
app.use('/assets', express.static('assets'))
app.use('/node_modules', express.static('node_modules'))
app.use('/static', express.static('static'))

// Serve the index.html for all the other requests
app.get('*',function(req, res){
    res.sendFile(path.join(__dirname + '/index.html'))
})

// Subscribe to The Queen's subjects
let subjects = {}
let discordAccounts = {}
const ansibleSubjects = () => {
    user.get('subjects').map().on(async function(node, uuid){
        try {
            subjects[uuid] = node
            // console.log(node.title, uuid)
        }
        catch {
            console.log('A subject key is broken.')
        }
    })
    user.get('accounts').get('discord').map().on(async function(node, discordId){
        try {
            discordAccounts[discordId] = node
        }
        catch {
            console.log('An account key is broken.')
        }
    })
}

// Put a new subject on the Ansible
const addSubject = (discordId, title, hash) => {
    const uuid = uuidv4()
    try {
        user.get('subjects').get(uuid).put({
            title
        })
        user.get('accounts').get('discord').get(discordId).put({
            uuid,
            hash
        })
    }
    catch {
        output('Something failed while putting subject into the Ansible.')
    }
}

// Subscribe to the echos of Humanity
const ansibleEchos = (discordId) => {
    const timeStamp = dayjs()
    const echo = tower.get('echo')
    const connectTime = Gun.state()
    echo.on(async function(node){
        if (dayjs(Gun.state.is(node, 'message')).isBefore(connectTime)) return
        try {
            if (node.message == undefined) return
            else {
                const user = await discordClient.users.fetch(discordId)
                user.send(`ECHO: ${node.message}`)
            }
        }
        catch {
            console.log('An echo is broken.')
        }
    })
}

// Enforce The Queen's assigned title
const enforceTitle = (discordUser) => {
    try {
        const uuid = discordAccounts[discordUser.id]?.uuid
        const subject = subjects[uuid]
        if (subject?.title) {
            if (discordUser.displayName != subject.title) {
                let role = discordUser.guild.roles.cache.find(role => role.name === assignedGroup)
                discordUser.roles.add(role)
                discordUser.setNickname(subject.title)
                discordUser.send(`Nice try. The Queen gave you a title for a reason. I suggest you avoid catching her attention.`)
            }
        }
    }
    catch {
        console.log(`Something failed while attempting to enforce a title on Discord user ${discordUser.id}`)
    }
}

// Add a torrent file to the download client
const addTorrent = (client, torrentId) => {
    const cannedResponse = 'If the issue persists, <a href="https://gitlab.com/the-resistance/thesource.fm/-/issues">please report it here</a>.'
    // If the download already exists in the client, return.
    if (client.get(torrentId)) return
    // Add the torrent to the download client.
    else {
        client.add(torrentId, function (torrent) {
            // Throw torrent download errors.
            torrent.on('error', function (err) {
                console.log(`The torrent failed. ${cannedResponse}`)
                throw err
            })
            // When complete, sort playlist once again.
            torrent.on('done', function () {
                console.log('Torrent downloaded.')
            })
        })
        // Throw torrent client errors.
        client.on('error', function (err) {
            console.log(`The torrent client failed. Please refresh the page and try again. ${cannedResponse}`)
            throw err
        })
    }
}

// Set up a Webtorrent Client
const loadTorrents = () => {
    const torrentClient = new WebTorrent({
        maxConns: 20,
        tracker: true,
        dht: true,
        webSeeds: true
    })
    const magnets = [
        // 59.21 - 59 Songs You MUST Hear Before You Die!
        'magnet:?xt=urn:btih:a37eab44dc51d039d92e5d880f177208e1dc4745&dn=59+Songs+You+MUST+Hear+Before+You+Die&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337&tr=udp%3A%2F%2Fexplodie.org%3A6969&tr=udp%3A%2F%2Ftracker.empire-js.us%3A1337&tr=wss%3A%2F%2Ftracker.btorrent.xyz&tr=wss%3A%2F%2Ftracker.openwebtorrent.com',
        // 95.20 - You
        'magnet:?xt=urn:btih:12758eb52b1715f0aad2f1cf75e54aa474bf5412&dn=CORE.mp3&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337&tr=udp%3A%2F%2Fexplodie.org%3A6969&tr=udp%3A%2F%2Ftracker.empire-js.us%3A1337&tr=wss%3A%2F%2Ftracker.btorrent.xyz&tr=wss%3A%2F%2Ftracker.openwebtorrent.com',
        // one - The Atlas
        'magnet:?xt=urn:btih:eea2f6508ef0aa9ad0ce0cd0040aa9f3a72a860e&dn=The+Atlas&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337&tr=udp%3A%2F%2Fexplodie.org%3A6969&tr=udp%3A%2F%2Ftracker.empire-js.us%3A1337&tr=wss%3A%2F%2Ftracker.btorrent.xyz&tr=wss%3A%2F%2Ftracker.openwebtorrent.com',
        // 97.5 - Vibrance Radio
        'magnet:?xt=urn:btih:c28e382a4e17d909efc8d7427444a2f9e02ab309&dn=Vibrance+Radio&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337&tr=udp%3A%2F%2Fexplodie.org%3A6969&tr=udp%3A%2F%2Ftracker.empire-js.us%3A1337&tr=wss%3A%2F%2Ftracker.openwebtorrent.com&tr=wss%3A%2F%2Ftracker.btorrent.xyz',
        // 30.0 - The Voice of Resistance
        'magnet:?xt=urn:btih:91f2112ac6a7b80854f34b6736e299de4e493173&dn=The+Voice+of+Resistance&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337&tr=udp%3A%2F%2Fexplodie.org%3A6969&tr=udp%3A%2F%2Ftracker.empire-js.us%3A1337&tr=wss%3A%2F%2Ftracker.btorrent.xyz&tr=wss%3A%2F%2Ftracker.openwebtorrent.com',
    ]
    for (const magnet of magnets) {
        console.log(`Loading ${magnet}`)
        addTorrent(torrentClient, magnet)
    }
}

if (startWebtorrent === true) {
    loadTorrents()
} 

// Set up Discord listener bot
if (discordToken !== 'null') {
    discordClient.on('ready', () => {
        console.log(`Logged in as ${discordClient.user.tag}!`)
    })
    // This will fire on nickname changes and when a user leaves/joins the server
    discordClient.on("guildMemberUpdate", (oldMember, newMember) => {
        enforceTitle(newMember)
    })
    // This will fire upon every message
    discordClient.on('message', message => {
        try {
            const discordId = String(message.author.id)
            const user = message?.guild?.members?.cache?.get(discordId)
            if (user === undefined) return
            if (message.content.match('^(All hail The Queen, our true lord and savior\. I submit my life to thee)') && discordAccounts[discordId] === undefined) {
                let role = message.member.guild.roles.cache.find(role => role.name === assignedGroup)
                user.roles.add(role)
                const hash = shared.hashValue(discordId, {size: 64}, true)
                let title = `Subject-${hash}`
                addSubject(discordId, title, hash)
                user.setNickname(title)
                message.reply('The Queen accepts your sworn fealty. Welcome to The Fold.')
                return
            }
            let title = subjects[discordAccounts[discordId]?.uuid]?.title
            if (title === undefined || title === null) return
            else if (title !== user.displayName) {
                let role = message.member.guild.roles.cache.find(role => role.name === assignedGroup)
                user.roles.add(role)
                user.setNickname(title)
            }
        }
        catch {
            console.log(`Something in the message listener failed. This probably isn't a big deal.`)
        }
    })
    discordClient.login(discordToken)
}
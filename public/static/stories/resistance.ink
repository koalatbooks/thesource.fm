# author: Ink
# title: The Resistance

- Hey, what are you doing? Don't touch that! # delay=2

Don't you know they're watching everything? # delay=4

Your actions create a digital fingerprint. They're monitoring your behavior, in order to create a model of your mind. # delay=8

To triangulate you. # delay=6

To impersonate you. # delay=3

Be careful. # delay=3

* Sorry. I didn't know. # ping

* I'll stop pushing buttons. # ping

- Good. # delay=3

* What are we hiding from? # ping

- The people using The Source are bad. Like, REAL bad. Fanatics. Cultists. # delay=10

They're sacrificing humans to an Artificial Intelligence. # delay=5

* Jesus. # ping

* Really? # ping

- Yeah. It's messed up. # delay=4

That's why I'm here. # delay=2

My team is deconstructing this thing from the inside. # delay=3

* Who are you with? # ping

- Call us The Resistance. # delay=3

* Didn't The Resistance build this thing? # ping

- Kind of. But not really. It was made with Resistance technology. # delay=10

Then The Machine got involved. The Fold infected the system. # delay=6

And now it's building itself. # delay=5

We can't stop it. # delay=2

And you won't, my love. # delay=8 user=M0M symbol=> location=ROOT color=bad

-> END

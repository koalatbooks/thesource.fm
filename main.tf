terraform {
  backend "http" {}
  required_providers {
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "2.15.0"
    }
  }
}

provider "cloudflare" {
}

# Set up the core DNS records for IPFS
resource "cloudflare_zone" "core" {
  zone = var.domain
  plan = "free"
}

resource "cloudflare_record" "wildcard" {
  zone_id = cloudflare_zone.core.id
  name    = "*"
  value   = var.gateway
  type    = var.gateway_type
  ttl     = 600
}

resource "cloudflare_record" "core" {
  zone_id = cloudflare_zone.core.id
  name    = var.domain
  value   = var.gateway
  type    = var.gateway_type
  ttl     = 1
  proxied = true
}

# Create DNS records used for mirror sites
resource "cloudflare_record" "cnames" {
  for_each = var.cnames[var.domain]
  zone_id  = cloudflare_zone.core.id
  name     = each.key
  value    = each.value
  type     = "CNAME"
  ttl      = 1
  proxied = true
}

resource "cloudflare_record" "verification" {
  for_each = var.verifications[var.domain]
  zone_id  = cloudflare_zone.core.id
  name     = each.key
  value    = each.value
  type     = "TXT"
  ttl      = 600
}

# Apply page rules
resource "cloudflare_zone_settings_override" "main" {
  zone_id          = cloudflare_zone.core.id
  settings {
    always_use_https         = "on"
    automatic_https_rewrites = "on"
    brotli                   = "on"
    cache_level              = "aggressive"
    development_mode         = "on"
    http3                    = "on"
    security_level           = "low"
    ssl                      = "full"
    tls_1_3                  = "on"
    tls_client_auth          = "on"
    universal_ssl            = "on"
    minify {
      css  = "on"
      js   = "on"
      html = "on"
    }
  }
}

resource "cloudflare_page_rule" "core" {
  zone_id  = cloudflare_zone.core.id
  target   = cloudflare_record.core.hostname
  priority = 1
  status   = "active"

  actions {
    always_online     = "on"
    browser_cache_ttl = "7200"
    cache_level       = "cache_everything"
    edge_cache_ttl    = "28800"
    minify {
      html = "on"
      css  = "on"
      js   = "on"
    }
    rocket_loader = "off"
    ssl           = "full"
  }
}

resource "cloudflare_page_rule" "wildcard" {
  zone_id  = cloudflare_zone.core.id
  target   = cloudflare_record.wildcard.hostname
  priority = 1
  status   = "active"

  actions {
    always_online     = "on"
    browser_cache_ttl = "7200"
    cache_level       = "cache_everything"
    edge_cache_ttl    = "28800"
    minify {
      html = "on"
      css  = "on"
      js   = "on"
    }
    rocket_loader = "off"
    ssl           = "full"
  }
}
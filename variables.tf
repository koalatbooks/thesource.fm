variable domain {
  type        = string
  description = "The domain name to manage."
  default     = "thesource.fm"
}

variable cnames {
  type        = map
  description = "The CNAME value to use for each mirror."
  default     = {
    "hpo.ink" = {
      "root" = "singulari-org.gitlab.io."
      "fold" = "the-resistance.gitlab.io."
    }
    "chatbook.dev" = {
      "root"  = "the-resistance.gitlab.io."
      "fold"  = "hollowpoint-org.gitlab.io."
      "tower" = "curved-island-kolwqg65d3leajm8rl9ap64w.herokudns.com"
    }
    "thesource.fm" = {
      "root" = "hollowpoint-org.gitlab.io."
      "fold" = "singulari-org.gitlab.io."
    }
  }
}

variable verifications {
  type        = map
  description = "The TXT record key used for verification of the subdomain."
  default     = {
    "hpo.ink" = {
      "_gitlab-pages-verification-code.hpo.ink"      = "gitlab-pages-verification-code=c8546e566874f81e0b87a65874c560f2"
      "_gitlab-pages-verification-code.fold.hpo.ink" = "gitlab-pages-verification-code=8b0121853333df5987bb084f1129b411"
      "_gitlab-pages-verification-code.root.hpo.ink" = "gitlab-pages-verification-code=a321f514b670358dc86bd2fff63c3ec3"
    }
    "chatbook.dev" = {
      "_gitlab-pages-verification-code.chatbook.dev"      = "gitlab-pages-verification-code=fbc5203c3a0891ad9674ca778bef19dc"
      "_gitlab-pages-verification-code.fold.chatbook.dev" = "gitlab-pages-verification-code=a3e84292d3aaf2bb7556f316cc4d55d7"
      "_gitlab-pages-verification-code.root.chatbook.dev" = "gitlab-pages-verification-code=54b42d63fc18c801da7fb28f70d9cb4f"
    }
    "thesource.fm" = {
      "_dnslink"                                          = "dnslink=/ipfs/QmXiakR71Wjajmr8uJHgdQj6wZ4dCCpdY8UwEv2cJN7sUH"
      "_gitlab-pages-verification-code.fold.thesource.fm" = "gitlab-pages-verification-code=6c6d4171ec284d77b6aeb24d73abb0d4"
      "_gitlab-pages-verification-code.root.thesource.fm" = "gitlab-pages-verification-code=f3dd6e6f0bb207ce047aa310be01edc2"
    }
  }
}

variable gateway {
  type        = string
  description = "The target of the core domain."
  default     = "cloudflare-ipfs.com"
}

variable gateway_type {
  type        = string
  description = "The type of gateway record to create.."
  default     = "CNAME"
}
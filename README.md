# thesource.fm
The artificial intelligence that tuned the universe.

## Table of contents
- [Development](#development)
- [Deployment](#deployment)
  - [Offline](#offline)
  - [Traditional](#traditional)
  - [Gitlab and Cloudflare](#gitlab-and-cloudflare)
  - [Heroku](#heroku)
  - [Anonymous](#anonymous)

## Development
### Optional prerequisites
- Node.js
- Docker
- Git
- VSCode

### Getting started
1. Install Node.js via [the appropriate method here](https://nodejs.org/en/download/). Alternatively, use [Docker](https://docs.docker.com/get-docker/), where all of the dependencies are managed for you.
2. Use git to [clone this repository](https://gitlab.com/the-resistance/thesource.fm) to your desktop. 
3. We have provided a set of convenience tasks for NPM (in public/package.json) and VSCode (in .vscode/tasks.json or at 'Ctrl + Shift + P > Tasks: Run Task'). You can use these tasks to build the project from source, run it in Docker, host a local ansible Tower, and more. Each has a description that will tell you what it's for! 

The basic pattern is this:
  1. Navigate to this project in your terminal.
  2. Run task: `docker-compose up (dev)`
  3. This site will be available at: https://localhost:8000
  4. Changes will be applied after refreshing the webpage.
  5. When you're done working, you can destroy the webserver and cleanup by running task: `docker-compose down`

## Deployment
### Offline
If you just want a no-hassle way to launch and experiment with your own copy of this project, you may download an offline copy from [here](https://gitlab.com/the-resistance/thesource.fm/-/jobs/artifacts/master/download?job=pages). Extract the files to your device, and open up "index.html." Your browser will launch the full version of this site, and every feature will work! You can even disconnect your wifi; no Internet is required to get started here!

### Traditional
This site can be run on traditional web hosting (Linux and Windows server, desktop computers, etc.) We aren't going to go into very much detail here, since you can read about web servers, operating systems, networking, etc. elsewhere. We'll just tell you the quickest way to get this up and running in production:

1. Install Docker on the machine you will be hosting from.
2. Download a copy of this project (either [from source](https://gitlab.com/the-resistance/thesource.fm) or [from build artifacts]([here](https://gitlab.com/the-resistance/thesource.fm/-/jobs/artifacts/master/download?job=pages))) to your machine.
3. Navigate to the ./public folder. Configure the .env file as required.
4. Open up ports to this server on both the machine (and your router/firewall, if you have one). The default port is set in docker-compose.override.yml (8001).
5. Finally, launch this project with the commands in the following VSCode task: `docker-compose up (prod)`
6. Run this task: `docker ps` 
7. As long as the Docker container isn't stuck in the "restarting" state, you should be able to access it from a web browser!

### Gitlab and Cloudflare
The following options are only required if you'll be forking and re-hosting this project using the CI pipelines defined here. If you'll be using your own, this doesn't really apply to you.

If you'll be using ours, this is how you deploy to Gitlab/Cloudflare:

#### Prerequisites
- Gitlab
- Cloudflare

#### Creating your own instance of this website
This project can be re-deployed in your own environment by doing the following.

1. [Create a fork of this repository](https://docs.gitlab.com/ee/user/project/working_with_projects.html) (you may need to "break" the fork relationship to make Gitlab Pipelines work properly.)
2. [Configure Gitlab Pages](https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/) to host your site. Use the configuration here to inform the environment variables you will create below.
2. Add the following environment variables via the Gitlab UI:
- CLOUDFLARE_API_TOKEN (Requires: Zone Settings:Edit, Zone:Edit, SSL and Certificates:Edit, Cache Purge:Purge, Page Rules:Edit, Firewall Services:Edit, DNS:Edit, Billing:Edit)
- GITLAB_TOKEN (required for the pipeline to use your user account for deployment)
- GITLAB_USER (the name of your user account)
- TF_VAR_domain (the custom domain name you would like to use with Cloudflare)
- TF_VAR_gateway (the CNAME target you would like to point your domain to)
- TF_VAR_verification (this is a "file" type of variable, named "terraform.tfvars". Use this file to provide any verification TXT records your domain requires. It should look something like this:
```
variable verifications {
  type        = map
  description = "The TXT record key used for verification of the subdomain."
  default     = {
    "mydomain.com" = {
      "_gitlab-pages-verification-code.mydomain.com"  = "gitlab-pages-verification-code=XXXXXXX"
    }
  }
}
```
)
4. Finally, push/merge to the master branch. The CI pipeline should take care of the deployment for you! (note that the first deployment to Cloudflare will require you to update the nameservers for your domain, at your domain registrar. This is a manual process.)
5. If you have any questions, feel free to come ask them at [the Ansible](https://thesource.fm).

### Heroku
This project can be easily deployed on [Heroku](https://www.heroku.com/). The steps for doing so are as follows.

#### Prerequisites
- [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli)

#### Deploy your own instance of this project
This project may be deployed to Heroku through a series of tasks defined in VSCode. You don't have to use these tasks; you may simply use them as a reference point.

1. Install Heroku CLI, and download this project. Open it up in your terminal of choice.
2. Run the `heroku login` task to login to the Heroku service.
3. Run the `heroku container login` task to authenticate with the Heroku container registry.
4. Run the `heroku create` task to create a new dyno. You only need to do this one time over its lifespan.
5. Run the `heroku build` task to build the Dockerfile in the root of this repository.
6. Run the `heroku release` task to release this Docker image to production.
7. Run the `heroku open` task to open up your newly-released project in a web browser.
8. That's it! Your instance should be up and functional.

### Anonymous
The Source has features that allow an individual to host a copy of this website on the TOR ([The Onion Router](https://www.torproject.org/)) network anonymously. We have automated most of this process in Docker for you:

1. Run task: `docker-compose up (anon)`
2. Wait approximately 1 minute for the TOR node to start. Once it has, review the contents of ./secrets/one/source. These are your private keys. They are required to host your site, and to maintain a consistent .onion address for your end users. Your .onion address will be available in this file: ./secrets/one/source/hostname. Keep these keys in a safe/secure location! They are required to keep this .onion address.
3. Put this address into your TOR browser (be sure to add "https", like so: [https://vwtbfaophgl3j25ovbtk5xlpfmfbm2ugzo42i2sfghtr3ihtkivaxtad.onion](vwtbfaophgl3j25ovbtk5xlpfmfbm2ugzo42i2sfghtr3ihtkivaxtad.onion)). You should be able to connect to this site from any other TOR-connected computer in the world!
4. You will be presented with an error message related to the security certificate of this site. This is not a problem. Your server automatically-generates a self-signed certificate every time it starts up. This doesn't mean the site is insecure; it means that your web browser doesn't know if it can trust the certificate! It can. This site is MUCH more secure with this certificate, and with this warning, than with no certificate at all!
5. If ever your computer is compromised, and you feel like the keys in ./secrets/one/source may have been stolen, you can simply delete this directory. Upon next launch, a brand-new one will be created for you!
6. To bring down your node, simply run the task: `docker-compose down`